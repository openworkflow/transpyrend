# TransPyRend: Overview

<ins>Trans</ins>portmodell in <ins>Py</ins>thon für den Austrag von <ins>R</ins>adionukliden aus einem <ins>End</ins>lager / Transportmodel in Python for radionuclides in and around nuclear waste repositories.

TransPyREnd is a model for simulating the diffusion, advection, sorption and decay of radionuclides in a porous medium in one spatial dimension. A [paper describing the methodology](https://adgeo.copernicus.org/articles/58/109/2023/) is published in "Advances in Geoscience".

The aim for the development of TransPyREnd was to provide a flexible, modular, open-source tool to simulate radionuclide transport in the subsurface and to calculate mass and particle flux of radionuclides around a repository.

The partial differential equations that govern radionuclide transport are discretized following the finite-difference method.

Please note that the package is still under development, undergoing rigorous testing.


![Concentration of radionuclides as a function of depth in a synthetic test](figures/fig1.png "Concentration versus depth")

_Concentration of radionuclides as a function of depth in a synthetic test_

## Getting started

* Download the zip file or clone the repository:
  
  ```bash
  git clone https://gitlab.opengeosys.org/openworkflow/transpyrend
  ```

* Install dependencies using [Anaconda](https://www.anaconda.com/) or [pip](https://pypi.org/project/pip/):

  ```bash
  conda env create -f environment.yml --name rnmodel
  conda activate rnmodel
  
  # Or with a virtual environment and pip:
  python -m venv .venv
  source .venv/bin/activate

  # Now install transpyrend into local Python environment.
  # In this case also install all optional dependencies:
  pip install -e .[dev,tests,docs,notebooks]
  ```

## Run tests

The numerical model code was evaluated by comparing it to several analytical and semi-analytic solutions. Tests can be found in the subdirectory [tests/](tests). To run them:

```bash
python -m unittest
```

## Run a notebook

* Start the notebook application `jupyter notebook`
* Open from the `notebook/examples`-directory

## API documentation

There is an automatically generated html documentation done with [Sphinx](https://www.sphinx-doc.org/en/master/) in the subdirectory [docs/](docs/). To generate the documentation:

```bash
# With an activated conda environment (see Getting started):
conda install sphinx
pip install sphinx-autoapi myst-parser numpydoc

# OR with an activated virtual environment (see Getting started):
# Already installed via the [...,docs,...] optional dependency group

cd docs
make html
# open build/html/index.html in a browser
```

## Build the package

```bash
# Creates tarballs and wheels in dist-directory:
python -m build
```

## License

This work is provided under the terms of the EUPL 1.2 if not stated otherwise, see the `LICENSE` file. If you make use of TransPyREnd in your work, please cite our [paper describing the methodology](https://adgeo.copernicus.org/articles/58/109/2023/) (published in "Advances in Geoscience"):

* Behrens, C., Lujiendijk, E., Kreye, P., Panitz, F., Bjorge, M., Gelleszun, M., Renz, A., Miro, S., Rühaak, W.: *TransPyREnd: a code for modelling the transport of radionuclides on geological timescales*, Adv. Geosci., 58, 109-119, DOI: [10.5194/adgeo-58-109-2023](https://adgeo.copernicus.org/articles/58/109/2023/)
## Acknowledgments

We thank the [OpenGeoSys](https://www.opengeosys.org) team for useful discussions, review and comparisons.

Atomic masses in TransPyREnd are taken from [Nubase2020](https://www-nds.iaea.org/relnsd/nubase/nubase_min.html) data, for details see 
* F.G. Kondev, M. Wang, W.J. Huang, S. Naimi, and G. Audi, Chin. Phys. C45, 030001 (2021)

The implementation of the analytic solution to the Bateman equation for linear chains has been adapted from [this github repository](https://github.com/bjodah/batemaneq), with the copyright notice retained as required.


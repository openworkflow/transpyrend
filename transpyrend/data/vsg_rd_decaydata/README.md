# vsg_rd_decaydata

This folder contains decay data for the reduced set of nuclides as provided in GRS report GRS-289. The binary files
can be read by the Python package `radioactivedecay`.
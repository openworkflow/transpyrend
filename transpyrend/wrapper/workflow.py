"""
contains a function to call the newer parameterworkflow routines to calculate the transport parameters and run the
resulting transpyrend models as a batch job.
"""

import logging
import traceback
import os
import time

import astropy.units as u

import transpyrend.core.ensemble as EnsembleModel1D
import transpyrend.parameters.model as ModelParameters
from transpyrend.parameters.releasemodel import ReleaseModelParameters
from transpyrend.parameters.misc import convert_layers
from transpyrend.wrapper.output import process_results
from transpyrend.wrapper.tools import instantiate_model
from transpyrend.utils.nuclides import vsg_tab411_data


def run_workflow(args):
    from parameterworkflow.drivers.fromfiles import generate_model_parameters
    nuclides = list(vsg_tab411_data.keys())
    in_dir_workflow = args.inname
    out_dir_workflow = args.inname
    generate_model_parameters(nuclides, in_dir_workflow, out_dir_workflow)
    return os.path.join(out_dir_workflow, "model_parameters.yaml")


def run_model_batch(args, return_results=False, ensemble_modeltype=EnsembleModel1D.VSGEnsembleModel1D):
    """
    run models in batchmode. supports both the sampling_matrix mode of parameterworkflow, and the older per-directory
     mode.

    - if args.template and args.sampling_matrix are set, args.template is interpreted as the path to a
    template parameter set as defined in parameterworkflow. args.sampling_matrix needs to point to a csv file holding
     the sampling matrix. args.indir is interpreted as the directory in which the resulting parameters and transpyrend
      runs should be stored.

    - otherwise, args.indir is read as a directory containing at least one directory in inputDataFiles/run1 with the
    input parameters for executing the workflow.

    Parameters
    ----------
    args : ModelArgs or Namespace
        the arguments
    return_results : bool
        return list of results if true
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model

    Returns
    -------
    allresults: list or None
        the results
    """
    from enum import Enum
    from parameterworkflow.drivers.fromsamplingmatrix import load_sampling_matrix
    from parameterworkflow.drivers.fromfiles import prepare_workflow
    from parameterworkflow.utils.io import write_workflow_parameters_to_hdf5

    nuclides = list(vsg_tab411_data.keys())

    RunMode = Enum('RunMode', ['FILEBASED', 'MATRIXBASED'])

    if args.template is None:
        RunMode = RunMode.FILEBASED
    else:
        RunMode = RunMode.MATRIXBASED

    sampling_matrix = None

    # location of output files from workflow / input files for transpyrend
    in_dirname = os.path.join(args.multi_run_dir, "modelInputFiles")
    if not args.in_memory:
        os.makedirs(in_dirname, exist_ok=True)
    logging.info(f"reading transpyrend inputs from {in_dirname} ")

    if RunMode is RunMode.FILEBASED:
        # location of input files for workflow
        in_geo_dirname = os.path.join(args.multi_run_dir, "inputDataFiles")
        logging.warning(f"reading workflow inputs from {in_geo_dirname} ")
        # each individual directory in in_geo_dirname is a single run; figure out names and number
        content = os.listdir(in_geo_dirname)
        from natsort import natsorted
        content = natsorted(content)
        run_names = []
        for d in content:
            full_path = os.path.join(in_geo_dirname, d)
            if os.path.isdir(full_path):
                run_names.append(d)
        nruns = len(run_names)

    if RunMode is RunMode.MATRIXBASED:
        sampling_matrix = load_sampling_matrix(args.sampling_matrix)
        if args.in_memory:
            if "special.location_repository" in sampling_matrix.columns:
                logging.info("reading repository locations from sampling matrix")
            else:
                logging.warning(
                    "repository locations not present in sampling matrix. Will take default value from template!")
        nruns = len(sampling_matrix.index)
        run_names = [f"model{i}" for i in range(nruns)]
        logging.info(f"preparing {nruns} runs from sampling matrix.")

    # location of output files from transpyrend
    out_dirname = os.path.join(args.multi_run_dir, "modelOutputFiles")
    if args.nrunner > 1:
        hdf5_fname = os.path.join(args.multi_run_dir, f"ensemble_results_{args.rid}.hdf5")
    else:
        hdf5_fname = os.path.join(args.multi_run_dir, "ensemble_results.hdf5")
    import h5py
    #clear the file if it exists
    with h5py.File(hdf5_fname, "w"):
        pass

    os.makedirs(out_dirname, exist_ok=True)
    logging.info(f"writing outputs to {out_dirname} ")
    # prefix of the output directories
    outprefix = "modelOutput_"

    allresults = []
    irun = 0

    for i in range(nruns):
        # if we have more than one runner, decide whether this job is ours
        if args.nrunner > 1 and (i % args.nrunner != args.rid):
            # this is not our job; proceed with the next one
            continue
        # prepare pathes
        run_start = time.time()
        run_name = run_names[i]
        in_dir_transpyrend = os.path.join(in_dirname, run_name)
        out_dir_workflow = in_dir_transpyrend
        output_prefix = outprefix + run_name
        args.outdir = os.path.join(out_dirname, output_prefix)
        os.makedirs(args.outdir, exist_ok=True)
        if RunMode is RunMode.FILEBASED:
            in_dir_workflow = os.path.join(in_geo_dirname, run_name)
        else:
            in_dir_workflow = args.template

        if not args.in_memory:
            args.inname = os.path.join(in_dir_transpyrend, "model_parameters.yaml")
            _prepare_files(in_dir_transpyrend, in_dir_workflow, out_dir_workflow)
        else:
            args.inname = os.path.join(args.template, "model_parameters.yaml")

        try:
            irun += 1
            logging.info(f">>> Running model {irun} <<<")
            model, mp = _run(args, nuclides, in_dir_workflow, out_dir_workflow, RunMode, sampling_matrix, i,
                             ensemble_modeltype)
        except Exception as e:
            if args.graceful:
                logging.error(traceback.format_exc())
            else:
                raise e
        results = [run_name, process_results(args, model, mp)]
        if args.in_memory:
            _write_results(hdf5_fname, run_name, results[1])
            sampling_matrix.to_hdf(hdf5_fname, "sampling_matrix")
            all_params = prepare_workflow(args.template)
            write_workflow_parameters_to_hdf5(hdf5_fname, *all_params, prefix="template")

        if return_results:
            allresults.append(results)
        del model
        del mp
        run_end = time.time()
        logging.info(f"Model+Workflow done in {run_end - run_start:0.2f} s")
    if return_results:
        return allresults
    return None


def _write_results(fname, run_name, results):
    import h5py
    with h5py.File(fname, "a") as f:
        f.require_group(run_name)
        for key in results:
            f.create_dataset(run_name+"/"+key, data=results[key])


def _run(args, nuclides, in_dir_workflow, out_dir_workflow, RunMode, sampling_matrix, i, ensemble_modeltype):
    """
    helper function to run a single model out of a batch

    Parameters
    ----------
    args : ModelArgs or Namespace
        the arguments
    nuclides : list
        the list of nuclides considered
    in_dir_workflow : str
        the directory with the workflow parameter files for this run
    out_dir_workflow : str
        the output directory of the workflow
    RunMode : Enum
        Enum type indicating if we run from files or sampling matrix
    sampling_matrix : pandas.DataFrame
        the sampling matrix
    i : int
        the index of the run
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model

    Returns
    -------
    model: VSGEnsembleModel1D
        the model
    mp: TransPyrEndParameters
        the model parameters
    """
    from parameterworkflow.drivers.fromfiles import generate_model_parameters
    from parameterworkflow.drivers.fromsamplingmatrix import generate_sampled_model_parameters
    from transpyrend.wrapper.tools import prepare_model_from_disk, run_model

    logging.info("running parameter workflow")
    if args.in_memory:
        model, mp = prepare_model_in_memory(args, sampling_matrix, i,
                                            ensemble_modeltype=ensemble_modeltype)
        if "special.location_repository" in sampling_matrix.columns:
            mp.location_repository = sampling_matrix["special.location_repository"].values[i] * u.m
    else:
        # generate the parameter files from the workflow
        if RunMode is RunMode.FILEBASED:
            generate_model_parameters(nuclides, in_dir_workflow, out_dir_workflow)
        else:
            generate_sampled_model_parameters(nuclides, in_dir_workflow, out_dir_workflow, sampling_matrix, i)
        model, mp = prepare_model_from_disk(args, ensemble_modeltype=ensemble_modeltype)
    model, mp = run_model(args, model, mp)
    return model, mp


def _prepare_files(in_dir_transpyrend, in_dir_workflow, out_dir_workflow):
    """
    helper routine to copy files necessary to run transpyrend in batch

    Parameters
    ----------
    in_dir_transpyrend : str
        the directory with the transpyrend parameter files for this run
    in_dir_workflow : str
        the directory with the workflow parameter files for this run
    out_dir_workflow : str
        the output directory of the workflow

    Returns
    -------

    """
    import shutil
    os.makedirs(out_dir_workflow, exist_ok=True)
    # copy model_parameters.yaml to the right directory
    original_model_parameters_fn = os.path.join(in_dir_workflow, "model_parameters.yaml")
    target_model_parameters_fn = os.path.join(out_dir_workflow, "model_parameters.yaml")
    shutil.copy(original_model_parameters_fn, target_model_parameters_fn)
    # copy release model if necessary
    mp = ModelParameters.TransPyREndParameters(filename=os.path.join(in_dir_workflow, "model_parameters.yaml"),
                                               read_shallow=True)
    if mp.release_model is not None:
        # copy the release model from the inputDate to the inputModel
        # TODO this is going to break for composite release models!
        src = mp.release_model
        shutil.copy(src, in_dir_transpyrend)
        rp = ReleaseModelParameters(src)
        shutil.copy(rp.nuclide_parameters_filename, in_dir_transpyrend)


def prepare_model_in_memory(args, sampling_matrix, i, ensemble_modeltype=EnsembleModel1D.VSGEnsembleModel1D,
                            override_args=None):
    """
    prepare a model given a ModelArgs object; generate the parameter from a workflow in-memory.

    Parameters
    ----------
    args: ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    sampling_matrix: pandas.DataFrame
        the sampling matrix
    i : int
        the index of the run considered
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model
    override_args: dict
        override these attribute (keys) in the parameter object with the corresponding values

    Returns
    -------
    model: VSGEnsembleModel1D
        the prepared model
    mp: TransPyrEndParameters
        the model parameters

    """
    mp = instantiate_parameters(args, i, override_args, sampling_matrix)
    model, mp = instantiate_model(args, mp, ensemble_modeltype=ensemble_modeltype)
    return model, mp


def instantiate_parameters(args, i, override_args, sampling_matrix):
    """
    instantiate a ModelParameters object from scratch

    Parameters
    ----------
    args: ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    sampling_matrix: pandas.DataFrame
        the sampling matrix
    i : int
        the index of the run considered
    override_args: dict
        override these attribute (keys) in the parameter object with the corresponding values

    Returns
    -------
    mp: TransPyrEndParameters
        the model parameters

    """
    from parameterworkflow.drivers.fromsamplingmatrix import get_sampled_parameters
    logging.info("constructing parameters")
    nuclides = list(vsg_tab411_data.keys())
    dfs, df_layers = get_sampled_parameters(nuclides, args.template, sampling_matrix, i)
    parameters_rock, parameters_transport_material = convert_layers(dfs, df_layers)
    mp = ModelParameters.TransPyREndParameters(filename=os.path.join(args.template, "model_parameters.yaml"),
                                               read_shallow=True)
    mp.parameters_rock = parameters_rock
    mp.parameters_transport_material = parameters_transport_material
    mp.setup_grid()
    if override_args is not None:
        for key, value in override_args.items():
            logging.warning(f"Overriding {key} in parameters")
            setattr(mp, key, value)
    if args.round_down or mp.snap_to_units is False:
        assert mp.snap_to_units is False
        mp.round_down_unit_thicknesses(mp.dx)
        logging.warning("Option snap_to_units is disabled, rounding down unit widths to match grid!")
    else:
        logging.info("Snapping grid to units")
    mp.add_stable_species()
    return mp

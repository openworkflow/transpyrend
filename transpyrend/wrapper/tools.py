"""
contains several routines used to prepare and execute transpyrend runs in the wrapper scripts
"""
import logging
import traceback
import os
import time

import astropy.units as u
import numpy as np

import transpyrend.core.ensemble as EnsembleModel1D
import transpyrend.parameters.model as ModelParameters
from transpyrend.core.externalsolver import DOP853TransportModel1D
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D
from transpyrend.core.cranknicolson import CrankNicolsonTransportModel1D
from transpyrend.core.base import GeometryArgs, OptionArgs
from transpyrend.parameters.releasemodel import ReleaseModelParameters, read_release_model
from transpyrend.wrapper.output import process_results


class ModelArgs:
    """
    wrapper class for calling run_model without invoking the command line. Needs to mimic the structure of the Args
    object returned by ArgParse.parse (argpase.Namespace)


    Parameters
    ----------
    inname : str
        the input directory or file
    outdir : str
        the output directory
    output : list
        the desired outputs
    observation_point : float
        a point on the x axis

    """

    def __init__(self, inname, outdir=None, output=None, observation_point=None):

        self.solver = "euler-backwards"
        self.inname = inname
        self.outdir = outdir
        self.realization = None
        self.multiproc_driver = "pathos"
        self.nprocs = 8
        self.output = output
        self.use_hdf5 = False
        if outdir is None:
            self.save_output = False
        else:
            self.save_output = True
        self.observation_point = observation_point
        self.dry = False
        self.profile = None
        self.output_interval = None
        self.round_down = False
        self.nrunner = 1
        self.in_memory = False
        self.template = None
        self.rid = 0


def get_git_info():
    """
    retrieve information about the git revision and the state of the working directory

    Returns
    -------
    out : str
        a string indicating revision number and state
    """
    import subprocess
    dirname = os.path.dirname(__file__)
    try:
        out = subprocess.check_output(['git', 'rev-parse', 'HEAD'],cwd=dirname).decode('ascii').strip()
        result = subprocess.run(['git', 'diff-index', '--quiet', 'HEAD'], cwd=dirname)
        if result.returncode == 0:
            out += ", working dir CLEAN"
        else:
            out += ", working dir DIRTY"
    except OSError:
        out = "<git not found>"

    return out


def instantiate_parameters_from_disk(args, override_args=None):
    """
    prepare the ModelParameters object

    Parameters
    ----------
    args: ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    override_args: dict
        override these attribute (keys) in the parameter object with the corresponding values

    Returns
    -------
    mp: TransPyrEndParameters
        the model parameters

    """
    logging.info("Reading parameters")
    mp = ModelParameters.TransPyREndParameters(filename=args.inname)
    if override_args is not None:
        for key, value in override_args.items():
            logging.warning(f"Overriding {key} in parameters")
            setattr(mp, key, value)

    if args.round_down or mp.snap_to_units is False:
        assert mp.snap_to_units is False
        mp.round_down_unit_thicknesses(mp.dx)
        logging.warning("Option snap_to_units is disabled, rounding down unit widths to match grid!")
    else:
        logging.info("Snapping grid to units")
    mp.add_stable_species()

    return mp


def instantiate_model(args, mp, ensemble_modeltype=EnsembleModel1D.VSGEnsembleModel1D):
    """
    prepare the model

    Parameters
    ----------
    args: ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model

    Returns
    -------
    model: VSGEnsembleModel1D
        the prepared model
    mp: TransPyrEndParameters
        the model parameters

    """
    # handle additional arguments
    src_args = {}
    if mp.release_model is not None:
        logging.info("Generating source term from release model...")
        tspan = [0, 0] * u.yr
        tspan[1] = mp.time_interval
        tspan[1] *= 1.1
        bs = read_release_model(mp.release_model, mp.storage_year)
        bst = bs.get_output_source_term(tspan, 100000)
        src_args["source_term"] = bst
        src_args["near_field_index"] = [mp.get_deposit_position()[0]]

    # build model
    logging.info("Building Model")

    if args.solver == "euler-backwards":
        modeltype = EulerBackwardsModel1D
    elif args.solver == "dop853":
        modeltype = DOP853TransportModel1D
    elif args.solver == "crank-nicolson":
        modeltype = CrankNicolsonTransportModel1D
    else:
        msg = "Solver must be one of: 'euler-backwards', 'dop853', 'crank-nicolson'"
        raise RuntimeError(msg)
    geometry = GeometryArgs({"x": mp.x, "area_cell": mp.area_repository})
    options = OptionArgs({"include_decay": mp.include_decay,
                          "use_bateman": mp.use_bateman,
                          "include_sorption": mp.include_sorption,
                          "store_flux": mp.store_flux})
    model = ensemble_modeltype(mp.parameters_rock,
                               parameters_transport_material=mp.parameters_transport_material,
                               geometry=geometry,
                               time_interval=mp.time_interval,
                               modeltype=modeltype,
                               options=options, parameters_source=src_args)
    model.set_cfl_limit(mp.cfl_limit)
    model.set_hl_limit(mp.hl_limit)

    if mp.use_bateman and args.solver != "dop853":
        logging.info("Using Bateman solution: Setting hl_limit=10")
        model.set_hl_limit(10.)

    return model, mp


def prepare_model_from_disk(args, ensemble_modeltype=EnsembleModel1D.VSGEnsembleModel1D, override_args=None):
    """
    prepare a model given the ModelArgs. Assumes that the parameters are to be read from disk

    Parameters
    ----------
    args: ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model
    override_args: dict
        override these attribute (keys) in the parameter object with the corresponding values

    Returns
    -------
    model: VSGEnsembleModel1D
        the prepared model
    mp: TransPyrEndParameters
        the model parameters

    """
    mp = instantiate_parameters_from_disk(args, override_args=override_args)
    model, mp = instantiate_model(args, mp, ensemble_modeltype=ensemble_modeltype)
    return model, mp


def run_model(args, model, mp):
    """
    run the specified model with the specified parameters

    Parameters
    ----------
    args : ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    model : TransportModel1D
        the model to be run
    mp :  modelparameters
        the model parameters

    Returns
    -------
    model: VSGEnsembleModel1D
        the finished model
    mp: modelparameters
        the model parameters

    """
    logging.info("Running Model - this might take a while...")
    # TODO next line does not work anymore with variable dx
    width = ((mp.width_repository / mp.dx).to(u.dimensionless_unscaled))
    if width != int(width):
        msg = "width of the deposit must be multiple of dx"
        raise RuntimeError(msg)
    if mp.release_model is None:
        # catch the case in which we have our own inventory files
        # TODO this is fragile
        fname = os.path.join(os.path.dirname(args.inname), "nuclide_parameters.csv")
        if not os.path.isfile(fname):
            # use default files if the local file is not present
            fname = None
        ic_dict = ReleaseModelParameters.get_ics_for_instant_release(model.species, mp.height_repository,
                                                                     mp.area_repository, width=int(width), N=model.N,
                                                                     deposit_index=mp.get_deposit_position()[0],
                                                                     return_dict=True, fname=fname,
                                                                     storage_year=mp.storage_year, nuclide_scheme="VSG")

    else:
        ic_dict = {}
        for spec in model.species:
            ic_dict[spec] = 0.0*u.mol/u.m**3
    times = []
    for m in model.models:
        times.append(m.get_max_timestep().to(u.yr).value)

    if not args.dry:
        if mp.dt_max > -1 * u.yr:
            dts_max = mp.dt_max
        else:
            dts_max = None
            logging.info("using maximum timesteps of:")
            for i,t in enumerate(times*u.yr):
                logging.info(f"\t{t:.1f} for submodel#{i} with species {model.models[i].species[:-1]}")
        if mp.output_interval > -1 * u.yr:
            t_eval = np.arange(0, (mp.time_interval + mp.output_interval).value, mp.output_interval.value) * u.yr
        else:
            t_eval = None
        if args.output_interval is not None:
            logging.info("Setting output_interval to command line input of " + repr(args.output_interval * u.yr))
            t_eval = np.arange(0, (mp.time_interval + args.output_interval * u.yr).value,
                               (args.output_interval * u.yr).value) * u.yr

        run_start = time.time()
        model.run(ics=ic_dict, multiproc_driver=args.multiproc_driver, nprocs=args.nprocs, dts_max=dts_max,
                  t_eval=t_eval)
        run_end = time.time()
        logging.info(f"Model done in {run_end - run_start:0.2f} s")
    return model, mp


def prepare_and_run_model_from_disk(args, ensemble_modeltype=EnsembleModel1D.VSGEnsembleModel1D):
    """
    pepare and run a model given a ModelArgs object (or the parsed arguments from argparse)

    Parameters
    ----------
    args : ModelArgs or Namespace
        the arguments needed; see ModelArgs for details
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model

    Returns
    -------
    model: VSGEnsembleModel1D
        the finished model
    mp: modelparameters
        the model parameters

    """
    model, mp = prepare_model_from_disk(args, ensemble_modeltype=ensemble_modeltype)
    return run_model(args, model, mp)


def prepare_and_run_model_batch(args, return_results=False, ensemble_modeltype=EnsembleModel1D.VSGEnsembleModel1D):
    """
    run models in batchmode. In this case, args.inname specifies a directory, and a model is extracted from each
    subdirectory. args.outdir is interpreted as the prefix of the output directory.

    Parameters
    ----------
    args : ModelArgs or Namespace
        the arguments
    return_results : bool
        return list of results if true
    ensemble_modeltype: child of EnsembleModel1D
        the desired type of model


    Returns
    -------
    allresults: list or None
        the results


    """
    dirname = args.inname
    outprefix = args.outdir
    mycontent = os.listdir(dirname)
    allresults = []
    irun = 0
    from natsort import natsorted
    mycontent = natsorted(mycontent)
    for obj in mycontent:
        p = os.path.join(dirname, obj)
        if os.path.isfile(p):
            continue
        irun += 1
        logging.info(f">>> Running model {irun} <<<")
        args.inname = os.path.join(p, "model_parameters.yaml")
        args.outdir = os.path.join(p, outprefix)
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)
        try:
            model, mp = prepare_and_run_model_from_disk(args, ensemble_modeltype=ensemble_modeltype)
        except Exception as e:
            if args.graceful:
                logging.error(traceback.format_exc())
            else:
                raise e
        else:
            results = [obj, process_results(args, model, mp)]
            if return_results:
                allresults.append(results)
            del model
            del mp
    if return_results:
        return allresults

    return None




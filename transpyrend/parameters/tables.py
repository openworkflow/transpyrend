"""
defines wrapper classes for treating pandas tables, including physical units
"""
import os
import pandas as pd
import numpy as np
import astropy.units as u
from transpyrend.utils.misc import is_hdf5


def read_csv(fname, delimiter=";", decimal=",", **args):
    """
    read a csv file using pandas, make sure that all numbers are interpreted as floats
    Parameters
    ----------
    fname : str
        filename
    delimiter : str
        delimiter
    decimal : str
        decimal separator
    args : dict
        other key word argument to be passed to pandas

    Returns
    -------
    d : pandas.Dataframe
        the read table

    """
    d = pd.read_csv(fname, delimiter=delimiter, decimal=decimal, **args)
    for dtype, col in zip(d.dtypes, d.columns):
        if dtype.char in np.typecodes['AllInteger']:
            d[col] = d[col].astype(np.float64)
    return d


def get_units_from_csv(fname, delimiter=";", decimal=",", skiprows=None):
    """
    read units from a csv file, formatted as by add_units_to_csv

    Parameters
    ----------
    fname : str
        filename
    delimiter : str
        delimiter
    decimal : str
        decimal separator

    Returns
    -------
    units : dict
        the read units

    """
    df = pd.read_csv(fname, nrows=1, delimiter=delimiter, decimal=decimal, index_col=False,
                             keep_default_na=False, skiprows=skiprows)
    read_units = df.values.tolist()[0]
    read_cols = list(df.columns)
    return construct_units(read_cols, read_units)


def construct_units(read_cols, read_units):
    """
    construct a dictionary with information about units in a table from the columns and the units as list of strings
    Parameters
    ----------
    read_cols : list of str
        the columns
    read_units : list of str
        the units

    Returns
    -------
    units : OrderedDict
        the unit info

    """
    if len(read_units) != len(read_cols):
        msg = "table has inconsistent header; number of columns != number of given units"
        raise RuntimeError(msg)
    from collections import OrderedDict
    units = OrderedDict()
    for col, uni in zip(read_cols, read_units):
        uni1 = uni
        is_log = False
        if "log" in uni:
            is_log = True
            uni1 = uni.split(" ")[1]
        if uni1 == "dimensionless" in uni:
            myu = u.dimensionless_unscaled
        elif not uni1:
            myu = None
        else:
            myu = u.Unit(uni1)
        units[col] = {}
        units[col]["unit"] = myu
        units[col]["is_log"] = is_log
    return units


def add_units_to_csv(fname, columns, units, delimiter=";"):
    """
    adds a row with units to a pandas-generated csv file. Line will be placed in the second row.

    Parameters
    ----------
    fname : str
        filename
    columns : list
        the columns in the file
    units : dict
        a dictionary specifying the units
    delimiter : str
        delimiter

    Returns
    -------

    """
    line = get_units_line(columns, delimiter, units)
    with open(fname) as f:
        lines = f.readlines()
    lines.insert(1, line + os.linesep)
    with open(fname, "w") as f:
        for line in lines:
            f.write(line)


def get_units_line(columns, delimiter, units):
    """
    returns a string containing the humand-readable, phyiscal units of a table

    Parameters
    ----------
    columns : list
        the columns in the file
    delimiter : str
        delimiter
    units : dict
        a dictionary specifying the units

    Returns
    -------
    line : str
        the units string

    """
    line = ""
    for i, col in enumerate(columns):
        if i == len(columns)-1:
            delimiter = ""
        uni = units[col]["unit"]
        if uni == u.degree:
            line += "decimal_degrees" + delimiter
        elif uni == u.dimensionless_unscaled or uni is None:
            if col == "log_kf":
                line += "log m/s" + delimiter
            else:
                line += delimiter
        else:
            line += str(uni) + delimiter
    return line


class BaseTable:
    """
    Base class for handling tables with units and some checks on the provided columns.
    """
    def __init__(self, df=None, fname=None, units=None, file_has_units=False,
                 reader_args={}, required_columns=[], allowed_columns=None, incompatible_columns=[],
                 columns=None, prefix=""):
        """

        Parameters
        ----------
        df : pandas.DataFrame
            if given, create table from this dataframe
        fname : str
            filename: if given, create table from this file
        units : dict
            provides information about units
        file_has_units : bool
            if true, assume that file contains units as written by add_units_to_csv
        reader_args : dict
            additional arguments to the pandas csv reader
        required_columns : list
            list of required columns
        allowed_columns : list
            list of allowed columns
        incompatible_columns : list of lists
            list of columns that exclude each other, e.g. if column A and B cannot be present at the same time, the
            argument would read [["A","B"]]
        columns : list
            the list of columns; used when instantiating from scratch
        """
        self.fname = None
        self.units = None
        self.meta = {}
        self.df = None
        assert not (fname is None and df is None)
        if df is not None:
            self.df = df
            if units is not None:
                for key in df.columns:
                    if key not in units:
                        units[key] = {"unit": None, "is_log": False}
        elif fname is not None:
            if is_hdf5(fname):
                self.from_hdf5(fname, prefix)
            else:
                self.from_csv(fname, file_has_units, reader_args)
        else:  # create an empty table with columns given by the columns= argument
            self.df = pd.DataFrame(columns=columns)

        if allowed_columns is not None:
            self.check_allowed_columns(allowed_columns)
        self.check_required_columns(required_columns)
        self.check_incompatible_columns(incompatible_columns)

        # handle units
        if fname is None:
            self.units = units

    def from_hdf5(self, fname, prefix):
        """
        load table from hdf5

        Parameters
        ----------
        fname : str
            filename
        prefix : str
            hdf5 group

        Returns
        -------

        """
        self.fname = fname
        self.df = pd.read_hdf(fname, f"{prefix}/dataframe")
        import h5py
        with h5py.File(fname,"r") as f:
            line = f[prefix].attrs["units"]
        units_list = line.split(";")
        cols = list(self.df.columns)
        self.units = construct_units(cols, units_list)

    def from_csv(self, fname, file_has_units, reader_args):
        """
        load table from csv
        Parameters
        ----------
        fname : str
            csv filename
        file_has_units : bool
            true if the csv file has a line with units
        reader_args : dict
            further arguments for the csv reader

        Returns
        -------

        """
        if file_has_units:
            skiprows = [1]
        else:
            skiprows = None
        df = read_csv(fname, skiprows=skiprows, **reader_args)
        self.fname = fname
        self.df = df
        if file_has_units:
            self.units = get_units_from_csv(fname)

    def check_required_columns(self, cols):
        """
        check if all required columns are present
        Parameters
        ----------
        cols : list
            the required columns

        Returns
        -------

        """
        for col in cols:
            if col not in self.df.columns:
                if self.fname is None:
                    msg = f"Missing required column '{col}' in data!"
                    raise RuntimeError(msg)

                msg = f"Missing required column '{col}' in file {self.fname}!"
                raise RuntimeError(msg)

    def check_allowed_columns(self, cols):
        """
        check if all columns are actually allowed

        Parameters
        ----------
        cols : list
            the allowed columns

        Returns
        -------

        """
        for col in self.df.columns:
            if col not in cols:
                if self.fname is None:
                    msg = f"Column '{col}' not allowed in data!"
                    raise RuntimeError(msg)

                msg = f"Column '{col}' not allowed in file {self.fname}!"
                raise RuntimeError(msg)

    def check_incompatible_columns(self, col_sets):
        """
        check for incompatible columns

        Parameters
        ----------
        col_sets : list of lists
            list of incompatible column sets

        Returns
        -------

        """
        for lst in col_sets:
            ihit = 0
            for col in lst:
                if col in self.df.columns:
                    ihit += 1
                if ihit > 1:
                    if self.fname is None:
                        msg = f"Incompatible column '{col}': only one of {lst} can be given!"
                        raise RuntimeError(msg)

                    msg = (
                        f"Incompatible column '{col}': only one of {lst} "
                        f"can be given in {self.fname}!"
                    )
                    raise RuntimeError(msg)

    def __getitem__(self, key):
        """
        returns column data with units
        Parameters
        ----------
        key : str
            the column

        Returns
        -------
        values : array-like
            the desired values; if they have a physical unit, they are returned as astropy.Quantity object

        """
        unit = None
        values = self.df[key].values
        if self.units is not None:
            unit = self.units[key]["unit"]
        if self.units[key]["is_log"]:
            values = 10**values
        if unit is not None:
            return values * unit

        return values

    def to_csv(self, fname, sep=";", decimal=",", **args):
        """
        store table as csv; store units as well if needed

        Parameters
        ----------
        fname : str
            filename
        sep : str
            delimiter
        decimal : str
            decimal separator
        args : dict
            additional arguments for the pandas csv reader

        Returns
        -------

        """
        self.df.to_csv(fname, sep=sep, decimal=decimal, index=False, **args)
        if self.units is not None:
            add_units_to_csv(fname, self.df.columns.tolist(), self.units)

    def to_hdf5(self, fname, prefix, mode="a"):
        """
        store the table as hdf5 group

        Parameters
        ----------
        fname : str
            filename
        prefix : str
            group name in the file
        mode : str, default: 'a'
            file mode

        Returns
        -------

        """
        self.df.to_hdf(fname, f"{prefix}/dataframe", mode, index=False)
        line = get_units_line(self.df.columns.tolist(), ";", self.units)
        import h5py
        with h5py.File(fname, mode) as f:
            f[prefix].attrs.update({"units": line})


class DiffusionParametersTable(BaseTable):
    """a table for the diffusion-related parameters of transpyrend"""
    def __init__(self, df=None, fname=None, reader_args={}, prefix=""):
        super().__init__(df=df, fname=fname,
                         units=None,
                         file_has_units=True,
                         reader_args=reader_args,
                         required_columns=["nuclide", "nuclide_param_key", "De"],
                         allowed_columns=["nuclide", "element", "nuclide_param_key", "De",
                                         "accessible_porosity"],
                         incompatible_columns=[],
                         columns=["nuclide", "element", "nuclide_param_key", "De"],
                         prefix=prefix)


class InventoryTable(BaseTable):
    """a table for the inventory data of transpyrend"""
    def __init__(self, df=None, fname=None, reader_args={}, prefix=""):

        allowed_columns = ["nuclide","matrix_fraction","cladding_fraction","vitrified_fraction",
                           "instant_release_fraction","inventory"]
        required_columns = allowed_columns
        columns = allowed_columns

        super().__init__(df=df, fname=fname,
                         units=None,
                         file_has_units=True,
                         reader_args=reader_args,
                         required_columns=required_columns,
                         allowed_columns=allowed_columns,
                         incompatible_columns=[],
                         columns=columns,
                         prefix=prefix)

    def from_csv(self, fname, file_has_units, reader_args):
        """
        load table from csv
        Parameters
        ----------
        fname : str
            csv filename
        file_has_units : bool
            true if the csv file has a line with units
        reader_args : dict
            further arguments for the csv reader

        Returns
        -------

        """
        if file_has_units:
            skiprows = [0,2]
        else:
            skiprows = [0]
        df = read_csv(fname, skiprows=skiprows, **reader_args)
        # read the reference time
        with open(fname) as f:
            line = f.readline()
        name, value = line.split("=")
        assert(name.strip()=="t_ref")
        self.meta["t_ref"] = int(value)
        self.fname = fname
        self.df = df
        if file_has_units:
            self.units = get_units_from_csv(fname, skiprows=[0])


class SorptionParametersTable(BaseTable):
    """a table for the sorption parameters of transpyrend"""
    def __init__(self, df=None, fname=None, reader_args={}, prefix=""):
        super().__init__(df=df, fname=fname,
                         units=None,
                         file_has_units=True,
                         reader_args=reader_args,
                         required_columns=["nuclide", "nuclide_param_key", "Kd"],
                         allowed_columns=["nuclide", "element", "nuclide_param_key", "Kd"],
                         incompatible_columns=[],
                         columns=["nuclide", "nuclide_param_key", "Kd"],
                         prefix=prefix)


class GeologicalUnitParametersTable(BaseTable):
    """a table for the parameters related to geological units in transpyrend"""
    def __init__(self, df=None, fname=None, reader_args={}, prefix=""):
        allowed_columns = ["profile_name", "profile_latitude", "profile_longitude", "geological_unit_name",
                           "geological_unit_code", "thickness", "bulk_density", "nuclide_param_key_sorption",
                           "nuclide_param_key_diffusion", "porosity", "log_kf", "darcy_flux", "hydraulic_gradient"]
        required_columns = ["geological_unit_name",
                            "geological_unit_code", "thickness", "bulk_density", "nuclide_param_key_sorption",
                            "nuclide_param_key_diffusion", "porosity", "log_kf"]
        columns = ["geological_unit_name",
                   "geological_unit_code", "thickness", "bulk_density", "nuclide_param_key_sorption",
                   "nuclide_param_key_diffusion", "porosity", "log_kf"]
        super().__init__(df=df, fname=fname,
                         units=None,
                         file_has_units=True,
                         reader_args=reader_args,
                         required_columns=required_columns,
                         allowed_columns=allowed_columns,
                         incompatible_columns=[
                             ["darcy_flux", "hydraulic_head_gradient"]],
                         columns=columns,
                         prefix=prefix)

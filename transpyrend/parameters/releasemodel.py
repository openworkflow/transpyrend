"""
implements tools for dealing with the parameters of the release model
"""
import os

import numpy as np
import logging
from astropy import units as u
from transpyrend.release.release import CompositeReleaseModel, BaseReleaseModel

_BASE_PATH = os.path.dirname(os.path.abspath(__file__))
_INPUTS_PATH = os.path.join(_BASE_PATH, "../../input_parameters")
_DEFAULT_INVENTORY_PATH = "release/default/default_inventory.csv"


class CompositeReleaseModelParameters:
    """
    Class to handle parameters for the composite release model.

    The yaml file may look like this:

    >>> model:  test1
    ... date:   25.10.22
    ... author: ChB
    ... comment: this is a short test of what yaml can do for us
    ... nuclides: VSG
    ... ignore_decay: False
    ... nuclide_database: VSG
    ... release_models:
    ... - model1: file1.yaml
    ... - model2: file2.yaml
    ... - model3: file3.yaml
    ... weights:
    ...     - 1.
    ...     - 2.
    ...     - 0.5
    """

    def __init__(self, filename=None):
        self.names = []
        self.weights = []
        # there are arguments that need to used across all individual release models consistently;
        # they are collected here.
        self.shared_arguments = ["nuclides", "ignore_decay", "nuclide_database"]
        self.nuclides = None
        self.ignore_decay = None
        self.nuclide_database = None

        self.author = None
        self.comment = None
        self.date = None
        self.release_models = None

        if filename is not None:
            self.load(filename)

    def load(self, filename):
        import yaml
        import os
        with open(filename) as f:
            data = yaml.load(f, Loader=yaml.Loader)
        if isinstance(data["nuclides"], str):
            from transpyrend.utils.nuclides import vsg_tab411_data
            data["nuclides"] = list(vsg_tab411_data.keys())
        models = data["release_models"]
        self.names = []
        self.release_models = []
        for model in models:
            assert len(list(model.keys())) == 1
            for name, fname in model.items():
                self.names.append(name)
                if os.path.isabs(fname):
                    path = fname
                else:
                    path = os.path.join(os.path.dirname(filename), fname)
                self.release_models.append(ReleaseModelParameters(path))
        self.weights = data["weights"]
        for key in self.shared_arguments:
            if key not in data:
                raise RuntimeError("Missing argument " + key + " in CompositeRelaseModelParameters")
            setattr(self, key, data[key])
        for key in ["author", "comment", "date", "model"]:
            if key in data:
                setattr(self, key, data[key])

    def make_args(self):
        return {"model_params": self.release_models, "names": self.names, "weights": self.weights,
                "ignore_decay": self.ignore_decay, "nuclide_database": self.nuclide_database, "nuclides": self.nuclides}

    def get_total(self, quantity, storage_year, nuclide_scheme):
        if quantity == "amount":
            total = 0. * u.mol
        elif quantity == "mass":
            total = 0. * u.kg
        for m, w in zip(self.release_models, self.weights):
            total += w * m.get_total(quantity, storage_year, nuclide_scheme)
        return total


class ReleaseModelParameters:
    """
    Class to handle the parameters for the release model as read from disk as a yaml file.

    The yaml file may look like this:

    >>> model:  test1
    ... date:   22.07.22
    ... author: ChB
    ... comment: this is a short test of what yaml can do for us
    ...
    ... nuclides: VSG
    ...
    ...
    ... release_parameters:
    ...    breaching_time: 1e4 yr
    ...    matrix_release_rate: 1e-6/yr
    ...    cladding_release_rate: 1e-5/yr
    ...    vitrified_lifetime: 1e5 yr
    ...
    ... nuclide_parameters:
    ...     from_file: nuclide_parameters.csv
    ...
    ... #options:
    ... ignore_decay: False
    ... nuclide_database: VSG

    """

    def __init__(self, filename=None):
        """

        Parameters
        ----------
        filename : str
            path to yaml file with the parameters. If None, an empty object will be constructed.
        """

        self.nuclides = None
        self.release_parameters = None
        self.ignore_decay = None
        self.nuclide_database = None
        self.nuclide_parameters = None

        self.author = None
        self.comment = None
        self.date = None
        self.model = None
        self.reference_year = None

        if filename is not None:
            self.load(filename)

    def load_csv(self, filename):
        """
        load the nuclide parameters from a csv files

        Parameters
        ----------
        filename : str
            the filename

        """
        from .tables import InventoryTable
        inv = InventoryTable(fname=filename)
        self.reference_year = inv.meta["t_ref"]
        nuclides = list(inv["nuclide"])
        data = {}
        for nuclide in nuclides:
            data[nuclide] = {}
            row = nuclides.index(nuclide)
            for column in ["matrix_fraction", "cladding_fraction", "vitrified_fraction", "inventory",
                           "instant_release_fraction"]:
                data[nuclide][column] = inv[column][row]

        return data

    def make_args(self):
        return {"nuclides": self.nuclides, "nuclide_parameters": self.nuclide_parameters,
                "release_parameters": self.release_parameters, "nuclide_database": self.nuclide_database,
                "ignore_decay": self.ignore_decay, "reference_year": self.reference_year}

    def load(self, filename):
        """
        load parameters from a yaml file

        Parameters
        ----------
        filename : str
            the filename

        """
        import yaml
        import os
        self.filename = filename
        with open(filename) as f:
            data = yaml.load(f, Loader=yaml.Loader)
        if "from_file" in data["nuclide_parameters"]:
            # create an absolute path
            loc = data["nuclide_parameters"]["from_file"]
            if os.path.isabs(loc):
                fname = loc
            else:
                my_dir = os.path.dirname(filename)
                fname = os.path.join(my_dir, loc)
            self.nuclide_parameters = self.load_csv(fname)

            self.nuclide_parameters_filename = fname
        else:
            raise NotImplementedError
        if isinstance(data["nuclides"], str):
            from transpyrend.utils.nuclides import vsg_tab411_data
            data["nuclides"] = list(vsg_tab411_data.keys())
        elif not isinstance(data["nuclides"], list):
            raise NotImplementedError
        for parameter in data["release_parameters"]:
            v = data["release_parameters"][parameter]
            v = u.Quantity(v)
            data["release_parameters"][parameter] = v
        for key in ["nuclide_database", "ignore_decay", "nuclides", "release_parameters", "author", "comment", "date",
                    "model"]:
            if key in data:
                setattr(self, key, data[key])

    @classmethod
    def get_total_for_instant_release(cls, quantity, storage_year, nuclide_scheme, fname=None):
        """
        return the total amount or mass in a given nuclide_parameters.csv file. Used for the instant release model only.

        Parameters
        ----------
        quantity : str
            either "mass" or "amount"
        storage_year : int
            the year in which storage begins. Only used to evolve the inventory from the reference time
            to the storage year.
        nuclide_scheme : str
            the nuclide scheme to use, e.g. "VSG"
        fname : str
            filename of csv file. If None, use default.

        Returns
        -------
        total : astropy.units.Quantity
            the total amount/mass in the deposit

        """

        from transpyrend.utils.nuclides import mol2mass
        from .tables import InventoryTable

        if fname is None:
            fname = os.path.join(_INPUTS_PATH, _DEFAULT_INVENTORY_PATH)
        iv = InventoryTable(fname=fname)

        nucs = list(iv["nuclide"])
        mols = iv["inventory"]
        inv = dict(zip(nucs, mols))

        reference_year = iv.meta['t_ref']

        inv = cls.evolve_inventory_to_storage_year(inv, nuclide_scheme, reference_year, storage_year)

        nucs = []
        mols = []
        for nuc, mol in inv.items():
            unit = mol.unit
            nucs.append(nuc)
            mols.append(mol.value)
        mols *= unit

        if quantity == "amount":
            total = sum(mols)
        elif quantity == "mass":
            masses = mol2mass(mols, nucs, units=u.kg)
            total = sum(masses)
        else:
            msg = f"quantity must be mass or amount: got {quantity}"
            raise NotImplementedError(msg)

        return total

    @classmethod
    def get_ics_for_instant_release(cls, nuclides, H_repo_eff, A_repo_eff, width, N, deposit_index, storage_year,
                                    nuclide_scheme, return_dict=True, fname=None):
        """
        return array with initial conditions for a given inventory, in the limit of an IRF=1.
        Will rescale concentrations according to the specified area and height of the deposit. Note: T
        here is no check on consistency between cell
        volume and repository volume. Typically, it should be the case that
        with cell_volume = dx * cell_area:
        cell_volume * width == A_repo * H_repo
        If these conditions are not met, mass conservation might fail.

        Parameters
        ----------
        nuclides : list
            the nuclides considered
        H_repo_eff : astropy.units.Quantity
            the effective height of the deposit, i.e. the extension in z-direction
        A_repo_eff : astropy.units.Quantity
            the area of the deposit perpendicular to the orientation of the transport model, i.e. the lateral area
            if the direction of transport is +-z
        width : int
            width of the deposit in cells along the direction of transport
        N : int
            number of cells
        deposit_index : int
            cell index at which the deposit begins, i.e. the lowest cell index that contains the deposit
        storage_year : int
            the year in which storage begins. Only used to evolve the inventory from the reference time
            to the storage year.
        nuclide_scheme : str
            the nuclide scheme to use, e.g. "VSG"
        return_dict : bool
            whether we return a dictionary with the nuclides as keys
        fname : str
            filename of the nuclide_parameters file. If None, fall back to default.

        Returns
        -------
        data : dict or astropy.units.Quantity
            the ICs

    """
        from .tables import InventoryTable
        repo_volume = A_repo_eff * H_repo_eff
        if fname is None:
            fname = os.path.join(_INPUTS_PATH, _DEFAULT_INVENTORY_PATH)
        iv = InventoryTable(fname=fname)

        nucs = list(iv["nuclide"])
        mols = iv["inventory"]
        inv = dict(zip(nucs, mols))

        reference_year = iv.meta['t_ref']
        logging.info(f"Inventory is at reference year {reference_year}")

        inv = cls.evolve_inventory_to_storage_year(inv, nuclide_scheme, reference_year, storage_year, True)

        bcs = []
        d = {}
        for nuc in nuclides:
            if nuc not in nucs and nuc != "stable":
                msg = "You asked for a nuclide that is not in the inventory"
                raise RuntimeError(msg)
            vec = np.zeros(N) * u.mol / u.m ** 3
            if nuc == "stable":
                density = 0. * u.mol / u.m ** 3
            else:
                density = (inv[nuc] / repo_volume).to(u.mol / u.m ** 3)
            vec[deposit_index:deposit_index + width] = density
            d[nuc] = vec
            bcs.append(vec)
        if return_dict:
            return d

        return bcs

    @classmethod
    def evolve_inventory_to_storage_year(cls, inv, nuclide_scheme, reference_year, storage_year, verbose=False):
        """
        if needed, age the provided inventory

        Parameters
        ----------
        inv : dict
            the inventory
        nuclide_scheme : str
            the nuclide scheme, e.g. "VSG"
        reference_year : int
            the reference year of the inventory
        storage_year : int
            the storage year
        verbose: bool
            be verbose

        Returns
        -------
        inv : dict
            the aged inventory
        """
        if reference_year is None:
            deltayr = 0.
        else:
            deltayr = storage_year - reference_year
        if deltayr < 0:
            msg = "Storage cannot start prior to reference year of the inventory!"
            raise RuntimeError(msg)
        if deltayr != 0:
            if verbose:
                logging.info(f"Storage year differs from inventory reference. Need to age the inventory by {deltayr} yr.")
            from transpyrend.utils.nuclides import evolve_to_age
            return evolve_to_age(inv, deltayr * u.yr, nuclide_scheme=nuclide_scheme, quantity="amount")
        return inv

    def get_total(self, quantity, storage_year, nuclide_scheme):
        return ReleaseModelParameters.get_total_for_instant_release(quantity, storage_year, nuclide_scheme,
                                                                    fname=self.nuclide_parameters_filename)


def read_release_model(fname, storage_year=None):
    """
    read a yaml file specifying a release model and return a BaseReleaseModel or a CompositeReleaseModel,
    depending on the file.

    Parameters
    ----------
    fname : str
        the yaml file
    storage_year : astropy.units.Quantity or None
        the designated storage year. If None, assume it is the same as the reference year of the inventory.

    Returns
    -------
    model : BaseReleaseModel/CompositeReleaseModel
    """
    params = read_release_model_parameters(fname)
    if isinstance(params, CompositeReleaseModelParameters):
        return CompositeReleaseModel(**params.make_args(), storage_year=storage_year)

    return BaseReleaseModel(**params.make_args(), storage_year=storage_year)


def read_release_model_parameters(fname):
    """
    read a yaml file specifying a release model and return  ReleaseModelParameters or CompositeReleaseModelParameters,
    depending on the file.

    Parameters
    ----------
    fname : str
        the yaml file

    Returns
    -------
    model : CompositeReleaseModelParameters/ReleaseModelParameters
    """
    import yaml
    with open(fname) as f:
        data = yaml.load(f, Loader=yaml.Loader)
    if "release_models" in data:
        return CompositeReleaseModelParameters(fname)

    return ReleaseModelParameters(fname)

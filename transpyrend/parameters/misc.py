import astropy.units as u


def _convert_layer(df, df_layer):
    """
    convert dataframes containing information about the parameters in a single geological units to the corresponding
    entries of the parameters_rock and parameter_transport_material arguments in TransPyREnd


    Parameters
    ----------
    df : pandas.DataFrame
        the dataframe with the parameters of the geological layer
    df_layer : pandas.DataFrame
        the dataframe with the transport parameters of the nuclides in that layer


    Returns
    -------
    new_layer : dict
        the entry for the parameters_rock object
    new_params : dict
        the entry for the parameters_transport_material object

    """
    # CAUTION: this assumes some specific units!
    new_layer = {}
    new_layer["full_name"] = df_layer["geological_unit_name"].values[0]
    new_layer["name"] = df_layer["geological_unit_code"].values[0]
    new_layer["thickness"] = df_layer["thickness"].values[0] * u.m
    new_layer["bulk_density"] = df_layer["bulk_density"].values[0] * u.kg /u.m**3
    new_layer["porosity"] = df_layer["porosity"].values[0] * u.kg /u. m**3

    new_layer["hydraulic_conductivity"] = 10**df_layer["log_kf"].values[0] * u. m /u.s
    if "darcy_flux" in df_layer.columns:
        new_layer["head_gradient"] = df_layer["darcy_flux"].values[0 ] /new_layer["hydraulic_conductivity"].value
    else:
        new_layer["head_gradient"] = df_layer["head_gradient"].values[0]

    new_params = {}
    for _, row in df.iterrows():
        d = {"effective_diffusion_coefficient": row["De"] * u.m ** 2 /u.s,
             "accessible_porosity": row["accessible_porosity"],
             "sorption_coefficient": row["Kd"] * u.m** 3 /u.kg}
        new_params[row["nuclide"]] = d
    return new_layer, new_params

def convert_layers(df, df_layers):
    """
    convert the geological and transport parameters coming from parameterworkflow's run_workflow() routine directly to
    the arguments parameters_rock and parameters_transport_material used in TransPyREnd

    Parameters
    ----------
    df : list
        list of datamframes with the info about the geological layers
    df_layery : list
        list of dataframes with the nuclide transport parameters in each layer


    Returns
    -------
    parameters_rock : list
        the parameters_rock object
    parameters_transport_material : list
        the parameters_transport_material object
    """

    parameters_rock = []
    parameters_transport_material = []

    for i in range(len(df)):
        layer, params = _convert_layer(df[i], df_layers[i])
        parameters_rock.append(layer)
        parameters_transport_material.append(params)
    return parameters_rock, parameters_transport_material

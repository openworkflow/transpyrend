"""contains tools to deal with model parameters"""
import os
import astropy.units as u
import numpy as np
from transpyrend.parameters.parametersets import ParameterSet

_base_path = os.path.dirname(os.path.abspath(__file__))
# this is copied from the parameter workflows...
def add_units_to_csv(fname, line):
    """
    adds a row with units to a pandas-generated csv file. Line will be placed in the second row.
    """
    with open(fname) as f:
        lines = f.readlines()
    lines.insert(1, line + os.linesep)
    with open(fname,"w") as f:
        for line in lines:
            f.write(line)


class TransPyREndParameters(ParameterSet):
    """
    a class to read the yaml file controlling transpyrend runs. Supersedes ModelParameters.

    Parameters
    ----------
    filename : str
        if given, initialize from yaml file on disk
    parameterset : ParameterSet
        if given, initialize from a ParameterSet object
    dx : astropy.units.Quantity
        the distance between nodes
    profile_name : str
        the name of the geological profile
    width_repository : astropy.units.Quantity
        the width of the repository
    height_repository : astropy.units.Quantity
        the height of the repository
    area_repository : astropy.units.Quantity
        the area of the repository
    location_repository : astropy.units.Quantity
        the location of the repository
    """
    """list of parameters that each configuration needs to specify"""
    _required_args = ["dx", "width_repository", "height_repository", "location_repository",
                      "model_orientation", "diffusion_parameters", "sorption_parameters", "geological_layers"]
    """list of optional parameters"""
    _optional_args = ["profile_latitude", "profile_longitude", "profile_name", "time_interval", "area_repository",
                      "mass_transport_threshold", "cfl_limit", "hl_limit", "dt_max", "output_interval", "snap_to_units",
                       "use_bateman", "release_model", "include_decay", "include_sorption", "storage_year"]
    """list of allowed meta parameters"""
    _meta_data_args = ["date", "model", "comment", "author"]
    """all allowed parameters"""
    _allowed_args = _meta_data_args + _required_args + _optional_args
    """list of parameters that have physical units"""
    _args_with_units = ["dx", "width_repository", "height_repository", "area_repository", "location_repository",
                        "time_interval", "area_repository", "dt_max", "output_interval"]
    """list of parameters that should be parsed to associated csv files"""
    _arg_tables = ["geological_layers", "diffusion_parameters", "sorption_parameters"]
    """list of arguments that are floats"""
    _float_args = ["cfl_limit", "hl_limit", "mass_transport_threshold"]

    def __init__(self, filename=None, parameterset=None, dx=None, profile_name=None, width_repository=None,
                 height_repository=None, area_repository=None, location_repository=None, model_orientation=None,
                  read_shallow=False):
        self.profile_name = None
        self.profile_latitude = None
        self.profile_longitude = None
        self.time_interval = 1e6 * u.yr
        self.dx = None
        self.area_repository = 1 * u.m ** 2
        self.width_repository = None
        self.height_repository = None
        self.location_repository = None
        self.mass_transport_threshold = 1e-4
        self.cfl_limit = 0.25
        self.hl_limit = 0.5
        self.dt_max = -1 * u.yr
        self.output_interval = 1000 * u.yr
        self.storage_year = 2075
        self.model_orientation = None
        self.snap_to_units = True
        self.files = {}
        self.use_bateman = True
        self.release_model = None
        self.include_decay = True
        self.include_sorption = True
        self.store_flux = False

        if filename is not None:
            self.from_file(filename)
            if read_shallow:
                return
            super().__init__(init_from="file", input_parameter_directory=None,
                             rock_parameter_file=self.files["geological_layers"],
                             nuclide_sorption_file=self.files["sorption_parameters"],
                             nuclide_diffusion_file=self.files["diffusion_parameters"])
        elif parameterset is not None:
            super().__init__(init_from="copy",obj=parameterset)
            self.profile_name = profile_name
            self.dx = dx
            self.width_repository = width_repository
            self.height_repository = height_repository
            self.location_repository = location_repository
            self.model_orientation = model_orientation
            self.files = {}
        else:
            raise NotImplementedError
        self.setup_grid()

    @property
    def x(self):
        """
        the node positions in m
        """
        self.setup_grid()
        return self._x

    @property
    def N(self):
        """
        the number of nodes
        """
        self.setup_grid()
        return self._N

    def setup_grid(self):
        if self.snap_to_units is False:
            xmax = self.get_xmax()
            self._N = int(xmax / self.dx) + 1
            # find cell sizes for nodes
            self._x = self.dx * np.arange(0, self._N)
        else:
            xmax = self.get_xmax()
            self._N = int(xmax / self.dx) + 1
            # find cell sizes for nodes
            x = self.dx * np.arange(0, self._N)
            self._x = self.snap_grid(x)

    def from_file(self, filename):
        self.load_yaml(filename)

    def load_yaml(self, filename):
        import yaml
        import os
        with open(filename) as f:
            data = yaml.load(f, Loader=yaml.Loader)
        self._check_required(data, filename)
        for key in data:
            if key not in self._allowed_args:
                msg = f"{key} is not an allowed argument in parameter file!"
                raise RuntimeError(msg)
            if key in self._arg_tables:
                self._handle_arg_tables(data, filename, key)
            else:
                if key in self._args_with_units:
                    setattr(self, key, u.Quantity(data[key]))
                elif key in self._float_args:
                    setattr(self, key, float(data[key]))
                else:
                    # convert "None" entries to None
                    if data[key] == "None":
                        data[key] = None
                    # fix relative pothes
                    if key == "release_model":
                        fname = data[key]
                        if fname is None or os.path.isabs(fname):
                            pass
                        else:
                            abspath = os.path.abspath(filename)
                            yamldir = os.path.dirname(abspath)
                            data[key] = os.path.join(yamldir, fname)
                    if data[key] == "None":
                        data[key] = None
                    setattr(self, key, data[key])

    def _handle_arg_tables(self, data, filename, key):
        arg = data[key]
        if "from_file" in arg:
            fname = arg["from_file"]
            if os.path.isabs(fname):
                self.files[key] = fname
            else:
                abspath = os.path.abspath(filename)
                yamldir = os.path.dirname(abspath)
                self.files[key] = os.path.join(yamldir, fname)

        else:
            msg = "a filename must be passed"
            raise NotImplementedError(msg)

    def _check_required(self, data, filename):
        for key in self._required_args:
            if key not in data:
                msg = f"missing parameter {key} in {filename}!"
                raise RuntimeError(msg)

    def snap_grid(self, x):
        """
        align the grid points x with the geological units in the problem

        Parameters
        ----------
        x : astropy.units.Quantity
            the initial, regular grid

        Returns
        -------
        x_snap : astropy.units.Quantity
            the aligned grid

        """
        x_snap = x[:]
        xcum = 0. * u.m
        for unit in self.parameters_rock:
            xcum += unit["thickness"]
            # find the closest regular grid point to the right edge of this unit
            delta = abs(x_snap - xcum)
            imin = np.where(delta == delta.min())[0][0]
            x_snap[imin] = xcum
        assert (np.all(np.diff(x_snap) > 0.))
        return x_snap

    def get_unit_names(self):
        """
        return the names of the geological units
        """
        return [unit["name"] for unit in self.parameters_rock]

    def get_unit_thicknesses(self, cumulative=False):
        """
        return a vector with thicknesses in each layer
        """
        thickness = [unit["thickness"].to(u.m).value for unit in self.parameters_rock] * u.m
        if cumulative:
            cumu = np.zeros(thickness.size + 1) * u.m
            cumu[1:] = thickness
            for i in range(1, cumu.size):
                cumu[i] += cumu[i - 1]
            return cumu
        return thickness

    def get_xmax(self):
        """
        get the total extent of the model
        """
        t = self.get_unit_thicknesses()
        return t.sum()

    def find_spatial_index(self, x):
        """
        find the index of the node close to position x
        Parameters
        ----------
        x - astropy.units.Quantity

        Returns
        -------

        """
        if x > self.x[-1]:
            msg = "x > grid size"
            raise RuntimeError(msg)
        delta = abs(self.x - x)
        indizes = np.where(delta == delta.min())[0]
        # we could obtain two nodes with the same distance to x here. In that case, always choose the upper node.
        if len(indizes) > 2:
            msg = "Could not locate cell!"
            raise NotImplementedError(msg)
        if len(indizes) == 2:
            if self.x[indizes[0]] - x > 0.0 * u.m:
                return indizes[0]
            return indizes[1]

        return indizes[0]


    def get_deposit_position(self):
        """
        returns the left edge and width of the deposit in cell indizes. If width=1, left edge and center are the same.
        """
        left_index = self.find_spatial_index(self.location_repository)
        width_index = self.find_spatial_index(self.location_repository + self.width_repository) - left_index
        return left_index, width_index

    def set_darcy_velocities(self, q):
        """
        set the same darcy velocity to all geological units by changing the hydraulic gradient in each layer
        according to:
        dhdx = q/K
        with K the conductivity.
        """
        for unit in self.parameters_rock:
            unit["head_gradient"] = (q / unit["hydraulic_conductivity"]).to(u.dimensionless_unscaled)

    def get_darcy_velocity(self, unit):
        """
        return the darcy velocity q = dhdx * K for a given unit
        """
        units = self.get_unit_names()
        for ui, name in enumerate(units):
            if unit == name:
                uii = ui
                break
        return self.parameters_rock[uii]["hydraulic_conductivity"] * self.parameters_rock[ui]["head_gradient"]

    def set_darcy_velocities_from_unit(self, unit, dhdx_in_unit=None):
        """
        set the darcy velocities in all units to be the same as in one specific unit by changing, for each unit,
        the hydraulic gradient. If dhdx_in_unit is set, it will be used as the head gradient in the reference unit.
        """
        units = self.get_unit_names()
        for ui, name in enumerate(units):
            if unit == name:
                uii = ui
                break
        if dhdx_in_unit is not None:
            self.parameters_rock[uii]["head_gradient"] = dhdx_in_unit
        q = self.get_darcy_velocity(unit)
        self.set_darcy_velocities(q)

    def write_file(self, fname):
        """
        writes a model parameters yaml file.

        Parameters
        ----------
        fname : str
            file name

        Returns
        -------

        """
        import yaml
        data = {}
        for key in self._allowed_args:
            if key in self._arg_tables:
                # CAREFUL this will write the absolute pathes to these files. We might run into trouble with this later one
                data[key] = {"from_file": self.files[key]}
            else:
                if hasattr(self, key):
                    v = getattr(self, key)
                    if isinstance(v, u.Quantity):
                        v = f"{v}"
                    data[key] = v
        with open(fname, "w") as f:
            yaml.dump(data, f, sort_keys=False)

    def round_down_unit_thicknesses(self, multiple_of=1 * u.m):
        """
        rounds the thickness of each unit down to be a multiple of some number.
        """
        for unit in self.parameters_rock:
            times_thickness = int((unit["thickness"] / multiple_of).to(u.dimensionless_unscaled))
            if times_thickness == 0:
                msg = f"Rounding down of unit thickness: layer {unit['name']} has thickness zero!"
                raise RuntimeError(msg)
            thickness = times_thickness * multiple_of.to(u.m)
            unit["thickness"] = thickness
        self.setup_grid()

    def add_stable_species(self):
        """
        add a 'stable' species to the model parameters.
        """
        for unit in self.parameters_transport_material:
            unit["stable"] = {}

    def add_stop_blocks(self):
        """
        add two cells at left and right edge with diffusion coefficient zero und conductivity zero.
        Returns
        -------
        """
        params_rock_stop = {
            "hydraulic_conductivity": 0 * u.m / u.s,
            "porosity": 1.0,
            "bulk_density": 0 * u.kg / u.m ** 3,
            "thickness": 2 * self.dx,
            "head_gradient": 0.,
            "name": "__none__"
        }
        params_material_stop = {}
        for species in self.parameters_transport_material[0]:
            params_material_stop[species] = {
                "effective_diffusion_coefficient": 0 * u.m ** 2 / u.s,
                "sorption_coefficient": 0. * u.m ** 3 / u.kg
            }
        self.parameters_rock = [params_rock_stop, *self.parameters_rock, params_rock_stop]
        self.parameters_transport_material = [params_material_stop, *self.parameters_transport_material,
                                              params_material_stop]


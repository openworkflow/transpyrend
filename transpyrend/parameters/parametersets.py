"""implements parameter set objects to deal with parameters"
"""
import logging
import os.path

import astropy.units as u

from transpyrend.utils.nuclides import vsg_tab411_data

_base_path = os.path.dirname(os.path.abspath(__file__))
_base_model_path = os.path.normpath(os.path.split(_base_path)[0])


class BaseParameterSet:
    """
    the base class for a set of parameters for a 1D transport simulation.
    """

    def __init__(self):
        # parameters related to the geological units
        self.parameters_rock = None
        # parameters related to the material transported in the model
        self.parameters_transport_material = None
        # name of the parameter set
        self.name = ""
        # source of the parameter set
        self.source = ""
        # a description of the model
        self.description = ""


class ParameterSet(BaseParameterSet):
    """
    a parameter set for a 1D transport simulation

    Parameters
    ----------
    init_from : str
        either "scratch", "copy", or "file"
    nuclides: list of str
        list of nuclides to consider
    rock_parameter_file : str
        file name for the rock parameters
    nuclide_sorption_file : str
        file name for the sorption data
    nuclide_diffusion_file : str
        file name for the diffusion data
    parameters_rock : dict or list of dicts
        rock parameters to generate the parameter set from
    parameters_transport_material : dict or list of dicts
        material parameters to generate the parameter set from
    obj: ParameterSet
        used if init_from="copy"
    """

    def __init__(self, init_from="scratch",
                 nuclides=None,
                 input_parameter_directory=None,
                 rock_parameter_file="geo_profile.csv",
                 nuclide_sorption_file="nuclide_sorption_parameters.csv",
                 nuclide_diffusion_file="nuclide_diffusion_parameters.csv",
                 parameters_rock=None, parameters_transport_material=None, obj=None):
        super().__init__()
        if nuclides is None:
            nuclides = list(vsg_tab411_data.keys())

        if init_from not in ["scratch", "db", "file", "copy"]:
            msg = "init_from must be in ", ["scratch", "db", "file", "copy"]
            raise NotImplementedError(msg)

        if init_from == "scratch":
            self.name = "test"
            self.source = "test_param_set"
            self.description = "dataset to test numerical stability with discontinuous diffusivity"
            self.parameters_rock = parameters_rock
            self.parameters_transport_material = parameters_transport_material
            self.nuclides = nuclides

        elif init_from == "db":
            raise NotImplementedError()
            # whatever we need to do to fill this object with data from a database

        elif init_from == "file":
            input_parameter_path = input_parameter_directory

            if input_parameter_directory is not None:
                rock_parameter_path = os.path.join(input_parameter_path, rock_parameter_file)
                nuclide_sorption_path = os.path.join(input_parameter_path, nuclide_sorption_file)
                nuclide_diffusion_path = os.path.join(input_parameter_path, nuclide_diffusion_file)
            else:
                assert os.path.isabs(rock_parameter_file)
                rock_parameter_path = rock_parameter_file
                assert os.path.isabs(nuclide_sorption_file)
                nuclide_sorption_path = nuclide_sorption_file
                assert os.path.isabs(nuclide_diffusion_file)
                nuclide_diffusion_path = nuclide_diffusion_file

            self.rock_parameter_file = rock_parameter_path
            self.nuclide_sorption_file = nuclide_sorption_path
            self.nuclide_diffusion_file = nuclide_diffusion_path

            # set parameters for geological units
            self.geological_units, self.parameters_rock = self.load_geological_unit_parameters(self.rock_parameter_file)

            # set up dictionary for nucldie specific parameters (sorption & diffusion coefficient)
            self.parameters_transport_material = []
            self.load_sorption_parameters(nuclides, self.nuclide_sorption_file)
            self.load_diffusion_parameters(nuclides, self.nuclide_diffusion_file)




        elif init_from == "copy":
            self.from_copy(obj)

    def load_geological_unit_parameters(self, rock_parameter_file,
                                        correct_nan_gradients=True):
        """
        Load geological unit parameters

        Parameters
        ----------
        rock_parameter_file : string
            the csv file containing the geological unit parameters
        correct_nan_gradients : bool, default=True
            if True change set hydraulic gradients for units without data to 0


        Returns
        -------
        parameters_rock : list of dictionaries
            parameters per geological unit. list of dictionaries, one entry per geological unit

        """
        from .tables import GeologicalUnitParametersTable
        layers = GeologicalUnitParametersTable(fname=rock_parameter_file)
        df = layers.df
        df = df.set_index("geological_unit_code", drop=False)
        geological_units = list(df["geological_unit_code"])

        # parameters for geological units
        parameters_rock = []

        # dictionary with column names for each parameter, and whether or not to log-transform the params
        param_columns = {
            "full_name":
                {"column": "geological_unit_name"},
            "name":
                {"column": "geological_unit_code"},
            "thickness":
                {"column": "thickness"},
            "bulk_density":
                {"column": "bulk_density"},
            "porosity":
                {"column": "porosity"},
            "head_gradient":
                {"column": "hydraulic_gradient"},
            "hydraulic_conductivity":
                {"column": "log_kf"},
            "sorption_key":
                {"column": "nuclide_param_key_sorption"},
            "diffusion_key":
                {"column": "nuclide_param_key_diffusion"},
            "darcy_flux":
                {"column": "darcy_flux"}
        }

        # add parameters for each geological unit
        for geo_unit in geological_units:
            p = {}
            for rpk in param_columns:
                n = param_columns[rpk]["column"]
                if n in df.columns:
                    v = df.loc[geo_unit, n]
                    if layers.units[n]["is_log"] is True:
                        if isinstance(v, int):
                            v = float(v)
                        try:
                            v = 10. ** v
                        except:
                            logging.error(f"something went wrong when reading column {n} with value {v}")
                            raise

                    v_unit = layers.units[n]["unit"]
                    if v_unit is not None:
                        v = v * v_unit
                    p[rpk] = v
                else:
                    p[rpk] = None
            parameters_rock.append(p)
        if "darcy_flux" in df.columns:
            for i, _ in enumerate(geological_units):
                parameters_rock[i]["head_gradient"] = parameters_rock[i]["darcy_flux"] / parameters_rock[i][
                    "hydraulic_conductivity"]
        # change nan hydraulic gradients to zero
        if correct_nan_gradients is True:
            for i, _ in enumerate(geological_units):
                if parameters_rock[i]["head_gradient"] is None:
                    parameters_rock[i]["head_gradient"] = 0.0

        return df["geological_unit_code"], parameters_rock

    def load_sorption_parameters(self, nuclides, nuclide_sorption_file):

        """
        Load the sorption coefficient values for each nuclide and each geological unit from a file

        Parameters
        ----------
        nuclides: list of str
            list of nuclides to consider
        nuclide_sorption_file : string
            filename of the csv file to read nuclide sorption data from


        """
        if self.parameters_transport_material == []:
            self.parameters_transport_material = [{} for i in range(len(self.geological_units))]
        Kd_default = 0 * u.m ** 3 / u.kg
        from .tables import SorptionParametersTable
        sorptionf = SorptionParametersTable(fname=nuclide_sorption_file)
        df = sorptionf.df
        # find element if only nuclide is given
        if "element" not in df.columns:
            nuclides_dfs = df["nuclide"]
            elements = [n.split("-")[0] for n in nuclides_dfs]
            df["element"] = elements
        for i in range(len(nuclides)):
            if "(" in nuclides[i]:
                nuclides[i] = nuclides[i].split("(")[0]

        # go through geological units and nuclides and assign Kd values
        for i, geo_unit in enumerate(self.geological_units):
            nkey = self.parameters_rock[i]["sorption_key"]
            if nkey == "_none_":
                for n in nuclides:
                    self.parameters_transport_material[i][n]["sorption_coefficient"] = 0.0 * u.m ** 3 / u.kg
            else:
                if nkey not in df["nuclide_param_key"].values:
                    msg = (
                        f"nuclide_param_key {nkey} for geological unit "
                        f"{geo_unit} not found in sorption file!"
                    )
                    raise RuntimeError(msg)
                dfsi = df.loc[df["nuclide_param_key"] == nkey]
                for n in nuclides:
                    element = n.split("-")[0]
                    if n not in self.parameters_transport_material[i]:
                        self.parameters_transport_material[i][n] = {}
                    if element in dfsi["element"].values:
                        self.parameters_transport_material[i][n]["sorption_coefficient"] = \
                            dfsi.loc[df['element'] == element]["Kd"].values[0] * u.m ** 3 / u.kg
                    else:
                        logging.warning(f"warning, could not find sorption data for at least one element, "
                                     f"assigning default Kd of {Kd_default}")
                        self.parameters_transport_material[i][n]["sorption_coefficient"] = Kd_default

    def load_diffusion_parameters(self, nuclides, nuclide_diffusion_file):
        """
        Load the effective diffusion coefficient data from a csv file

        Parameters
        ----------
        nuclides: list of str
            list of nuclides to consider
        nuclide_diffusion_file : string
            filename of the csv file to read nuclide sorption data from

        """
        from .tables import DiffusionParametersTable
        table = DiffusionParametersTable(fname=nuclide_diffusion_file)
        dfd = table.df
        eff_diffusivity_default = 1e-11 * u.m ** 2 / u.s
        # check if we have a accessible porosity column
        has_accessible_porosity = False
        if "accessible_porosity" in dfd.columns:
            logging.info("using accessible porosity column found in diffusion parameter file")
            has_accessible_porosity = True
        # find element if only nuclide is given
        if "element" not in dfd.columns:
            nuclides_dfd = dfd["nuclide"]
            elements = [n.split("-")[0] for n in nuclides_dfd]
            dfd["element"] = elements
        diff_unit = table.units["De"]["unit"]
        # go through geological units and nuclides and assign De value
        for i, _ in enumerate(self.geological_units):

            # get representative nuclide dataset for this geological unit
            nkey = self.parameters_rock[i]["diffusion_key"]
            dfdi = dfd.loc[dfd["nuclide_param_key"] == nkey]

            # go through all nuclides and find diffusion data
            for n in nuclides:
                element = n.split("-")[0]

                if element in dfdi["element"].values:
                    self.parameters_transport_material[i][n]["effective_diffusion_coefficient"] = \
                        dfdi.loc[dfdi['element'] == element, "De"].values[0] * diff_unit
                    if has_accessible_porosity:
                        self.parameters_transport_material[i][n]["accessible_porosity"] = \
                            dfdi.loc[dfdi['element'] == element, "accessible_porosity"].values[
                                0] * u.dimensionless_unscaled

                else:
                    logging.warning(f"warning, could not find diffusivity for at least one element, "
                                 f"assigning default diffusivity {eff_diffusivity_default}")
                    self.parameters_transport_material[i][n][
                        "effective_diffusion_coefficient"] = eff_diffusivity_default
                    if has_accessible_porosity:
                        # in this case, we dont do anything. Should fall back to the porosity of the host rock upstream.
                        pass

    def from_copy(self, obj):
        """
        construct parameter set from existing set

        Parameters
        ----------
        obj : ParameterSet
            the set to copy

        Returns
        -------

        """
        import copy
        self.parameters_rock = copy.deepcopy(obj.parameters_rock)
        self.parameters_transport_material = copy.deepcopy(obj.parameters_transport_material)
        self.name = obj.name
        self.source = obj.source
        self.description = obj.description

    def thickness(self):
        """
        return the total thickness of the units in the model

        Returns
        -------

        """
        t = 0.0 * u.m
        if isinstance(self.parameters_rock, list):
            for p in self.parameters_rock:
                t += p["thickness"]
            return t

        msg = "Thickness only available for models with more than one layer"
        raise NotImplementedError(msg)

    def write_human_readable(self, fname_root):
        """
        write the parameter set as a csv file in a human-readable fashion.

        Parameters
        ----------
        fname_root: str
            location and prefix of stored files. Files will have postfix "_rock.csv" and "_nuclides.csv".

        Returns
        -------

        """
        import pandas as pd
        fname_rock = fname_root + "_rock.csv"
        fname_nuclides = fname_root + "_nuclides.csv"

        # first, write the rock parameters
        columns = ["geologische Einheit", "Code", "Mächtigkeit", "Matrixdichte", "Porosität", "Durchlässigkeitsbeiwert"]

        units = []
        codes = []
        zs = []
        dens = []
        poro = []
        kf = []
        for iunit in range(len(self.parameters_rock)):
            units.append(self.parameters_rock[iunit]["full_name"])
            codes.append(self.parameters_rock[iunit]["name"])
            zs.append(self.parameters_rock[iunit]["thickness"].value)
            dens.append(self.parameters_rock[iunit]["bulk_density"].value)
            poro.append(self.parameters_rock[iunit]["porosity"])
            kf.append(self.parameters_rock[iunit]["hydraulic_conductivity"].value)
        df = pd.DataFrame(columns=columns)
        df["geologische Einheit"] = units
        df["Code"] = codes
        df["Mächtigkeit"] = zs
        df["Matrixdichte"] = dens
        df["Porosität"] = poro
        df["Durchlässigkeitsbeiwert"] = kf
        df.to_csv(fname_rock, index=False, sep=";", decimal=",", encoding='cp1252')

        # now, write the nuclid parameters
        columns = ["geologische Einheit", "Nuklid", "effektiver Diffusionskoeffizient", "Sorptionskoeffizient"]
        elems = []
        units = []
        des = []
        kds = []
        for iunit in range(len(self.parameters_rock)):

            for ielem, elem in enumerate(self.parameters_transport_material[iunit]):
                if elem != "stable":
                    elems.append(elem)
                    de = self.parameters_transport_material[iunit][elem]["effective_diffusion_coefficient"].value
                    kd = self.parameters_transport_material[iunit][elem]["sorption_coefficient"].value
                    kds.append(kd)
                    des.append(de)
                    if ielem == 0:
                        units.append(self.parameters_rock[iunit]["name"])
                    else:
                        units.append("")

        df = pd.DataFrame(columns=columns)
        df["Nuklid"] = elems
        df["geologische Einheit"] = units
        df["effektiver Diffusionskoeffizient"] = des
        df["Sorptionskoeffizient"] = kds
        df.to_csv(fname_nuclides, index=False, sep=";", decimal=",", encoding='cp1252')

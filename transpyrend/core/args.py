"""
defines some wrapper classes for providing arguments to the transport models
"""

import astropy.units as u


class ArgWrapper:
    """
    wrapper class for handling arguments for instantiating model objects.
    """
    set_existing_only = True
    ignore_non_existing = False
    must_be_fully_specified = False
    required = []

    def __init__(self, args):
        if isinstance(args, type(self)):
            args = args.__dict__
        if isinstance(args, dict):
            self._from_dict(args)
        elif args is None:
            if len(self.required) != 0:
                msg = f"missing required parameters {self.required}"
                raise RuntimeError(msg)
        else:
            raise NotImplementedError()
        self.post_init(args)

    def _from_dict(self, args):
        for required_parameter in self.required:
            if required_parameter not in args:
                msg = f"missing required argument {required_parameter}"
                raise RuntimeError(msg)
        for key, value in args.items():
            if hasattr(self, key):
                if self.check_valid_value(key, value):
                    setattr(self, key, value)
                else:
                    msg = f"Invalid value {value} for parameter {key}"
                    raise RuntimeError(msg)
            elif not self.ignore_non_existing:
                msg = f"Unexpected parameter '{key}'"
                raise RuntimeError(msg)

    def check_valid_value(self, _key, _value):
        """
        check if a parameter is valid

        Parameters
        ----------
        _key : str
            the parameter
        _value
            its value

        Returns
        -------
        res : bool
            True if parameter value is valid

        """
        return True

    def post_init(self, args):
        """
        To be called after instantiation

        Parameters
        ----------
        args :
            the arguments

        Returns
        -------

        """
        pass

    def __repr__(self):
        return type(self).__name__ + ": " + repr(self.__dict__)


class BCArgs(ArgWrapper):
    """
    wrapper class for boundary condition arguments

    existing arguments:
        - bc_type_left; the type of boundary condition employed at the left edge of the model.
        Can be "Dirichlet" or "Neumann". Default: Dirichlet
        - bc_type_right; the type of boundary condition employed at the right edge of the model.
        Can be "Dirichlet" or "Neumann". Default: Dirichlet
        - bc_left : value of the boundary condition at the left edge; must be astropy.units.Quantity.
        Default: 0 mol/m**3
        - bc_right : value of the boundary condition at the left edge; must be astropy.units.Quantity.
        Default: 0 mol/m**3
    """

    def __init__(self, args=None):
        self._bc_types = ["Dirichlet", "Neumann"]
        self.bc_type_left = "Dirichlet"
        self.bc_type_right = "Dirichlet"
        self.bc_left = 0.0 * u.mol / u.m ** 3
        self.bc_right = 0.0 * u.mol / u.m ** 3
        super().__init__(args)

    def check_valid_value(self, key, value):
        """
        check if value for key is valid
        Parameters
        ----------
        key : str
            the key
        value :
            its value

        Returns
        -------
        result : bool
            True if value is valid
        """
        if "bc_type_" in key:
            if value in self._bc_types:
                return True
            return False

        return super().check_valid_value(key, value)

    def post_init(self, args):
        """
        fix bc conditions after initialization

        Parameters
        ----------
        args : dict
            argument dictionary

        Returns
        -------

        """
        if self.bc_type_left == "Neumann" and "bc_left" not in args:
            self.bc_left /= u.yr
        if self.bc_type_right == "Neumann" and "bc_right" not in args:
            self.bc_right /= u.yr


class GeometryArgs(ArgWrapper):
    """
    wrapper class for geometry arguments

    existing arguments:
        - x, optional: array with the locations of grid nodes. If not present, the grid will be configured later on.
        - dx: the grid spacing. Must be astropy.units.Quantity. Default 1 m.
        - area_cell: the area of a single cell (orthogonal to the model direction). Must be astropy.units.Quantity.
         Default 1 m**2.
    """

    def __init__(self, args=None):
        self.x = None
        self.dx = 1 * u.m
        self.area_cell = 1 * u.m ** 2
        super().__init__(args)


class OptionArgs(ArgWrapper):
    """
    wrapper class for holding optional arguments

    existing arguments:
        - include_decay: if true, include radioactive decay in calculations. Default True
        - include_sorption: if true, include sorption in calculations. Default True
        - use_bateman: if True, use the analytic Bateman solution for operator splitting. Default False
        - nuclid_database: the name of nuclide database to be used. Default "rd"
        - external_solver_method: The name of the external solver to be used. Default None,
        only relevant for external solvers
        - external_solver_options: A dict with additional options for the external solver. Default {}, only relevant
        for external solvers
        - D_averaging_method: Name of the method used for averaging the diffusion coefficients at cell interfaces.
        Default "harmonic_mean"
        - theta: the theta parameter steering the partition of implicit and explicit contributions in the theta method.
        Default 0.5, only relevant for CN scheme
        - adapt_to_layers: if True, distort the grid such that each layer boundary lies on a node. Default False
        - store_flux: if True, store the total flux at each grid point and in each timestep. Not implemented in DOP853.
    """

    def __init__(self, args=None):
        self.include_decay = True
        self.include_sorption = True
        self.use_bateman = False
        self.nuclid_database = "rd"
        self.external_solver_method = None
        self.external_solver_options = {}
        self._D_averaging_methods = ["arithmetic_mean", "harmonic_mean", "geometric_mean"]
        self.D_averaging_method = "harmonic_mean"
        self.theta = 0.5
        self.adapt_to_layers = False
        self.store_flux = False
        super().__init__(args)

    def check_valid_value(self, key, value):
        """
        check if value for key is valid
        Parameters
        ----------
        key : str
            the key
        value :
            its value

        Returns
        -------
        result : bool
            True if valu
        """
        if key == "D_averaging_method":
            if value in self._D_averaging_methods:
                return True

            return False
        return super().check_valid_value(key, value)


class SourceArgs(ArgWrapper):
    """
    wrapper for dealing with arguments related to the source terms

    existing arguments:
        - near_field_index:
            node index of the location of the repository where radionuclides will be released into the far-field
        - source_term:
            the source term to be used; must be a BaseSourceTerm object
    """

    def __init__(self, args=None):
        self.near_field_index = None
        self.source_term = None
        super().__init__(args)

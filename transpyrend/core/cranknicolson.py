"""A Crank-Nicolson timestepping scheme"""
import numpy as np
import scipy
import scipy.sparse.linalg
from astropy import units as u
from numba import jit
from transpyrend.core.base import OptionArgs
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D
from transpyrend.utils.misc import find_diagonal_indizes


class CrankNicolsonTransportModel1D(EulerBackwardsModel1D):
    """
    implements Crank-Nicolson scheme. Inherits a large part of the functionality from EulerBackwardsModel1D.

       Parameters
       ----------
       species : list of str
           list of nuclides to consider
       parameters_rock : list of dict or dict
           the parameters of the geological units. dict if only one unit is considered.
       parameters_transport_material : list of dict or dict
           the transport parameters of the nuclides. dict if only one geological unit is considered.
       geometry : GeometryArgs or dict
           the geometry of the problem
       bcs : BCArgs or dict
           the boundary conditions
       time_interval : astropy.units.Quantity
           the total time interval considered in the model
       options : OptionArgs or dict
           optional parameters considering the solver
       parameters_source: SourceArgs or dict
           parameters concerning the source model
    """

    def __init__(self,
                 species,
                 parameters_rock,
                 parameters_transport_material=None,
                 geometry=None,
                 bcs=None,
                 time_interval=1e6 * u.yr,
                 options=None,
                 parameters_source=None
                 ):
        options = OptionArgs(options)
        self.theta = options.theta
        self.thetap = 1.0 - self.theta
        # call the __init__ of TransportModel1D.
        super().__init__(species, parameters_rock, parameters_transport_material=parameters_transport_material,
                         geometry=geometry,
                         bcs=bcs,
                         time_interval=time_interval,
                         options=options,
                         parameters_source=parameters_source)

    def setup_jit_functions(self):
        """
        create the relevant jitted functions for the solver

        Returns
        -------

        """

        bc_dirichlet_left = np.array(self.bc_dirichlet_left)
        bc_neumann_left = np.array(self.bc_neumann_left)
        bc_dirichlet_right = np.array(self.bc_dirichlet_right)
        bc_neumann_right = np.array(self.bc_neumann_right)
        dxs = self.dxs
        if self.bc_dirichlet_left is not None:
            left_is_dirichlet = True
        else:
            left_is_dirichlet = False
        if self.bc_dirichlet_right is not None:
            right_is_dirichlet = True
        else:
            right_is_dirichlet = False
        thetap = self.thetap
        u_coeff = self.u_coeff
        v_coeff_left = self.v_coeff_left
        v_coeff_right = self.v_coeff_right
        v_coeff_mid = self.v_coeff_mid

        @jit("f8[:,:](f8[:,:],f8[:,:],f8[:,:], f8)", nopython=True)
        def _ftv(c_old, s, W, dt):
            b = s * W
            b[:, 1:-1] += thetap * (u_coeff[:, 1:-1] * dt + v_coeff_left[:, 1:-1] * dt) * c_old[:, :-2] \
                          + (1 - thetap * v_coeff_mid[:, 1:-1] * dt) * c_old[:, 1:-1] \
                          + thetap * (-u_coeff[:, 1:-1] * dt + v_coeff_right[:, 1:-1] * dt) * c_old[:, 2:]
            # set upper boundary condition
            if left_is_dirichlet:
                b[:, 0] = bc_dirichlet_left[:]
            else:
                b[:, 0] = (1 - 2 * thetap * v_coeff_right[:, 0] * dt) * c_old[:, 0] + \
                          2 * thetap * v_coeff_right[:, 0] * dt * c_old[:, 1] + \
                          s[:, 0] * W[:, 0] - 2 * s[:, 0] * dxs[0] * (
                                  u_coeff[:, 0] + v_coeff_right[:, -1] * dt) * bc_neumann_left[:]
            if right_is_dirichlet:
                b[:, -1] = bc_dirichlet_right[:]
            else:
                b[:, -1] = (1. - 2 * thetap * v_coeff_left[:, -1] * dt) * c_old[:, -1] + \
                           2 * thetap * v_coeff_left[:, -1] * dt * c_old[:, -2] + \
                           s[:, -1] * W[:, 0] - 2 * s[:, -1] * dxs[-1] * (
                                   u_coeff[:, -1] * dt + v_coeff_left[:, -1] * dt) * bc_neumann_right[:]
            return b

        self._fill_transport_vector = _ftv

    def construct_transport_matrix_sparse(self, dt):
        """
        set up matrices for finite difference equations, using a sparse matrix.

        Parameters
        ----------
        dt : float
            timestep

        Returns
        -------

        """

        x = self.x
        nx = len(x)
        s = np.zeros(nx)
        s[1:-1] = 2.0 / (x[2:] - x[:-2])
        s[0] = 1.0 / (x[1] - x[0])
        s[-1] = 1.0 / (x[-1] - x[-2])

        t = np.zeros(nx)
        T = np.zeros(nx)
        t[:-1] = 1. / (x[1:] - x[:-1])
        T[:-1] = (x[1:] - x[:-1])

        u_ = np.zeros(nx)
        U_ = np.zeros(nx)
        u_[1:] = 1. / (x[1:] - x[:-1])
        U_[1:] = (x[1:] - x[:-1])

        uu = np.zeros(self.N)
        v_left = np.zeros(self.N)
        v_mid = np.zeros(self.N)
        v_right = np.zeros(self.N)
        ##
        u_coeff = np.zeros([self.nspecies, self.N])
        v_coeff_mid = np.zeros([self.nspecies, self.N])
        v_coeff_left = np.zeros([self.nspecies, self.N])
        v_coeff_right = np.zeros([self.nspecies, self.N])
        A_ = []

        for i in range(self.nspecies):
            uu[:] = self.q[:, i] * dt / (self.phi[:, i] * self.R[:, i] * (T + U_))
            v_left[:] = self.De_mid[:-1, i] * dt / (self.phi[:, i] * self.R[:, i]) * s * u_
            v_mid[:] = s * (u_ * self.De_mid[:-1, i] + t * self.De_mid[1:, i]) * dt / (
                    self.phi[:, i] * self.R[:, i])
            v_right[:] = self.De_mid[1:, i] * dt / (self.phi[:, i] * self.R[:, i]) * s * t

            # fill matrix
            # main = 1.0 + v_mid
            # lower = -uu - v_left
            # upper = uu - v_right

            main = 1.0 + self.theta * v_mid
            lower = self.theta * (-uu - v_left)
            upper = self.theta * (uu - v_right)

            # left-hand boundary
            if self.bc_dirichlet_left is not None:
                main[0] = 1
                upper[0] = 0
            else:
                main[0] = 1 + 2 * self.theta * v_right[0]
                upper[0] = -2 * self.theta * v_right[0]
            # right-hand boundary
            if self.bc_dirichlet_right is not None:
                main[-1] = 1
                lower[-1] = 0
            else:
                main[-1] = 1 + 2 * self.theta * v_left[-1]
                lower[-1] = -2 * self.theta * v_left[-1]

            # make lower diagonal conform definition of scipy.sparse.diags
            lower_ = lower[1:]
            upper_ = upper[:-1]

            Ai = scipy.sparse.diags(diagonals=[main, lower_, upper_],
                                    offsets=[0, -1, 1], shape=(self.N, self.N),
                                    format='csr')
            A_.append(Ai)
            ##
            u_coeff[i, :] = uu[:]
            v_coeff_mid[i, :] = v_mid[:]
            v_coeff_left[i, :] = v_left[:]
            v_coeff_right[i, :] = v_right[:]
        # we have to take out the factor of dt, as dt will change over time.
        # We will multiply with the correct dt when setting up the transport vector.
        self.u_coeff = u_coeff / dt
        self.v_coeff_left = v_coeff_left / dt
        self.v_coeff_mid = v_coeff_mid / dt
        self.v_coeff_right = v_coeff_right / dt
        self.A = A_

        # store the indices of the diagonal elements in the compressed sparse matrix format
        self.diagonal_indices = []
        for i in range(len(self.A)):
            indices = find_diagonal_indizes(self.A[i])
            self.diagonal_indices.append(indices)

        self.has_jit = True
        self.setup_jit_functions()

    # NOTE this is obsolete and superseded by setup_jit_functions()
    # it is still here for debug purposes, and in case we can not use jit for some reason.
    def fill_transport_vector(self, c_old, s, W, dt):
        """
        populate the vector with the known values, to be used for solving the transport equation later on.

        Parameters
        ----------
        c_old : array
            the concentrations at the last timestep
        s : array
            the s term in the transport equation, calculated as dt / (porosity * R)
        W : array
            the source terms
        dt : float
            time step size

        Returns
        -------
        b : array
            the right hand side of the matrix equation

        """
        b = s * W
        b[:, 1:-1] += self.thetap * (self.u_coeff[:, 1:-1] * dt + self.v_coeff_left[:, 1:-1] * dt) * c_old[:, :-2] \
                      + (1 - self.thetap * self.v_coeff_mid[:, 1:-1] * dt) * c_old[:, 1:-1] \
                      + self.thetap * (-self.u_coeff[:, 1:-1] * dt + self.v_coeff_right[:, 1:-1] * dt) * c_old[:, 2:]
        # set upper boundary condition
        if self.bc_dirichlet_left is not None:
            b[:, 0] = self.bc_dirichlet_left[:]
        elif self.bc_neumann_left is not None:
            b[:, 0] = (1. - 2 * self.thetap * self.v_coeff_right[:, 0] * dt) * c_old[:, 0] + \
                      2 * self.thetap * self.v_coeff_right[:, 0] * dt * c_old[:, 1] + \
                      s[:, 0] * W[:, 0] - s[:, 0] * self.dxs[0] * \
                      (self.u_coeff[:, 0] + self.v_coeff_right[:, -1] * dt) * self.bc_neumann_left[:]

        # set lower boundary condition
        if self.bc_dirichlet_right is not None:
            b[:, -1] = self.bc_dirichlet_right[:]
        elif self.bc_neumann_right is not None:
            b[:, -1] = (1. - 2 * self.thetap * self.v_coeff_left[:, -1] * dt) * c_old[:, -1] + \
                       2 * self.thetap * self.v_coeff_left[:, -1] * dt * c_old[:, -2] + \
                       s[:, -1] * W[:, 0] - s[:, -1] * self.dxs[-1] \
                       * (self.u_coeff[:, -1] * dt + self.v_coeff_left[:, -1] * dt) * self.bc_neumann_right[:]
        return b

    def construct_transport_matrix_sparse_constant_D(self, dt):
        """
        unimplemented function
        """
        raise NotImplementedError()

    def construct_transport_matrix_variable_x(self, dt,
                                              specified_concentration_left=None,
                                              specified_concentration_right=None,
                                              verbose=False):
        """
        unimplemented function
        """
        raise NotImplementedError()

    def construct_transport_matrix_with_upwinding(self, dt):
        """
        unimplemented function
        """
        raise NotImplementedError()

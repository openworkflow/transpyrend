"""
implements some ensemble models, that is, wrappers to run model in parallel
"""

import logging
import warnings

import matplotlib.pyplot as plt
import astropy.units as u
import numpy as np
from transpyrend.core.base import BaseTransportModel1D, BCArgs, OptionArgs, SourceArgs
from transpyrend.core.externalsolver import DOP853TransportModel1D
from transpyrend.utils.nuclides import vsg_tab411_uranium_chain_bateman_setup, vsg_tab411_actinium_chain_bateman_setup
from transpyrend.utils.nuclides import vsg_tab411_neptunium_chain_bateman_setup, vsg_tab411_thorium_chain_bateman_setup
from transpyrend.utils.nuclides import vsg_tab411_neptunium_chain, vsg_tab411_thorium_chain, vsg_tab411_uranium_chain
from transpyrend.utils.nuclides import vsg_tab411_actinium_chain, vsg_tab411_activation_products

# regular math instead of italics
plt.rcParams["mathtext.default"] = "regular"


def run_me(args):
    """
    wrapper for the parallelization of the simulation. Each subprocess will run an instance of this function.

    Parameters
    ----------
    args : tuple
        tuple of arguments for starting the calculation: (model, ics, t_eval, dt, dt_max, dt_init, f_dt_growth)

    Returns
    -------
    model : BaseTransportModel
        the processed model

    """
    model, ics, t_eval, dt, dt_max, dt_init, f_dt_growth = args
    logging.debug("running " + repr(model))
    model.run(t_eval=t_eval, ics=ics, dt=dt, dt_max=dt_max, dt_init=dt_init, f_dt_growth=f_dt_growth)
    logging.debug("done with " + repr(model.species))
    return model


class BaseEnsembleModel1D(BaseTransportModel1D):
    """
    this is a wrapper for running multiple instance of a transport model with varying nuclides in each of the runs.
    This can be useful to run, in each of the models, only the nuclides of a certain, closed decay chain. The
    individual chains can be run in parallel.


        Parameters
        ----------
        species_sets : list of list of str
            list of nuclides to consider
        parameters_rock : list of dict or dict
            the parameters of the geological units. dict if only one unit is considered.
        parameters_transport_material : list of dict or dict, default None
            the transport parameters of the nuclides. dict if only one geological unit is considered.
        geometry : GeometryArgs or dict, default None
            the geometry of the problem
        bcs : BCArgs or dict, default None
            the boundary conditions
        time_interval : astropy.units.Quantity, default 1e6 yr
            the total time interval considered in the model
        options : OptionArgs or dict, default None
            optional parameters considering the solver
        modeltype: DOP853TransportModel1D or ImplicitTransportModel1D, default DOP853TransportModel1D
            the employed solver
        ics: dict or astropy.units.Quantity, default None
            the initial conditions
        parameters_source: SourceArgs or dict, default None
            parameters concerning the source model

    """

    def __init__(self,
                 species_sets,
                 parameters_rock,
                 parameters_transport_material=None,
                 geometry=None,
                 bcs=None,
                 time_interval=1e6 * u.yr,
                 options=None,
                 modeltype=DOP853TransportModel1D,
                 ics=None,
                 parameters_source=None):

        if (not isinstance(bcs.bc_left, dict)) and (bcs.bc_left is not None):
            msg = "bc_left must be a dictionary or None for ensemble models"
            raise NotImplementedError(msg)
        if (not isinstance(bcs.bc_right, dict)) and (bcs.bc_right is not None):
            msg = "bc_right must be a dictionary or None for ensemble models"
            raise NotImplementedError()
        # the number of models in the ensemble
        self.ics = ics
        self.nruns = len(species_sets)
        self.modeltype = modeltype
        bcs = BCArgs(bcs)

        allspecies = []
        has_stable = False
        for i in range(self.nruns):
            for spec in species_sets[i]:
                if spec == "stable":
                    has_stable = True
                else:
                    allspecies.append(spec)
        if has_stable:
            allspecies.append("stable")

        allbc_left = []
        if bcs.bc_type_left != "noFlow":
            for nuclid in allspecies:
                if isinstance(bcs.bc_left, dict):
                    allbc_left.append(bcs.bc_left[nuclid])
                else:
                    allbc_left.append(bcs.bc_left)
        allbc_right = []
        if bcs.bc_type_right != "noFlow":
            for nuclid in allspecies:
                if isinstance(bcs.bc_right, dict):
                    allbc_right.append(bcs.bc_right[nuclid])
                else:
                    allbc_right.append(bcs.bc_right)
        allbcs = BCArgs(bcs)
        allbcs.bc_left = allbc_left
        allbcs.bc_right = allbc_right

        super().__init__(allspecies, parameters_rock, parameters_transport_material=parameters_transport_material,
                         geometry=geometry, bcs=allbcs, time_interval=time_interval, options=options,
                         parameters_source=parameters_source)

        # setup the individual models.
        self.models = []
        mybc_left = None
        mybc_right = None
        for i in range(self.nruns):
            mybc_left = []
            for nuclid in species_sets[i]:
                mybc_left.append(bcs.bc_left[nuclid])
            mybc_right = []
            for nuclid in species_sets[i]:
                mybc_right.append(bcs.bc_right[nuclid])
            mybcs = BCArgs(bcs)
            mybcs.bc_right = mybc_right
            mybcs.bc_left = mybc_left
            # feed the individual models only with the nuclides in the source term that they need
            my_parameters_source = SourceArgs(parameters_source)
            if my_parameters_source.source_term is not None:
                my_parameters_source.source_term = my_parameters_source.source_term.get_subset(species_sets[i])
            m = modeltype(species_sets[i], parameters_rock, parameters_transport_material=parameters_transport_material,
                          geometry=geometry, bcs=mybcs, time_interval=time_interval, options=options,
                          parameters_source=my_parameters_source)
            self.models.append(m)
        # construct the phi array for the Ensemble model
        self.phi = np.zeros([self.N, self.nspecies])
        offset = 0
        for mm in self.models:
            if has_stable:
                self.phi[:, offset:offset + mm.nspecies - 1] = mm.phi[:, :-1]
                offset += mm.nspecies - 1
            else:
                self.phi[:, offset:offset + mm.nspecies] = mm.phi
                offset += mm.nspecies
        if has_stable:
            self.phi[:, offset] = mm.phi[:, -1]

        # also, get the parameters of the individual models. We need them for flux calculations.
        De_mid = np.zeros([self.N + 1, self.nspecies])
        R = np.ones([self.N, self.nspecies])
        q = np.zeros([self.N, self.nspecies])
        offset = 0
        for mm in self.models:
            if has_stable:
                De_mid[:, offset:offset + mm.nspecies - 1] = mm.De_mid[:, :-1]
                R[:, offset:offset + mm.nspecies - 1] = mm.R[:, :-1]
                q[:, offset:offset + mm.nspecies - 1] = mm.q[:, :-1]
                offset += mm.nspecies - 1
            else:
                De_mid[:, offset:offset + mm.nspecies] = mm.De_mid
                R[:, offset:offset + mm.nspecies] = mm.R
                q[:, offset:offset + mm.nspecies] = mm.q
                offset += mm.nspecies
        self.De_mid = De_mid
        self.R = R
        self.q = q
        # automatically delete submodel data after running
        self.auto_shrink = True

    def get_max_timestep(self, unit=u.s, cap_timestep=None):
        """
        return the maximum allowed timestep across all submodels

        Parameters
        ----------
        unit : astropy.units.unit
            physical unit of the output
        cap_timestep : astropy.units.Quantity
            if given, cap the calculated timestep by this value

        Returns
        -------
        dt_max : list
            the maximum timestep allowed for each submodel
        """
        return [m.get_max_timestep(cap_timestep=cap_timestep).to(unit) for m in self.models]

    def _run(self, ics, t_eval, dts, dts_max, dts_init, fs_dt_growths, multiproc_driver="pathos", nprocs=8, **kwargs):
        """
        run all submodels

        Parameters
        ----------
        ics : astropy.units.Quantity
            dictionary with the initial concentration in each cell for each species
        t_eval : astropy.units.Quantity
            the times at which we wan to collect the results
        dt : astropy.units.Quantity
            timestep for each of the models. If set, all other parameters for the time stepping will be
            ignored. Cannot be used with t_eval.
        dts : astropy.units.Quantity
            legacy; should be either dt or dts_max
        dts_max : astropy.units.Quantity
            array with the maximum timesteps for each of the models
        dts_init : astropy.units.Quantity
            array with the initial timesteps for each of the models
        fs_dt_growth : array of floats
            array of factors by which the time step can grow (up to dts_max) for each model
        multiproc_driver : str
            indicates the package to use for multiprocessing. can be "pathos", "default" or "none"
        nprocs : int
            number of processes used in parallel execution
        kwargs : dict
            additional keyword arguments for the submodels


        Returns
        -------

        """
        assert multiproc_driver in ["pathos", "default", "none"]
        if multiproc_driver == "pathos":
            from pathos.pools import ProcessPool
        elif multiproc_driver == "default":
            from multiprocessing import Pool
        if len(self.models) != len(ics) or len(ics) != len(dts):
            msg = (
                "length of arrays does not match: lengths are ",
                repr(len(self.models)) + " " + repr(len(ics)) + " " + repr(len(dts))
            )
            raise RuntimeError(msg)
        args = [[self.models[i], ics[i], t_eval, dts[i], dts_max[i], dts_init[i], fs_dt_growths[i]] for i in
                range(self.nruns)]

        if multiproc_driver != "none":
            if multiproc_driver == "pathos":
                p = ProcessPool(nodes=nprocs)
            else:
                p = Pool(processes=nprocs)

            results = p.map(run_me, args)
            self.models = results
            # workaround for a bug in multiprocessing, requiring us to close the pool manually (
            p.close()
            # workaround for a bug in pathois, requiring us to clear each of the pools afterwards (#111 in pathos)
            if multiproc_driver == "pathos":
                p.clear()
        else:
            for i, m in enumerate(self.models):
                m.run(ics=ics[i], dt=dts[i], dt_max=dts_max[i], dt_init=dts_init[i], f_dt_growth=fs_dt_growths[i],
                      t_eval=t_eval, **kwargs)

    def run(self, ics=None, t_eval=None, dt=None, dts=None, dts_max=None, dts_init=None, fs_dt_growth=None,
            multiproc_driver="pathos", nprocs=8, **kwargs):
        """
        run the ensemble model by running all models in self.models. This can be done in parallel via multiprocessing.

        Parameters
        ----------
        ics : astropy.units.Quantity
            dictionary with the initial concentration in each cell for each species
        t_eval : astropy.units.Quantity
            the times at which we wan to collect the results
        dt : astropy.units.Quantity
            timestep for each of the models. If set, all other parameters for the time stepping will be
            ignored. Cannot be used with t_eval.
        dts : astropy.units.Quantity
            legacy; should be either dt or dts_max
        dts_max : astropy.units.Quantity
            array with the maximum timesteps for each of the models
        dts_init : astropy.units.Quantity
            array with the initial timesteps for each of the models
        fs_dt_growth : array of floats
            array of factors by which the time step can grow (up to dts_max) for each model
        multiproc_driver : str
            indicates the package to use for multiprocessing. can be "pathos", "default" or "none"
        nprocs : int
            number of processes used in parallel execution

        Returns
        -------

        """
        if self.model_completed:
            msg = "The model was already run before."
            raise RuntimeError(msg)

        if dts is None and dts_max is None and self.options.store_flux:
            msg = "if using store_flux, dt_max needs to be set"
            raise NotImplementedError(msg)
        if dts is not None:
            warnings.warn("use either dt or dts_max, depending on what you want. Assuming you wanted dts_max")
            dts_max = dts
        # catch the case in which the submodels will run with different timesteps and write different outputs.
        if t_eval is None and (dts_max is not None and dts_max.size > 1) or (
                dts_init is not None and dts_init.size > 1) or (fs_dt_growth is not None and len(fs_dt_growth) > 1):
            warnings.warn("t_eval needs to be set when different values of dts_max/dts_init/fs_dt_growth are used.")
            t_eval = self.default_t_eval()
        if t_eval is None and dts_max is None and dt is None:
            warnings.warn(
                "t_eval needs to be set if no constraints for the time steps are given. Setting it to the default.")
            t_eval = self.default_t_eval()
        if ics is None:
            ics = self.ics
        if not isinstance(ics, dict):
            msg = "ics must be a dictionary for ensemble models"
            raise NotImplementedError(msg)
        if ics is not None:
            myics = self.massage_ics(ics)
        else:
            myics = [None for i in range(len(self.models))]
        if dts_max is None:
            dts_max = [None for i in range(len(self.models))]
        elif dts_max.size == 1:
            dts_max = [dts_max for i in range(len(self.models))]
        if dts_init is None:
            dts_init = [1. * u.yr for i in range(len(self.models))]
        if fs_dt_growth is None:
            fs_dt_growth = [1.05 for i in range(len(self.models))]
        if dt is not None:
            if dt.size > 1:
                msg = "Can not use different values of dt for models in the same ensemble"
                raise NotImplementedError(msg)
            mydts = np.ones(self.nruns) * u.yr * dt
        else:
            mydts = [None for i in range(len(self.models))]
        self._run(ics=myics, t_eval=t_eval, dts=mydts, dts_max=dts_max, dts_init=dts_init, fs_dt_growths=fs_dt_growth,
                  multiproc_driver=multiproc_driver, nprocs=nprocs, **kwargs)
        self.post_run()
        self.model_completed = True

    def post_run(self):
        """
        run after model is processed. Collates the results and cleans up.

        Returns
        -------

        """
        # collate the data from the individual models.
        # this is a little messy. The model output has shape (n_x*n_species,n_t). We convert it to a 3D array first
        # and then reshape back.
        ts = self.models[0].results["t"]
        self.results = {}
        self.results["c_fluid"] = np.zeros((self.nspecies, ts.size, self.N))
        has_stable = False
        if "stable" in self.species:
            if self.modeltype == DOP853TransportModel1D:
                logging.warning("stable mass unavailable for DOP853TransportModel1D. Setting stable mass to zero.")
            has_stable = True
            self.results["stable_mass"] = np.zeros((ts.size, self.N))
        self.results["t"] = self.models[0].results["t"]
        offset = 0
        for i in range(self.nruns):
            assert self.models[i].results["t"].size == ts.size

            mynspecies = self.models[i].nspecies
            if has_stable:
                if self.modeltype != DOP853TransportModel1D:
                    self.results["stable_mass"] += self.models[i].results["stable_mass"]
                res = self.models[i].results["c_fluid"][:-1]
                self.results["c_fluid"][offset:offset + mynspecies - 1] = res
                stable = self.models[i].results["c_fluid"][-1]
                self.results["c_fluid"][-1] += stable
                offset += mynspecies - 1
            else:
                res = self.models[i].results["c_fluid"][:]
                self.results["c_fluid"][offset:offset + mynspecies] = res
                offset += mynspecies

            if "fluxes" in self.models[i].results and self.models[i].results["fluxes"] is not None:
                if i == 0:
                    self.results["fluxes"] = self.models[i].results["fluxes"]
                    self.results["mass_fluxes"] = self.models[i].results["mass_fluxes"]
                else:
                    self.results["fluxes"] += self.models[i].results["fluxes"]
                    self.results["mass_fluxes"] += self.models[i].results["mass_fluxes"]
                self.results["t_fluxes"] = self.models[i].results["t_fluxes"]

        # check for NaNs in the results
        if (np.any(np.isnan(self.results["c_fluid"]))):
            warnings.warn("NaNs in results array!")
        if self.auto_shrink:
            self.shrink()

    def shrink(self):
        """
        remove the individual models in order to reduce memory footprint. Results can still be accessed using .results.
        """
        self.models = []

    def set_cfl_limit(self, cfl_limit):
        """
        set the cfl limit

        Parameters
        ----------
        cfl_limit : astropy.units.Quantity
            the cfl limit

        Returns
        -------

        """
        super().set_cfl_limit(cfl_limit)
        for model in self.models:
            model.set_cfl_limit(cfl_limit)

    def set_cfl_d_limit(self, cfl_limit):
        """
        set the cfl limit for diffusion

        Parameters
        ----------
        cfl_limit : astropy.units.Quantity
            the cfl limit

        Returns
        -------

        """
        super().set_cfl_d_limit(cfl_limit)
        for model in self.models:
            model.set_cfl_d_limit(cfl_limit)

    def set_hl_limit(self, hl_limit):
        """
        set the hl limit

        Parameters
        ----------
        hl_limit : astropy.units.Quantity
            the hl limit

        Returns
        -------

        """
        super().set_hl_limit(hl_limit)
        for model in self.models:
            model.set_hl_limit(hl_limit)

    def write_system(self, fname, ics):
        """
        write the system of equations to a text file for each submodel

        Parameters
        ----------
        fname : str
            file name
        ics : astropy.units.Quantity
            initial condition

        Returns
        -------

        """
        for i in range(len(self.models)):
            self.models[i].write_system(fname + "_" + repr(i), ics)


    def massage_ics(self, ics):
        """
        for each of the models in the ensemble, construct an IC matrix from a dictionary

        Parameters
        ----------
        ics : astropy.units.Quantity
            initial condition

        Returns
        -------

        """
        theics = []
        for model in self.models:
            myics = model.massage_ics(ics)
            theics.append(myics)
        return theics

    def plot_overview_c_x(self, **args):
        """
        just a wrapper around the actual plotting function in utils.plotting

        Parameters
        ----------
        args
            argument list matching the arguments of transpyrend.utils.plotting.plot_overview_c_x

        Returns
        -------
        fig : figure object
            the figure created
        ax : axis object
            the axis
        """
        from transpyrend.utils.plotting import plot_overview_c_x
        return plot_overview_c_x(self, **args)

    def store_results(self, fname, ignore_submodels=True):
        """
        stores the results of the model in hdf5 format

        Parameters
        ----------
        fname: str
            filename for the hdf5 file
        ignore_submodels: bool
            if true, do not attempt to store the results of the submodels. Saves space.

        Returns
        -------

        """
        import h5py
        with h5py.File(fname, "w") as f:
            # store the results dict
            f.create_group("ensemble")
            for key in self.results:
                if key == "t":
                    self.results[key] = self.results[key].to(u.s)
                f.create_dataset("ensemble/" + key, data=self.results[key])
            # if the submodels have their own results, store them as well
            if not ignore_submodels:
                f.create_dataset("nmodels", data=np.array([len(self.models)]))
                for i, submodel in enumerate(self.models):
                    f.create_group(f"model_{i}")
                    for key in submodel.results:
                        if submodel.results[key] is not None:
                            if key == "t":
                                submodel.results[key] = submodel.results[key].to(u.s)
                            f.create_dataset(f"model_{i}/" + key, data=submodel.results[key])

    def load_results(self, fname, ignore_submodels=True):
        """
        loads the results of the model from a file in hdf5 format

        Parameters
        ----------
        fname: str
           filename for the hdf5 file
        ignore_submodels: bool
           if true, do not attempt to load the results of the submodels

        Returns
        -------

        """
        import h5py
        with h5py.File(fname, "r") as f:
            # load the results dict
            self.results = {}
            for key in f["ensemble"]:
                self.results[key] = f["ensemble/" + key][:]
                if key == "t":
                    self.results[key] = self.results[key] * u.s
            # if the submodels have their own results, load them as well
            if not ignore_submodels:
                nmodels = f["nmodels"][0]
                if nmodels != self.nruns:
                    msg = "number of submodels does not match"
                    raise RuntimeError(msg)
                for i, submodel in enumerate(self.models):
                    submodel.results = {}
                    group = f"model_{i}"
                    for key in f[group]:
                        submodel.results[key] = f[group + "/" + key][:]
                        if key == "t":
                            submodel.results[key] = submodel.results[key] * u.s

    def get_total_amount_over_time(self, normalized=True):
        """
        retrieve the total amount of substance as a function of time for the system

        Parameters
        ----------
        normalized: bool
            if true, normalize amout of substance by value at t=0

        Returns
        -------
        amount:
            the total amount of substance in the model as a function of time

        """
        ntimes = self.results["t"].size
        amount = np.zeros(ntimes) * u.mol / u.m ** 3
        for itime in range(ntimes):
            _, csum = self.get_summed_concentrations_x(itime=itime, per_rock_volume=True,
                                                       quantity="molar density", ignore_stable=False)
            amount[itime] = csum.sum()
        if normalized:
            amount /= amount[0]
        return amount


N_SPECIES_PER_RUN = 2


class VSGEnsembleModel1D(BaseEnsembleModel1D):
    """
    implements the VSG model for 1D transport, sorption, retention, radioactive decay, and splits up the calculation
    in five models, one for each of the decay chains and one for the activation products.
    These submodels can be run in parallel.

     Parameters
        ----------
        species : list of str
            list of nuclides to consider
        parameters_rock : list of dict or dict
            the parameters of the geological units. dict if only one unit is considered.
        parameters_transport_material : list of dict or dict
            the transport parameters of the nuclides. dict if only one geological unit is considered.
        geometry : GeometryArgs or dict
            the geometry of the problem
        bcs : BCArgs or dict
            the boundary conditions
        time_interval : astropy.units.Quantity
            the total time interval considered in the model
        options : OptionArgs or dict
            optional parameters considering the solver
        modeltype: DOP853TransportModel1D or ImplicitTransportModel1D
            the employed solver
        include_stable: bool
            include a stable nuclide
        ics: dict or astropy.units.Quantity
            the initial conditions
        parameters_source: SourceArgs or dict
            parameters concerning the source model

    """

    def __init__(self, parameters_rock,
                 parameters_transport_material=None,
                 geometry=None,
                 bcs=None,
                 time_interval=1e6 * u.yr,
                 options=None,
                 modeltype=DOP853TransportModel1D,
                 include_stable=True,
                 ics=None, parameters_source=None):

        # there is a large number of activation products, we can split them up to
        # get many models that can be run in parallel.

        chains = [vsg_tab411_activation_products[i:i + N_SPECIES_PER_RUN] for i in
                  range(0, len(vsg_tab411_activation_products), N_SPECIES_PER_RUN)]

        chains.append(vsg_tab411_neptunium_chain.copy())
        chains.append(vsg_tab411_uranium_chain.copy())
        chains.append(vsg_tab411_thorium_chain.copy())
        chains.append(vsg_tab411_actinium_chain.copy())

        self.include_stable = include_stable
        if include_stable:
            for chain in chains:
                chain.append("stable")
        bcs = BCArgs(bcs)
        thebc_left = None
        if bcs.bc_type_left != "noFlow":
            thebc_left = {}
            if not isinstance(bcs.bc_left, dict):
                for chain in chains:
                    for nuclid in chain:
                        thebc_left[nuclid] = bcs.bc_left
        thebc_right = None
        if bcs.bc_type_right != "noFlow":
            thebc_right = {}
            if not isinstance(bcs.bc_right, dict):
                for chain in chains:
                    for nuclid in chain:
                        thebc_right[nuclid] = bcs.bc_right
        allbcs = BCArgs(bcs)
        allbcs.bc_left = thebc_left
        allbcs.bc_right = thebc_right

        parameters_transport_material_mod = parameters_transport_material.copy()

        options = OptionArgs(options)
        options.nuclid_database = "vsg"
        super().__init__(chains, parameters_rock, bcs=allbcs,
                         parameters_transport_material=parameters_transport_material_mod,
                         geometry=geometry, time_interval=time_interval, options=options, modeltype=modeltype,
                         ics=ics, parameters_source=parameters_source)
        if options.use_bateman:
            self.setup_bateman()

    def setup_bateman(self, _chains=None, _states=None):
        """
        setup the bateman solver for the VSG nuclide scheme.

        Returns
        -------

        """
        for model in self.models:
            if model.species[0] in vsg_tab411_activation_products:
                chains = []
                states = []
                for species in model.species:
                    if species == "stable":
                        continue
                    if self.include_stable:
                        chain = [species, "stable"]
                        if len(states) == 0:
                            states.append(-1)
                        else:
                            states.append(1)
                    else:
                        chain = [species]
                        states.append(-1)
                    chains.append(chain)
                model.setup_bateman(chains, states)
            else:
                if model.species[0] in vsg_tab411_neptunium_chain:
                    chains = vsg_tab411_neptunium_chain_bateman_setup["chains"]
                    states = vsg_tab411_neptunium_chain_bateman_setup["states"]
                elif model.species[0] in vsg_tab411_uranium_chain:
                    chains = vsg_tab411_uranium_chain_bateman_setup["chains"]
                    states = vsg_tab411_uranium_chain_bateman_setup["states"]
                elif model.species[0] in vsg_tab411_actinium_chain:
                    chains = vsg_tab411_actinium_chain_bateman_setup["chains"]
                    states = vsg_tab411_actinium_chain_bateman_setup["states"]
                elif model.species[0] in vsg_tab411_thorium_chain:
                    chains = vsg_tab411_thorium_chain_bateman_setup["chains"]
                    states = vsg_tab411_thorium_chain_bateman_setup["states"]
                else:
                    raise NotImplementedError()
                if self.include_stable:
                    new_chains = []
                    for j, chain in enumerate(chains):
                        new_chain = [*chain, "stable"]
                        new_chains.append(new_chain)
                        if states[j] == -1 and j > 0:
                            states[j] = len(new_chain) - 1
                    chains = new_chains
                model.setup_bateman(chains, states)

    def check_if_significant_transport(self, c, c_plot_threshold=0.5, repo_grid_cell_distance=2,
                                       abs_threshold=1e-9 * u.kg / u.m ** 3):

        """
        Check if species was transported far enough to be counted as significant
        used for result plots where only non-sorbed species are highlighted

        Parameters
        ----------
        c :
            concentration of species
        c_transport_threshold : float
            max relative concentration outside repository for specie transport to be considered significant
        repo_grid_cell_distance : int
            distance in number of grid cells to start looking for transported species

        Returns
        -------

        """

        # c_ = c.value
        # catch the case in which c is a molar density
        if c.unit.physical_type == "mass density":
            pass
        elif c.unit.physical_type == "volumetric rate":
            abs_threshold = 10. * u.Bq / u.m ** 3
        else:
            abs_threshold = 1e-7 * u.mol / u.m ** 3

        c_ = c
        max_c = np.max(c_)
        ind_repo = np.argmax(c_)
        i1 = ind_repo - repo_grid_cell_distance
        i2 = ind_repo + 1 + repo_grid_cell_distance
        c_outside_repo = np.concatenate([c_[:i1], c_[i2:]])
        max_c_outside_repo = np.max(c_outside_repo)

        if (max_c_outside_repo / max_c) >= c_plot_threshold and max_c_outside_repo > abs_threshold:
            return True

        return False

"""An Euler-Backwards timestepping scheme"""
import logging

import numpy as np
import scipy
import scipy.sparse.linalg
from astropy import units as u
from numba import jit
from transpyrend.core.base import BaseTransportModel1D
from transpyrend.utils.bateman import bateman_full
from transpyrend.utils.misc import find_diagonal_indizes


class EulerBackwardsModel1D(BaseTransportModel1D):
    """
    Implicit model for transport of radionuclides. Implements Euler backwards for the timestepping.

        Parameters
        ----------
        species : list of str
            list of nuclides to consider
        parameters_rock : list of dict or dict
            the parameters of the geological units. dict if only one unit is considered.
        parameters_transport_material : list of dict or dict
            the transport parameters of the nuclides. dict if only one geological unit is considered.
        geometry : GeometryArgs or dict
            the geometry of the problem
        bcs : BCArgs or dict
            the boundary conditions
        time_interval : astropy.units.Quantity
            the total time interval considered in the model
        options : OptionArgs or dict
            optional parameters considering the solver
        parameters_source: SourceArgs or dict
            parameters concerning the source model
    """

    def __init__(self,
                 species,
                 parameters_rock,
                 parameters_transport_material=None,
                 geometry=None,
                 bcs=None,
                 time_interval=1e6 * u.yr,
                 options=None,
                 parameters_source=None
                 ):

        # call the __init__ of TransportModel1D.
        super().__init__(species, parameters_rock, parameters_transport_material=parameters_transport_material,
                         geometry=geometry,
                         bcs=bcs,
                         time_interval=time_interval,
                         options=options,
                         parameters_source=parameters_source)
        self.time_interval = time_interval.to(u.s)

        # setup the decays
        self.setup_decays()

        # switch order for R array from (xcoord, specie) to (specie, xcoord)
        # note W should already be in shape (specie, xcoord)
        self.R_ = self.R.T

    def construct_transport_matrix_sparse(self, dt):
        """
        set up matrices for finite difference equations, using a sparse matrix.

        Parameters
        ----------
        dt : float
            timestep

        Returns
        -------

        """

        x = self.x
        nx = len(x)
        s = np.zeros(nx)
        s[1:-1] = 2.0 / (x[2:] - x[:-2])
        s[0] = 1.0 / (x[1] - x[0])
        s[-1] = 1.0 / (x[-1] - x[-2])

        t = np.zeros(nx)
        T = np.zeros(nx)
        t[:-1] = 1. / (x[1:] - x[:-1])
        T[:-1] = (x[1:] - x[:-1])

        u_ = np.zeros(nx)
        U_ = np.zeros(nx)
        u_[1:] = 1. / (x[1:] - x[:-1])
        U_[1:] = (x[1:] - x[:-1])

        uu = np.zeros(self.N)
        v_left = np.zeros(self.N)
        v_mid = np.zeros(self.N)
        v_right = np.zeros(self.N)

        A_ = []

        for i in range(self.nspecies):
            # create the matrix coefficients
            uu[:] = self.q[:, i] * dt / (self.phi[:, i] * self.R[:, i] * (T + U_))

            v_left[:] = self.De_mid[:-1, i] * dt / (self.phi[:, i] * self.R[:, i]) * s * u_
            v_mid[:] = s * (u_ * self.De_mid[:-1, i] + t * self.De_mid[1:, i]) * dt / (
                    self.phi[:, i] * self.R[:, i])
            v_right[:] = self.De_mid[1:, i] * dt / (self.phi[:, i] * self.R[:, i]) * s * t

            main = 1.0 + v_mid
            lower = -uu - v_left
            upper = uu - v_right

            # left-hand boundary
            if self.bc_dirichlet_left is not None:
                main[0] = 1
                upper[0] = 0
            else:
                main[0] = 1 + 2 * v_right[0]
                upper[0] = -2 * v_right[0]

            # right-hand boundary
            if self.bc_dirichlet_right is not None:
                main[-1] = 1
                lower[-1] = 0
            else:
                main[-1] = 1 + 2 * v_left[-1]
                lower[-1] = -2. * v_left[-1]

            # make lower diagonal conform definition of scipy.sparse.diags
            lower_ = lower[1:]
            upper_ = upper[:-1]

            Ai = scipy.sparse.diags(diagonals=[main, lower_, upper_],
                                    offsets=[0, -1, 1], shape=(self.N, self.N),
                                    format='csr')
            A_.append(Ai)

        self.A = A_
        # store the indices of the diagonal elements in the compressed sparse matrix format
        self.diagonal_indices = []
        for i in range(len(self.A)):
            indices = find_diagonal_indizes(self.A[i])
            self.diagonal_indices.append(indices)

        self.has_jit = True

        self.setup_jit_functions()

    def update_transport_matrix(self, olddt, newdt):
        """
        Update the transport matrix when the timestep size has changed

        Parameters
        ----------
        olddt : float
            previous value of timestep
        newdt : float
            new value of timestep

        """
        # caution: this only works if the indices in A do not change due to e.g. the solver resorting things.
        one = np.float64(1.0)
        for i, _ in enumerate(self.A):
            self.A[i].data[self.diagonal_indices[i]] -= one
            self.A[i].data *= newdt / olddt
            self.A[i].data[self.diagonal_indices[i]] += one

        # this alternative version is considerably slower, but does not rely on constant indices.
        # eye = sparse.eye(*(self.A[0].shape), format="csr")
        # for i, m in enumerate(self.A):
        #    self.A[i] = (m - eye) * newdt / olddt + eye

    # NOTE this is obsolete and superseded by setup_jit_functions()
    def fill_transport_vector(self, c_old, s, W, dt):
        """
        populate the vector with the known values, to be used for solving the transport equation later on.

        Parameters
        ----------
        c_old : array
            the concentrations at the last timestep
        s : array
            the s term in the transport equation, calculated as dt / (porosity * R)
        W : array
            the source terms
        dt : float
            time step size

        Returns
        -------
        b : array
            the right hand side of the matrix equation

        """
        b = c_old + s * W
        # set upper boundary condition
        if self.bc_dirichlet_left is not None:
            b[:, 0] = self.bc_dirichlet_left[:]
        elif self.bc_neumann_left is not None:
            b[:, 0] = c_old[:, 0] + s[:, 0] * W[:, 0] - (s[:, 0] / self.dxs[0]) * self.bc_neumann_left[:]
        # set lower boundary condition
        if self.bc_dirichlet_right is not None:
            b[:, -1] = self.bc_dirichlet_right[:]
        elif self.bc_neumann_right is not None:
            b[:, -1] = c_old[:, -1] + s[:, -1] * W[:, -1] + (s[:, -1] / self.dxs[-1]) * self.bc_neumann_right[:]
        return b

    def setup_jit_functions(self):
        """
        create the jitted function used for calculations later on

        Returns
        -------

        """
        if self.bc_dirichlet_left is None:
            left_is_dirichlet = False
            bc_left = np.array(self.bc_neumann_left)
        else:
            left_is_dirichlet = True
            bc_left = np.array(self.bc_dirichlet_left)
        if self.bc_dirichlet_right is None:
            right_is_dirichlet = False
            bc_right = np.array(self.bc_neumann_right)
        else:
            right_is_dirichlet = True
            bc_right = np.array(self.bc_dirichlet_right)

        dxs = self.dxs
        nspecies = self.nspecies
        N = self.N

        # create empty array for transport eq. vector
        @jit("f8[:,:](f8[:,:],f8[:,:],f8[:,:], f8)", nopython=True)
        def _ftv(c_old, s, W, dt):
            # create empty array for transport eq. vector
            b = np.zeros((nspecies, N))
            b[:, :] = c_old + s * W
            # set upper boundary condition
            if left_is_dirichlet:
                b[:, 0] = bc_left
            else:
                b[:, 0] = c_old[:, 0] + s[:, 0] * W[:, 0] - (s[:, 0] / dxs[0]) * bc_left[:]
            if right_is_dirichlet:
                b[:, -1] = bc_right[:]
            else:
                b[:, -1] = c_old[:, -1] + s[:, -1] * W[:, -1] + (s[:, -1] / dxs[-1]) * bc_right[:]
            return b

        self._fill_transport_vector = _ftv

    def solve_1D_transport(self, c_old, s, W, dt,
                           solver=scipy.sparse.linalg.spsolve, check_nan=True, check_solution=False):
        """
        Solve the implicit finite difference formulation of the transport equation for a single timestep
        and all species

        Parameters
        ----------
        c_old : array
            concentrations at last timestep
        s : array
            the s term in the transport equation, calculated as dt / (porosity * R)
        W : array
            source term
        dt : float
            time step
        solver : type
            the matrix solver to use
        check_nan : bool
            if true, check for NaNs in the output
        check_solution : bool
            if true, check if the solution is plausible

        Returns
        -------
        c_new : array
            the concentration at the new timestep

        """
        if self.has_jit:
            b = self._fill_transport_vector(c_old, s, W, dt)
        else:
            b = self.fill_transport_vector(c_old, s, W, dt)
        c_new = np.zeros_like(c_old)

        for specie_number in range(self.nspecies):
            # note that we solve for a copy of the matrix A, as A might be modified in the solver, and we rely on the
            # exact order of entries in A in update_transport_matrix
            c_new[specie_number] = solver(self.A[specie_number].copy(), b[specie_number])

            if check_solution is True:
                check = np.allclose(np.dot(self.A[specie_number].todense(), c_new), b[specie_number])
                assert check is True

        if check_nan is True and np.any(np.isnan(c_new)):
            msg = f"error, nan value in solution in {self.species}"
            raise ValueError(msg)

        return c_new

    def run(self, dt=None, dt_max=None, dt_init=1. * u.yr, f_dt_growth=1.05, ics=None, screen_output=True, t_eval=None,
            enforce_cfl=False, store_source_term=False):
        """
        Iterate over species and timesteps and solve the implicit FD formulation of the transport equation

        Parameters
        ----------
        dt : astropy.units.Quantity
            if provided, set the timestep to a fixed value and ignore all other parameters relate to the time step size.
            Cannot be used together with t_eval
        dt_max : astropy.units.Quantity
            the maximum timestep to be used in the calculation. If None, use the maximum timestep calculated from CFL.
        dt_init : astropy.units.Quantity
            the initial size of the timestep
        f_dt_growth : float
            the fraction by which the timestep size can grow from step to step (bounded by dt_max)
        ics : astropy.units.Quantity
            the initial conditions to be used
        screen_output : bool
            if true, print the progress of the calculation on screen
        t_eval : astropy.units.Quantity
            the times at which we want to store the results of the calculation
        enforce_cfl : bool
            if true, enforce CFL criterion in dt_max
        store_source_term : bool
            if true, store the source terms in each timestep

        Returns
        -------

        """
        if self.model_completed:
            msg = "The model was already run before."
            raise RuntimeError(msg)

        self.check_sanity()

        # set timesteps
        if dt is not None:
            dt_max = dt
            f_dt_growth = 1.0
            dt_init = dt
            if t_eval is not None:
                msg = "Can't use t_eval with a fixed dt"
                raise AttributeError(msg)

        # enforce CFL limit
        if enforce_cfl is True or dt_max is None:
            dt_max = self.get_max_timestep()
            # catch cases in which the maximum allowed timestep is actually too large for our t_eval or time_interval
        if self.time_interval < dt_max:
            dt_max = self.time_interval / 10.
            # logging.warn("maximum timestep too small for time_interval. Set to " + repr(dt_max.to(u.yr)))
        if (dt_max < 0.1 * u.yr):
            logging.warning("dt_max is very small. Are you sure you know what you are doing?")
        # calculate timesteps
        do_output, dts = self._calculate_timesteps(dt_init, dt_max, f_dt_growth, t_eval)
        self.nt = len(dts)

        # set up matrix
        dt = dts[1]
        self.construct_transport_matrix_sparse(dt)

        c = self.convert_ics(ics)

        t = np.cumsum(dts)

        if t_eval is not None:
            nt_eval = t_eval.size
        else:
            t_eval = t * u.s
            nt_eval = self.nt
        # evaluate the external source term beforehand, as we know the timesteps now.
        if self.parameters_source.source_term is not None:
            rates = self.parameters_source.source_term.get_average_rate_over_timestep(t * u.s)
            species = self.parameters_source.source_term.nuclides
            myrates = []
            for s in self.species:
                if s in species:
                    idx = species.index(s)
                    myrates.append(rates[idx].to(u.mol / u.s).value)
                else:
                    if s == "stable":
                        myrates.append(np.zeros(t.size - 1))
                    else:
                        msg = f"species {s} not present in source term!"
                        raise RuntimeError(msg)
            self.external_source = np.array(myrates) / (
                self.volume_cell[self.parameters_source.near_field_index[0]].to(u.m ** 3).value)

        c_eval, stable_mass, fluxes, mass_fluxes, t_fluxes = self._run(c, dts, do_output, nt_eval, screen_output, store_source_term)

        self.results = {"t": t_eval.to(u.s), "c_fluid": c_eval, "stable_mass": stable_mass}
        if np.any(np.isnan(self.results["c_fluid"])):
            logging.warning("NaNs in results array in model with species" + repr(self.species))
        # delete large variables to make sure memory use individual thread stays below 2GB, which
        # otherwise seems to crash multiprocessing
        del c

        if self.options.store_flux:
            fluxes *= u.mol / u.m ** 2 / u.s
            mass_fluxes *= u.kg / u.m ** 2 / u.s
        self.results["fluxes"] = fluxes
        self.results["mass_fluxes"] = mass_fluxes
        self.results["t_fluxes"] = t_fluxes

        if screen_output is True:
            logging.debug('done')
        self.model_completed = True

    def _calculate_timesteps(self, dt_init, dt_max, f_dt_growth, t_eval):
        always_out = False
        ioutput = 1
        if t_eval is None:
            always_out = True
        else:
            t_out = t_eval[ioutput]
        olddt = dt_init / f_dt_growth
        t = 0.0 * u.yr
        dts = [0.0 * u.yr]
        # we assume here t_eval always includes 0 yr:
        do_output = [True]
        # catch the case in which 0 yr is not in t_eval.
        if t_eval is not None and t_eval[0] != 0 * u.yr:
            msg = "t_eval must start from 0 yr!"
            raise RuntimeError(msg)
        while t <= self.time_interval:
            newdt = olddt * f_dt_growth
            if newdt > dt_max:
                newdt = dt_max
            if t + newdt > self.time_interval:
                newdt = self.time_interval - t
            if always_out:
                do_output.append(True)
                storedt = newdt
            elif t + newdt >= t_out:
                storedt = newdt
                newdt = t_out - t
                do_output.append(True)
                if ioutput < t_eval.size - 1:
                    ioutput += 1
                    t_out = t_eval[ioutput]
                else:
                    t_out = self.time_interval * 10.
            else:
                do_output.append(False)
            dts.append(newdt)
            # print(t,newdt.to(u.yr),ioutput,do_output[-1],t+newdt,ioutput,t_eval[ioutput].to(u.yr))
            t += newdt
            if do_output[-1] is True:
                olddt = storedt
            else:
                olddt = newdt
        dts = [dt.to(u.s).value for dt in dts]
        return do_output, dts

    def _run(self, cinit, dts, do_output, nt_eval, screen_output, store_source_term):
        """
        Actually iterate over species and timesteps and solve the implicit FD formulation of the transport equation

        Parameters
        ----------
        cinit : astropy.units.Quantity
            the initial concentrations in each cell and species
        dts : list of floats
            the timesteps to consider
        do_output : list of int
            the list of output times
        nt_eval : int
            the number of outputs in time
        screen_output : bool
            if true, print the progress of the calculation on screen
        store_source_term : bool
            if true, store the source terms in each timestep

        Returns
        -------
        c_eval : array-like
            the array with all the concentrations in each cell, species for all output times
        stable_masses : arrray_like
            the stable mass at each cell for each output time

        """
        store_flux = self.options.store_flux
        mass_conversion_factor = self.mass_conversion_factor.value
        # setup concentration arrays
        # shape is (self.nspecies, self.N)
        new = np.zeros_like(cinit)
        old = np.zeros_like(cinit)
        # setup output array
        c_eval = np.zeros([cinit.shape[0], nt_eval, cinit.shape[1]])

        # if we track (the amount of) the stable species, setup tracking the stable mass as well
        stable_masses = np.zeros([nt_eval, self.N])

        # figure out whether we have the stable species
        if "stable" in self.species:
            if self.species[-1] != "stable":
                msg = "stable species must have index -1"
                raise NotImplementedError(msg)
            has_stable = True
            stable_mass = np.zeros(self.N)

        else:
            has_stable = False

        # set up counter for screen output
        output_counter = 1
        output_int = (self.nt - 1) / 10

        if store_source_term is True:
            self.d3s = []
            self.d4s = []

            self.Ws = np.zeros((self.nspecies, self.nt))
        fluxes = None
        mass_fluxes = None
        t_fluxes = None
        if store_flux is True:
            fluxes = np.zeros([self.nt - 1, self.N - 1])
            mass_fluxes = np.zeros([self.nt - 1, self.N - 1])
            self.xmid = (self.x[1:] + self.x[:-1]) / 2.0
            t_fluxes = np.cumsum(dts[1:])
            dxi = np.diff(self.x)
            mf = mass_conversion_factor[:, None]
        # initialize source/sink term that handles decay
        W = np.zeros((self.nspecies, self.N))
        # iterate over timesteps
        last_step = dts[1]
        new[:] = cinit
        old[:] = cinit
        ioutput = 1
        # multiply the result with R to get the total concentration (sorbed + in solution)
        c_eval[:, 0, :] = cinit * self.R_
        t_current = 0.0
        for timestep in range(1, self.nt):
            # need to explicitly set source to zero in case ingnore_decay is True
            W[:] = 0.0
            dt = dts[timestep]
            if screen_output is True and output_counter >= output_int:
                logging.debug(f'timestep {timestep} / {self.nt}')
                output_counter = 0

            if self.options.include_decay is True:
                # get decay source/sink term:
                y = old.T
                # this is a little convolved, so some hints:
                if self.options.use_bateman:
                    # temporary array for the bateman solution that is, the solution to the pure decay problem in each
                    # cell, given the concentrations in y and the size of the timestep dt
                    y_update = np.zeros_like(y)
                    # loop over the subchains involved
                    for i, chain_index in enumerate(self.chain_indizes):
                        # load the species in this chain into a temporary array
                        y_temp = y[:, chain_index].T
                        # remove spurious negative values
                        y_temp[y_temp < 0] = 0.
                        # if we have overlapping subchains, mask out the initial concentrations that we already
                        # accounted for. All concentrations with index equal or larger num_states[i] will be
                        # masked. Note that the index is relative to the subchain.
                        if self.num_states[i] > 0:
                            y_temp[self.num_states[i]:, :] = 0.
                        # feed y_temp to the bateman solver, get the result (for this subchain)
                        y_bateman = bateman_full(y_temp * self.R[:, chain_index].T, self.chain_lambdas[i],
                                                 dt).T / self.R[:, chain_index]
                        # sum up to get the full solution for all subchains
                        y_update[:, chain_index] += y_bateman
                        if has_stable:
                            delta_stable = y_bateman[:, -1] - y_temp[-1, :]
                            stable_mass += delta_stable * self.phi[:, chain_index[-2]] / self.phi[:, -1] * \
                                           mass_conversion_factor[chain_index[-2]]
                    W[:, :] = self.R.T * ((y_update - y) / dt).T * self.phi.T
                else:
                    d3 = (self.R * y) @ self.lambda_plus
                    d4 = -np.multiply(self.R * y, self.lambda_minus)
                    W[:, :] = ((d3 + d4) * self.phi).T
                    if has_stable:
                        stable_mass += (-d4[:, self.decays_to_stable_idx] *
                                        self.phi[:, self.decays_to_stable_idx] *
                                        mass_conversion_factor[self.decays_to_stable_idx]).sum(axis=1)\
                                        / self.phi[:, -1] * dt
            # set W to zero in the boundary nodes.
            W[:, 0] = 0.
            W[:, -1] = 0.
            if self.external_source is not None:
                ix = self.parameters_source.near_field_index[0]
                W[:, ix] += self.external_source[:, timestep - 1]
            if store_source_term is True:
                self.d3s.append(d3)
                self.d4s.append(d4)

                # store mass gain or loss per specie
                self.Ws[:, timestep] = np.sum(W, axis=1)

            # term that we will need for constructing the transport vector
            s = dt / (self.phi.T * self.R_)
            if last_step != dt:
                self.update_transport_matrix(last_step, dt)
            new = self.solve_1D_transport(old, s, W, dt, check_nan=True)
            if np.any(np.isnan(new)):
                logging.warning("NaNs in new array in model with species" + repr(self.species))
            if store_flux is True:
                dc = np.diff(new, axis=1)
                jd = -(self.De_mid[1:-1].T * dc) / dxi
                adv = self.q.T * new
                ja = (adv[:, 1:] + adv[:, :-1]) / 2.0
                j = jd + ja
                fluxes[timestep - 1] = j.sum(axis=0)
                mass_fluxes[timestep - 1] = (j * mf).sum(axis=0)
            if do_output[timestep]:
                # multiply the result with R to get the total concentration (sorbed + in solution)
                c_eval[:, ioutput, :] = new[:] * self.R_
                if has_stable:
                    stable_masses[ioutput] = stable_mass
                ioutput += 1
            t_current += dt
            output_counter += 1
            last_step = dt
            old[:] = new
        if np.any(np.isnan(c_eval)):
            logging.warning("NaNs in new array in model with species" + repr(self.species))
        return c_eval, stable_masses, fluxes, mass_fluxes, t_fluxes

    def check_sanity(self):
        """
        Some checks to ensure parameters behave okay

        Returns
        -------

        """
        if (np.any((self.De_mid > 1.) | (self.De_mid < 0.))):
            logging.warning("check_sanity(): De_mid > 1 or De_mid < 0. in at least one cell")
        if (np.any(~np.isfinite(self.De_mid))):
            logging.warning("check_sanity(): De_mid is NaN or infinite in at least one cell")
        if (np.any((self.phi > 1.) | (self.phi < 0.))):
            logging.warning("check_sanity(): porosity < 0 or > 1 in at least one cell")

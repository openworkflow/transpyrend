"""An interface for using the DOP853 timestepping scheme from scipy"""
import logging
import warnings

import astropy
import numpy as np
from astropy import units as u
from numba import jit
from scipy.integrate import solve_ivp
from transpyrend.core.base import BaseTransportModel1D, OptionArgs



class DOP853TransportModel1D(BaseTransportModel1D):
    """
    a wrapper class to solve the transport problem with the help of scipy's ODE solver interface solve_ivp. Note that
    this implementation currently does not support tracking the total mass of stable decay products. However, the stable
    amount of mass is tracked.


    Parameters
    ----------
    species : list of str
        list of nuclides to consider
    parameters_rock : list of dict or dict
        the parameters of the geological units. dict if only one unit is considered.
    parameters_transport_material : list of dict or dict
        the transport parameters of the nuclides. dict if only one geological unit is considered.
    geometry : GeometryArgs or dict
        the geometry of the problem
    bcs : BCArgs or dict
        the boundary conditions
    time_interval : astropy.units.Quantity
        the total time interval considered in the model
    options : OptionArgs or dict
        optional parameters considering the solver
    parameters_source: SourceArgs or dict
        parameters concerning the source model

    """

    def __init__(self,
                 species,
                 parameters_rock, parameters_transport_material=None,
                 geometry=None,
                 bcs=None,
                 time_interval=1e6 * u.yr,
                 options=None,
                 parameters_source=None,
                 use_coeff=False
                 ):
        options = OptionArgs(options)
        if options.external_solver_method is None:
            options.external_solver_method = "DOP853"
        if "rtol" not in options.external_solver_options:
            options.external_solver_options["rtol"] = 1e-13
        if "atol" not in options.external_solver_options:
            options.external_solver_options["atol"] = 1e-13

        super().__init__(species, parameters_rock, parameters_transport_material=parameters_transport_material,
                         geometry=geometry,
                         bcs=bcs,
                         time_interval=time_interval,
                         options=options,
                         parameters_source=parameters_source)
        # deal with the stable species:
        if (isinstance(self.parameters_transport_material, dict)):
            if ("stable" in self.parameters_transport_material):
                if (options.nuclid_database != "vsg"):
                    raise NotImplementedError()
                if ("effective_diffusion_coefficient" not in self.parameters_transport_material["stable"]):
                    self.parameters_transport_material["stable"][
                        "effective_diffusion_coefficient"] = 0.0 * u.m ** 2 / u.s
        else:
            for p in self.parameters_transport_material:
                if ("stable" in p):
                    if (options.nuclid_database != "vsg"):
                        raise NotImplementedError()
                    if ("effective_diffusion_coefficient" not in p["stable"]):
                        p["stable"]["effective_diffusion_coefficient"] = 0.0 * u.m ** 2 / u.s


        self.v *= self.phi

        # setup the decays
        self.setup_decays()
        # setup the integrand
        self.use_coeff = use_coeff
        self.setup_solver()

        if options.store_flux:
            raise NotImplementedError()

    def setup_solver(self):
        """
        defines the ODE we want to solve.

        Returns
        -------

        """
        # CAUTION: Note that once the integrand here has been defined, changing the values of e.g. the diffusion
        # coefficients or the cell volume will not affect the integrand! You need to call setup_solver in this case once
        #  again.
        N = self.N
        nspecies = self.nspecies
        v = self.v
        dxs = np.zeros((self.N, self.nspecies))
        for i in range(self.nspecies):
            dxs[:, i] = self.dxs

        S = np.zeros((self.N, self.nspecies))
        S[1:-1, :] = 2.0 / (self.x[2:, np.newaxis] - self.x[:-2, np.newaxis])
        S[0, :] = 1.0 / (self.x[1, np.newaxis] - self.x[0, np.newaxis])
        S[-1, :] = 1.0 / (self.x[-1, np.newaxis] - self.x[-2, np.newaxis])

        T = np.zeros((self.N, self.nspecies))
        T[:-1, :] = 1. / (self.x[1:, np.newaxis] - self.x[:-1, np.newaxis])

        U = np.zeros((self.N, self.nspecies))
        U[1:, :] = 1. / (self.x[1:, np.newaxis] - self.x[:-1, np.newaxis])

        lambda_plus = self.lambda_plus
        lambda_minus = self.lambda_minus
        R = self.R
        D = self.D
        phi = self.phi[1:-1]
        Dm = self.De_mid[1:-1]
        bc_neumann_left = self.bc_neumann_left
        bc_neumann_right = self.bc_neumann_right
        is_neumann_left = (bc_neumann_left is not None)
        is_neumann_right = (bc_neumann_right is not None)
        prefactor = R[1:-1, :] * phi
        prefactor_left = R[0] * self.phi[N - 1]
        prefactor_right = R[N - 1] * self.phi[N - 1]
        has_external_source = False
        if self.parameters_source.source_term is not None:
            logging.info("generating interpolating function for external source term")
            f_source = self.parameters_source.source_term.get_rate_function()
            has_external_source = True
            if len(self.parameters_source.near_field_index) > 1:
                msg = "Only implemented for single cell source for now - sorry"
                raise NotImplementedError(msg)
            source_index = self.parameters_source.near_field_index[0]
            volume_cell = self.volume_cell.to(u.m ** 3).value

        @jit(nopython=True)
        def dcidt_fast(t, y):
            """
            A fast, jitted implementation of the right hand side of our ODE

            Parameters
            ----------
            t : float
                the time at which we want evaluate the ODE
            y : array-like
                function values at time t

            Returns
            -------
            Y : array-like
                the right hand side of the ODE

            """
            rhs = np.zeros((N, nspecies))
            y = y.reshape(N, nspecies)

            d1 = S[1:-1] * (Dm[1:] * T[1:-1] * (y[2:, :] - y[1:-1, :]) - Dm[:-1] * U[1:-1] * (y[1:-1, :] - y[0:-2, :]))
            # centered difference for advection
            d2 = -1. * (v[2:] * y[2:, :] - v[0:-2] * y[0:-2, :]) / (dxs[:-1] + dxs[1:])[1:]
            # source terms:
            # decay into species i
            Ry = R*y
            d3 = (Ry @ lambda_plus)
            # decay of species i
            d4 = -np.multiply(Ry, lambda_minus)
            # left boundary
            if is_neumann_left:
                adv = - v[0] * (y[1, :] + y[0, :]) / dxs[0]
                diff = -2. / dxs[0] * (-D[0, :] * (y[1, :] - y[0, :]) / dxs[0] - bc_neumann_left)
                rhs[0] = ((diff + adv) / prefactor_left + (d3[0] + d4[0])/R[0])
            else:
                rhs[0] = 0.
            # right boundary
            if is_neumann_right:
                adv = - v[N - 1] * (y[N - 1, :] + y[N - 2, :]) / dxs[-1]
                diff = -2. / dxs[N - 1] * (bc_neumann_right + D[N - 1, :] * (y[N - 1, :] - y[N - 2, :]) / dxs[N - 1])
                rhs[N - 1] = ((diff + adv) / prefactor_right + (d3[N - 1] + d4[N - 1])/R[N - 1])
            else:
                rhs[N - 1] = 0.
            # update normal cells
            rhs[1:-1, :] = ((d1 + d2) / prefactor + (d3[1:-1, :] + d4[1:-1, :])/R[1:-1, :])
            if has_external_source:
                source_term = f_source(t)
                rhs[source_index] += source_term / volume_cell[source_index] / phi[source_index - 1]

            return rhs.reshape(y.size)

        # the PDE we are solving
        self.integrand = dcidt_fast

    def run(self, ics=None, t_eval=None, dt=None, dt_max=None, dt_init=1. * u.yr, f_dt_growth=None, warn_negative=True):
        """
        runs the model

        Parameters
        ----------
        ics : astropy.units.Quantity
            initial conditions to use with shape (self.N, self.nspecies). If scalar, all cells will be filled with
            those values.
            If size is self.nspecies, each species get a different value, but in all cells.
        t_eval : astropy.units.Quantity
            the times at which to store the output of the model
        dt : astropy.units.Quantity
            the maximum timestep used in the calculation
        warn_negative : bool
            if true, emit warning if there are negative values in the output

        Returns
        -------

        """
        if self.model_completed:
            msg = "The model was already run before."
            raise RuntimeError(msg)
        if dt is not None:
            warnings.warn("ExplicitModel1D does not support setting a constant timestep")
        if f_dt_growth is not None:
            warnings.warn("ExplicitModel1D does not support setting a growth rate for the timestep")
        if (t_eval is None):
            t_eval = self.default_t_eval()
        else:
            t_eval = t_eval.to(u.s)

        ics = self.convert_ics(ics).T

        if (self.bcs.bc_type_left == "Dirichlet"):
            ics[0, :] = self.bc_dirichlet_left
        if (self.bcs.bc_type_right == "Dirichlet"):
            ics[-1, :] = self.bc_dirichlet_right
        if (isinstance(dt_max, astropy.units.quantity.Quantity)):
            max_dt_cfl = self.get_max_timestep()
            if (max_dt_cfl < dt_max):
                logging.warning("Warning: size of timestep violates CFL/half life condition - maximum is " +
                                repr(max_dt_cfl.to(u.yr)))
            dt_max = dt_max.to(u.s).value

        elif (dt_max is None):
            # always enforce the CFL/HL criterion
            dt_max = self.get_max_timestep().to(u.s).value

        if (ics.shape != (self.N, self.nspecies)):
            msg = (
                "ICs have shape", ics.shape, "instead of the required",
                (self.N, self.nspecies),
                self.species
            )
            raise ValueError(msg)
        t0 = 0.0 * u.s
        if self.parameters_source.source_term is not None:
            st = self.parameters_source.source_term
            # check if we need to change the ics now
            if st.irf_time == 0.0 * u.yr:
                logging.info("Instant release happens at t=0. Adding IRF to ICs.")

            else:
                logging.info(f"Instant release happens at t={st.irf_time}. Requiring ICs to be zero.")
                assert np.all(ics == 0.0 * u.mol / u.m ** 3)
                t0 = st.irf_time.to(u.s)
            irf = np.zeros(self.nspecies)
            nfi = self.parameters_source.near_field_index[0]
            for i, spec in enumerate(self.species):
                ispec = st.nuclides.index(spec)
                irf[i] = (st.irf_inventory[ispec] / self.volume_cell[nfi] / self.phi[nfi, i]).to(u.mol / u.m ** 3).value
            ics[self.parameters_source.near_field_index[0]] += irf
        ics = np.reshape(ics, ics.size)
        calculated_t_eval = t_eval[t_eval >= t0]
        self.results = solve_ivp(self.integrand, [t0.value, (self.time_interval).to(u.s).value],
                                 ics, vectorized=False, max_step=dt_max, first_step=dt_init.to(u.s).value,
                                 t_eval=calculated_t_eval, **self.options.external_solver_options,
                                 method=self.options.external_solver_method)
        if not self.results["success"]:
            msg = "Integrator failed!"
            raise RuntimeError(msg)
        # added new variable to store modelled concentrations here
        # this is because i always get confused with the y variable
        nsteps = self.results["y"].shape[1]
        c_fluid = np.reshape(self.results["y"], [self.N, self.nspecies, nsteps]) * self.R[:, :, None]
        # switch axes to conform with c variable in implicit solver ([specie, timestep, x])
        c_fluid = np.moveaxis(c_fluid, 0, -1)
        # if we track (the amount of) the stable species, setup tracking the stable mass as well. This does not work
        # in the dop853 model, this is just for consistency.
        self.results["stable_mass"] = np.zeros([nsteps, self.N])
        # store concentrations as results["c"]
        self.results["c_fluid"] = c_fluid
        # remove the old y array to save space
        del self.results["y"]
        # in cases we used a source term, there could be empty missing frames. Readd them.
        offset = t_eval.size - calculated_t_eval.size
        if offset != 0:
            temp = np.zeros([self.nspecies, t_eval.size, self.N])
            offset = t_eval.size - calculated_t_eval.size
            temp[:, offset:, :] = self.results["c_fluid"]
            self.results["c_fluid"] = temp

        # give result["t"] a unit
        self.results["t"] = t_eval.to(u.s)

        if warn_negative and np.any(self.results["c_fluid"] < -1e20):
            logging.warning("some concentrations are negative.")
        self.model_completed = True


    def setup_bateman(self, chains, states):
        """
        Warn user that Bateman solver is not implemented for this class.

        """
        logging.warning("Can't use Bateman solution in explicit solver. Falling back to default.")

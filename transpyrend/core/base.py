"""
base for all transport model classes
"""
import logging

import astropy
import numpy as np
from astropy import units as u
from transpyrend.core.args import BCArgs, GeometryArgs, OptionArgs, SourceArgs
from transpyrend.utils.nuclides import mass2mol, get_daughters
from transpyrend.utils.nuclides import mol2mass, mol2activity_vsg
from transpyrend.utils.misc import average_midpoint_variable
from transpyrend.utils.misc import interpolator, summed

from functools import cached_property

def store_model(model, fname):
    """
    stores a model by serializing it with dill
    CAUTION: the output might not be portable

    Parameters
    ----------
    model : any serializable object
        object to serialize
    fname : str
        file name
    """
    from dill import dump
    with open(fname, "wb") as file:
        dump(model, file)


def load_model(fname):
    """
    loads a model by deserializing it with dill
    CAUTION: the output might not be portable

    Parameters
    ----------
    fname : str
        file name

    Returns
    -------
    model : modeltype
        the deserialized model
    """
    from dill import load
    with open(fname, "rb") as file:
        return load(file)


class BaseTransportModel1D:
    """
    Base class for transport models. Some functionality to setup model and extract data.
    Should only be called by child classes.

        Parameters
        ----------
        species : list of str
            list of nuclides to consider
        parameters_rock : list of dict
            the parameters of the geological units
        parameters_transport_material : list of dict
            the transport parameters of the nuclides.
        geometry : GeometryArgs or dict
            the geometry of the problem
        bcs : BCArgs or dict
            the boundary conditions
        time_interval : astropy.units.Quantity
            the total time interval considered in the model
        options : OptionArgs or dict
            optional parameters considering the solver
        parameters_source: SourceArgs or dict
            optional parameters for configuring a radionuclide source term

        """

    def __init__(self,
                 species,
                 parameters_rock,
                 parameters_transport_material=None,
                 geometry=None,
                 bcs=None,
                 time_interval=1e6 * u.yr,
                 options=None,
                 parameters_source=None):
        # set to true once model was run
        self.model_completed = False
        bcs = BCArgs(bcs)
        # the boundary conditions for the model
        self.bcs = bcs
        geometry = GeometryArgs(geometry)
        # the geometry of the problem
        self.geometry = geometry
        options = OptionArgs(options)
        # options related to the solver
        self.options = options
        # a list of dictionaries,
        # one for each geological unit. In the latter case, it is required to include the thickness as a parameter in
        # each dictionary.
        self.parameters_rock = parameters_rock
        # a list of dictionaries,
        # one for each geological unit.
        self.parameters_transport_material = parameters_transport_material
        # the species we will consider
        self.species = species
        # the number of species
        self.nspecies = len(self.species)
        if parameters_transport_material is None:
            # placeholder
            raise NotImplementedError()
        # calculate xmax from parameters_rock
        if isinstance(parameters_rock, list):
            xmax = 0.0 * u.m
            for unit in parameters_rock:
                xmax += unit["thickness"]
        else:
            xmax = parameters_rock["thickness"]
        # the right domain boundary
        self.xmax = xmax
        if geometry.x is None:
            # the size of a cell in meters
            dx = self.geometry.dx.to(u.m).value
            # the number of grid points
            self.N = int(self.xmax.value / dx) + 1
            # find cell sizes for nodes
            self.x = dx * np.arange(0, self.N)
        else:
            self.N = geometry.x.size
            self.x = geometry.x.to(u.m).value
        dxs = np.zeros(self.N)
        dxs[1:-1] = np.diff(self.x)[:-1]
        dxs[0] = (self.x[1] - self.x[0]) / 2.0
        dxs[-1] = (self.x[-1] - self.x[-2]) / 2.0
        self.dxs = dxs
        # the area covered by a cell
        # the volume of a cell
        self.volume_cell = (self.dxs * u.m * self.geometry.area_cell).to(u.m ** 3)

        # the time interval we need to consider for the simulation
        self.time_interval = time_interval
        # the CFL limit for advection
        self.cfl_limit = 0.25
        # the CFL limit for diffusion
        self.cfl_d_limit = 100.
        # the fraction of a half life we want to resolve in time
        self.hl_limit = 0.5

        parameters_source = SourceArgs(parameters_source)
        # the parameters of an external source term
        self.parameters_source = parameters_source
        # the external source term
        self.external_source = None

        # setup model parameters
        self.setup_parameters()
        self.setup_bc()

    def average_D(self, D):
        """
        calculate averaged diffusion coefficients at midpoints between cells

        Parameters
        ----------
        D : astropy.units.Quantity
            diffusion coefficients considered

        Returns
        -------
        D_mid : astropy.units.Quantity
            diffusion coefficients at cell mid-points
        """
        De_mid = np.zeros((self.N + 1, self.nspecies))
        De_ = D.to(u.m ** 2 / u.s).value
        De_mid[1:-1] = average_midpoint_variable(De_, self.dxs, self.options.D_averaging_method)
        De_mid[0] = De_[0]
        De_mid[-1] = De_[-1]
        return De_mid

    def setup_bc(self):

        """
        setup the boundary conditions at left and right-hand boundaries

        Parameters
        ----------

        Returns
        -------

        """

        # set up boundary conditions
        self.bc_dirichlet_left = None
        self.bc_dirichlet_right = None
        self.bc_neumann_left = None
        self.bc_neumann_right = None

        bcs = self.bcs

        if not isinstance(bcs.bc_left, list) and bcs.bc_left.isscalar:
            bcs.bc_left = [bcs.bc_left for i in range(self.nspecies)]
        if not isinstance(bcs.bc_right, list) and bcs.bc_right.isscalar:
            bcs.bc_right = [bcs.bc_right for i in range(self.nspecies)]

        bc_amount_units_left = u.mol / u.m ** 3
        bc_amount_units_right = u.mol / u.m ** 3
        if bcs.bc_type_left == "Neumann" or bcs.bc_type_left == "noFlow":
            bc_amount_units_left /= u.yr
        if bcs.bc_type_right == "Neumann" or bcs.bc_type_right == "noFlow":
            bc_amount_units_right /= u.yr
        # second clause is for the Neumann/noFlow case:
        if (bcs.bc_left[0].unit.physical_type == "mass density") or (
                (bcs.bc_left[0] * u.yr).unit.physical_type == "mass density"):
            converted_bcs = []
            for i, bci in enumerate(bcs.bc_left):
                converted_bcs.append(mass2mol(bci, self.species[i], bc_amount_units_left))
            bcs.bc_left = converted_bcs
        if (bcs.bc_right[0].unit.physical_type == "mass density") or (
                (bcs.bc_right[0] * u.yr).unit.physical_type == "mass density"):
            converted_bcs = []
            for i, bci in enumerate(bcs.bc_right):
                converted_bcs.append(mass2mol(bci, self.species[i], bc_amount_units_right))
            bcs.bc_right = converted_bcs

        if bcs.bc_type_left == "Dirichlet":
            bcvalues = [bci.to(bc_amount_units_left).value for bci in bcs.bc_left]
            self.bc_dirichlet_left = bcvalues / self.phi[0]
        elif bcs.bc_type_left == "Neumann":
            bcflux = [bci.to(bc_amount_units_left).value for bci in bcs.bc_left]
            self.bc_neumann_left = np.array(bcflux) / self.phi[0]

        if bcs.bc_type_right == "Dirichlet":
            bcvalues = [bci.to(bc_amount_units_right).value for bci in bcs.bc_right]
            self.bc_dirichlet_right = bcvalues / self.phi[-1]
        elif bcs.bc_type_right == "Neumann":
            bcflux = [bci.to(bc_amount_units_right).value for bci in bcs.bc_right]
            self.bc_neumann_right = np.array(bcflux) / self.phi[-1]

    def default_t_eval(self):
        """
        return the default times - every 1000 years, starting from t=0 -
        at which we store the output of the calculation, given the total time span of consideration.

        Returns
        -------
        t_eval :  astropy.units.Quantity
            times at which the model should produce an output
        """
        t_eval = np.arange(0, (self.time_interval + 1000 * u.yr).to(u.s).value, (1000 * u.yr).to(u.s).value)
        return t_eval * u.s

    def set_cfl_limit(self, cfl_limit):
        """
        set the cfl limit for the advection

        Parameters
        ----------
        cfl_limit : float
            cfl limit

        Returns
        -------

        """
        self.cfl_limit = cfl_limit

    def set_cfl_d_limit(self, cfl_limit):
        """
        set the cfl limit for diffusion

        Parameters
        ----------
        cfl_limit : float
            cfl limit

        Returns
        -------

        """
        self.cfl_d_limit = cfl_limit

    def set_hl_limit(self, hl_limit):
        """
        set the fraction of the half life a time step may take, i.e. setting this to 1 means that the time step will not
        be larger than the half life of the shortest-lived nuclide in the model.

        Parameters
        ----------
        hl_limit : float
            the fraction of a half life

        Returns
        -------

        """
        self.hl_limit = hl_limit

    def get_max_timestep(self, cap_timestep=None):
        """
        calculate the maximum allowed time step; checks CFL and half lifes of considered nuclides

        Parameters
        ----------
        cap_timestep : astropy.units.Quantity
            if given, cap the calculated timestep by this value

        Returns
        -------
        max_step : astropy.units.Quantity
            the maximum timestep allowed
        """
        # we use divide here to get rid of case where some entries of v are zero and throw a divide_by_zero error
        # (happens for the "stable" species)
        # noinspection PyArgumentList
        mindx = self.dxs.min()
        max_dt_adv = np.divide(self.cfl_limit * mindx, np.abs(self.v),
                               out=np.ones_like(self.v) * 1e50, where=self.v != 0,
                               dtype=float).min() * u.s
        Dmax = self.D.max()
        if Dmax == 0:
            Dmax = 1e-30
        max_dt_diff = (self.cfl_d_limit * mindx ** 2 / Dmax) * u.s
        max_dt = min(max_dt_adv, max_dt_diff)
        if self.options.include_decay:
            min_half_life = 1e50 * u.s
            for nuclid in self.species:
                daughters = get_daughters(nuclid, self.options.nuclid_database)
                for _daughter, lam in daughters.items():
                    half_life = np.log(2) / lam * u.s
                    if half_life < min_half_life:
                        min_half_life = half_life
            max_dt_decay = min_half_life * self.hl_limit
            max_dt = min(max_dt_decay, max_dt)

        if cap_timestep is not None and max_dt > cap_timestep:
            max_dt = cap_timestep

        return max_dt

    def find_spatial_index(self, x):
        """
        find the index of the node close to position x

        Parameters
        ----------
        x : astropy.units.Quantity
            the position x

        Returns
        -------
        ix : the spatial index belonging to x

        """
        x = x.to(u.m).value
        if x > self.x[-1]:
            msg = "x > grid size"
            raise RuntimeError(msg)
        delta = abs(self.x - x)
        return np.where(delta == delta.min())[0][0]

    def get_concentrations_x(self, ispecies, itime=-1, quantity="mass density", units=None, per_rock_volume=False):
        """
        get the concentration of a species as a function of location at a specific time step.

        Parameters
        ----------
        ispecies : string or int
            name or index of species
        itime :  int
            time index of the data
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        x : astropy.units.Quantity
            array with spatial locations at which the concentrations are estimated
        c : astropy.units.Quantity
            array with concentrations
        """
        quantities = ["mass density", "molar density", "activity density", "dose density"]
        if quantity not in quantities:
            msg = "quantity must be one of ", quantities
            raise NotImplementedError(msg)
        if isinstance(ispecies, str):
            if ispecies in self.species:
                if self.species.count(ispecies) > 1:
                    msg = "There are multiple species with this name, don't know what to do."
                    raise NotImplementedError(msg)
                ispecies = self.species.index(ispecies)
            else:
                msg = "Unknown species " + ispecies
                raise RuntimeError(msg)

        if isinstance(itime, astropy.units.quantity.Quantity):
            msg = "interpolation in time is not implemented"
            raise NotImplementedError(msg)
        if self.species[ispecies] == "stable" and quantity == "mass density":
            if units is None:
                units = u.kg / u.m ** 3
            results = (self.results["stable_mass"][itime, :] * u.kg / u.m ** 3).to(units)
        else:
            results = self.results["c_fluid"][ispecies, itime, :] * u.mol / u.m ** 3
            # results is in mol/m**3. We might need to convert it.
            results, units = self._scale_to_units(ispecies, quantity, results, units)
        if per_rock_volume:
            results *= self.phi[:, ispecies]
        results = results.to(units)
        return self.x * u.m, results

    def _scale_to_units(self, ispecies, quantity, data, units):
        """
        helper functions to scale retrieved concentrations to the right physical type and unit

        Parameters
        ----------
        ispecies : int
            index of the species
        quantity : str
            "mass density" or "molar density" or "activity density" or "dose density"
        data : astropy.units.Quantity
            array with concentrations
        units : astropy.units.Unit
            desired units

        Returns
        -------
        data : astropy.units.Quantity
            scaled array with concentrations
        units : astropy.units.units
            the physical units
        """
        if quantity == "mass density":
            if units is None:
                units = u.kg / u.m ** 3
                data = data * self.mass_conversion_factor[ispecies]
        elif quantity == "molar density":
            if units is None:
                units = u.mol / u.m ** 3
        elif quantity == "activity density":
            if units is None:
                units = u.Bq / u.m ** 3
            data = data * self.activity_conversion_factor[ispecies]
        elif quantity == "dose density":
            raise NotImplementedError
        else:
            raise NotImplementedError()
        return data, units

    def get_concentrations_t(self, ispecies, ix=0, quantity="mass density", units=None, per_rock_volume=False):
        """
        get the concentration of a species as a function of time at a specific spatial cell index.

        Parameters
        ----------
        ispecies : string or int
            name or index of species
        ix :  int
            cell index of the data
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        t : astropy.units.Quantity
            times at which the concentrations are estimated
        c : astropy.units.Quantity
            array with concentrations
        """
        quantities = ["mass density", "molar density", "activity density", "dose density"]
        if quantity not in quantities:
            msg = "quantity must be one of ", quantities
            raise NotImplementedError(msg)
        if isinstance(ix, astropy.units.quantity.Quantity):
            ixx = self.find_spatial_index(ix)
            logging.info(f"retrieving data for x= {self.x[ixx]*u.m} instead of requested x = {ix}")
            ix = ixx
        if isinstance(ispecies, str):
            if ispecies in self.species:
                if self.species.count(ispecies) > 1:
                    msg = "There is multiple species with this name, dont know what to do."
                    raise NotImplementedError(msg)
                ispecies = self.species.index(ispecies)
            else:
                msg = "Unknown species " + ispecies
                raise RuntimeError(msg)
        if self.species[ispecies] == "stable" and quantity == "mass density":
            if units is None:
                units = u.kg / u.m ** 3
            results = (self.results["stable_mass"][:, ix] * u.kg / u.m ** 3).to(units)
        else:
            results = self.results["c_fluid"][ispecies, :, ix] * u.mol / u.m ** 3
            results, units = self._scale_to_units(ispecies, quantity, results, units)
        if (per_rock_volume):
            results *= self.phi[ix, ispecies]
        results = results.to(units)
        return (self.results["t"]).to(u.yr), results

    @interpolator
    def get_interpolated_concentrations_x(self, ispecies, with_units=True, smooth=False, itime=-1, quantity="mass density", units=None,
                                          per_rock_volume=False):
        """
        get interpolated concentration as a function of space.

        Parameters
        ----------
        ispecies : string or int
            name or index of species
        ix :  int
            node index
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        x : astropy.units.Quantity
            array with spatial locations at which the concentrations are estimated
        interp1d_with_units : function
            interpolating function
        """
        return self.get_concentrations_x(ispecies, itime=itime, quantity=quantity,
                                                     units=units,
                                                     per_rock_volume=per_rock_volume)

    @interpolator
    def get_interpolated_concentrations_t(self, ispecies, ix=0, with_units=True, smooth=False, quantity="mass density", units=None,
                                          per_rock_volume=False):
        """
        get interpolated concentration as a function of time.

        Parameters
        ----------
        ispecies : string or int
            name or index of species
        ix :  int
            node index
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        t : astropy.units.Quantity
            times at which the concentrations are estimated
        interp1d_with_units : function
            interpolating function
        """
        return self.get_concentrations_t(ispecies, ix=ix, quantity=quantity,
                                                     units=units,
                                                     per_rock_volume=per_rock_volume)

    @summed
    def get_summed_concentrations_x(self, itime=-1, quantity="mass density", units=None, ignore_stable=True,
                                    per_rock_volume=False):
        """
        get the summed concentrations as a function of space.

        Parameters
        ----------
        kind : str
            "time" or "space"
        ix :  int
            node index
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        ignore_stable : bool
            whether to consider the stable species as well
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        y : astropy.units.Quantity
            positions at which the concentrations are estimated
        c : astropy.units.Quantity
            array with summed concentrations
        """
        return self.get_concentrations_x

    @summed
    def get_summed_concentrations_t(self, ix=0, quantity="mass density", units=None, ignore_stable=True,
                                    per_rock_volume=False):
        """
        get the summed concentration curve as a function of time.

        Parameters
        ----------
        ix :  int
            node index
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        ignore_stable : bool
            whether to consider the stable species as well
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        y : astropy.units.Quantity
            times at which the concentrations are estimated
        c : astropy.units.Quantity
            array with summed concentrations
        """
        return self.get_concentrations_t

    @interpolator
    def get_interpolated_summed_concentrations_x(self, itime=-1, with_units=True, smooth=False, quantity="mass density",
                                                 units=None, ignore_stable=True, per_rock_volume=False):
        """
        get the interpolated summed concentration curve as function of space.

        Parameters
        ----------
        kind : str
            "time" or "space"
        itime :  int
            time index of the data
        with_units : bool
            whether to use astropy units in output
        smooth: bool
            whether to smooth the results with a Gaussian
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        ignore_stable : bool
            whether to consider the stable species as well
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        x : astropy.units.Quantity
            positions the interpolator was initialized with
        interp : astropy.units.Quantity
            interpolating function
        """
        return self.get_summed_concentrations_x(itime=itime,
                                                quantity=quantity, units=units,
                                                ignore_stable=ignore_stable,
                                                per_rock_volume=per_rock_volume)

    @interpolator
    def get_interpolated_summed_concentrations_t(self, ix=0, quantity="mass density",
                                                 units=None, ignore_stable=True, per_rock_volume=False):
        """
        get the interpolated summed concentration curve as function of time.

        Parameters
        ----------
        kind : str
            "time" or "space"
        ix :  int
            node index
        with_units : bool
            whether to use astropy units in output
        smooth: bool
            whether to smooth the results with a Gaussian
        quantity : string
            "mass density" or "molar density" or "activity density" or "dose density"
        units : astropy.units.Quantity
            physical units for the concentration. must be consistent with quantity
        ignore_stable : bool
            whether to consider the stable species as well
        per_rock_volume : bool
            if true, return the concentration per rock volume instead of per fluid volume

        Returns
        -------
        t : astropy.units.Quantity
            times the interpolator was initialized with
        interp : astropy.units.Quantity
            interpolating function
        """
        return self.get_summed_concentrations_t(ix=ix,
                                                quantity=quantity, units=units,
                                                ignore_stable=ignore_stable,
                                                per_rock_volume=per_rock_volume)

    def massage_ics(self, ics):
        """
        construct an IC matrix from a dictionary

        Parameters
        ----------
        ics : dict
            dictionary relating nuclid names to initial conditions vectors

        Returns
        -------
        myics : astropy.units.Quantity
            IC vector for the model
        """
        myspecies = self.species
        for i, nuclid in enumerate(myspecies):
            if i == 0:
                units = ics[nuclid].unit
                myics = np.zeros([self.N, self.nspecies]) * units
            if (nuclid == "stable"):
                a = 0.0 * units
            else:
                a = ics[nuclid].to(units)
            myics[:, i] = a
        return myics

    def find_all_transport_lengths(self, repo_index, total_mass, total_amount,
                                   threshold=1e-4, itime=-1, area=None, species=None, repo_width=1,
                                   include_stable=True):
        """
        return the 4 transport lengths - 2x2, one for each direction of transport, one for mass and one for amount of
        substance

        Parameters
        ----------
        total_mass : astropy.units.Quantity
            the total mass in the model
        total_amount : astropy.units.Quantity
            the total amount of substance in the model
        repo_index : int
            left edge of the repository as cell index
        threshold : astropy.units.Quantity
            the mass fraction used as threshold
        itime : int
            time index to be considered
        area : astropy.units.Quantity
            area of the deposit
        units : astropy.units.Quantity
            physical units for the concentration
        species: None or string
            desired species for calculation. If None, all are used.
        repo_width: int
            width of repository in cells
        include_stable: bool
            include stable species in calculation; should be true since we want to
            also consider nuclides that have been transported and decayed afterwards.

        Returns
        -------
        Type : list of str
            a list describing the transport lengths returned
        tlm : astropy.units.Quantity
            the mass-related transport length to the left
        trm : astropy.units.Quantity
            the mass-related transport length to the right
        tla : astropy.units.Quantity
            the amount-related transport length to the left
        tra : astropy.units.Quantity
            the amount-related transport length to the right

        """
        tlm = self.find_transport_length(repo_index, total_mass, from_side="left", threshold=threshold, itime=itime,
                                         quantity="mass density", area=area, species=species, repo_width=repo_width,
                                         include_stable=include_stable)
        trm = self.find_transport_length(repo_index, total_mass, from_side="right", threshold=threshold, itime=itime,
                                         quantity="mass density", area=area, species=species, repo_width=repo_width,
                                         include_stable=include_stable)
        tla = self.find_transport_length(repo_index, total_amount, from_side="left", threshold=threshold, itime=itime,
                                         quantity="molar density", area=area, species=species, repo_width=repo_width,
                                         include_stable=include_stable)
        tra = self.find_transport_length(repo_index, total_amount, from_side="right", threshold=threshold, itime=itime,
                                         quantity="molar density", area=area, species=species, repo_width=repo_width,
                                         include_stable=include_stable)
        return ["left, mass density", "right, mass density", "left, molar density",
                "right, molar density"], tlm, trm, tla, tra

    def find_maximum_transport_length(self, repo_index, total_mass=None, total_amount=None, quantity="both",
                                      threshold=1e-4, itime=-1, area=None, units=None, species=None, repo_width=1,
                                      return_info=False, include_stable=True):
        """
        calculate the maximum transport length among the 2x2 possible lengths: two for transport into +x and
         -x direction, and two for mass/number density.

        Parameters
        ----------
        total_mass : astropy.units.Quantity
            the total mass in the model
        total_amount : astropy.units.Quantity
            the total amount of substance in the model
        repo_index : int
            left edge of the repository as cell index
        threshold : astropy.units.Quantity
            the mass fraction used as threshold
        itime : int
            time index to be considered
        quantity : str
            "mass density", "molar density", or "both". Both will return the maximum of the two.
        area : astropy.units.Quantity
            area of the deposit
        units : astropy.units.Quantity
            physical units for the concentration
        species: None or string
            desired species for calculation. If None, all are used.
        repo_width: int
            width of repository in cells
        include_stable: bool
            include stable species in calculation; should be true since we want to also consider nuclides that have been
             transported and decayed afterwards.
        Returns
        -------
        threshold_distance : astropy.units.Quantity
            the transport length
        """
        if quantity not in ["mass density", "molar density", "both"]:
            msg = "quantity must be mass/molar density or both"
            raise NameError(msg)
        if (quantity == "both"):
            r1 = self.find_maximum_transport_length(repo_index, threshold=threshold, itime=itime,
                                                    quantity="mass density", area=area, units=units, species=species,
                                                    repo_width=repo_width, total_mass=total_mass, return_info=True,
                                                    include_stable=include_stable)
            r2 = self.find_maximum_transport_length(repo_index, threshold=threshold, itime=itime,
                                                    quantity="molar density", area=area, units=units, species=species,
                                                    repo_width=repo_width, total_amount=total_amount, return_info=True,
                                                    include_stable=include_stable)
            if r1[0] > r2[0]:
                result = r1
            else:
                result = r2
        else:
            if quantity == "mass density":
                total = total_mass
            else:
                total = total_amount
            tl = self.find_transport_length(repo_index, total, from_side="left", threshold=threshold, itime=itime,
                                            quantity=quantity, area=area, units=units, species=species,
                                            repo_width=repo_width, include_stable=include_stable)
            tr = self.find_transport_length(repo_index, total, from_side="right", threshold=threshold, itime=itime,
                                            quantity=quantity, area=area, units=units, species=species,
                                            repo_width=repo_width, include_stable=include_stable)
            if tl > tr:
                result = [tl, "left", quantity]
            else:
                result = [tr, "right", quantity]
        if return_info:
            return result

        return result[0]

    def find_transport_length(self, repo_index, total_content, from_side="right", threshold=1e-4, itime=-1,
                              quantity="mass density", area=None, units=None, return_arrays=False, species=None,
                              repo_width=1, include_stable=True):
        """
        Find the typical transport length of the overall concentration.

        Parameters
        ----------
        total_content : astropy.units.Quantity
            the total mass/mole content of the model
        repo_index : int
            left edge of the repository as cell index
        from_side : str
            "left", "right", or "both". "both" will return the maximum of the other two.
        threshold : astropy.units.Quantity
            the mass fraction used as threshold
        itime : int
            time index to be considered
        quantity : str
            "mass density" or "molar density"
        area : astropy.units.Quantity
            area of the deposit
        units : astropy.units.Quantity
            physical units for the concentration
        return_arrays : bool
            whether or not to return additional debug information
        species: None or string
            desired species for calculation. If None, all are used.
        repo_width: int
            width of repository in cells
        include_stable: bool
            include stable species in calculation; should be true since we want to also consider nuclides that have been
             transported and decayed afterwards.

        Returns
        -------
        threshold_distance : astropy.units.Quantity
            transport length
        """
        if from_side not in ["left", "right"]:
            raise NotImplementedError()
        if from_side == "right":
            order = -1
        elif from_side == "left":
            order = 1

        dxs = self.dxs * u.m
        if species is None:
            x, c = self.get_summed_concentrations_x(itime=itime, quantity=quantity, units=units, per_rock_volume=True,
                                                    ignore_stable=not include_stable)
        else:
            if include_stable:
                msg = "can't use include_stable with single species"
                raise NotImplementedError(msg)
            x, c = self.get_concentrations_x(ispecies=species, itime=itime, quantity=quantity, units=units,
                                             per_rock_volume=True)
        csum = np.cumsum(c[::order] * dxs[::order])
        mass_fraction = (csum * area.to(u.m ** 2) / total_content).to(u.dimensionless_unscaled)
        actual_offset = repo_index
        if from_side == "right":
            actual_offset += repo_width - 1
        if from_side == "right":
            threshold_distance = np.interp(threshold, mass_fraction, x[::order]) - (
                    x[actual_offset] + dxs[actual_offset] / 2.)
        else:
            threshold_distance = (x[actual_offset] - dxs[actual_offset] / 2.) - np.interp(threshold, mass_fraction,
                                                                                          x[::order])
        if return_arrays:
            return x[::order], mass_fraction, threshold_distance

        return threshold_distance

    def calculate_total_flux(self, include_stable=False, return_final_flux_only=True, quantity="mass density"):
        """
        calculate the total flux (summed over species) per node and time step

        Parameters
        ----------
        include_stable : bool
            if True, include the stable species in the calculation
        return_final_flux_only : bool
            if True, only return the flux in the last timestep
        quantity : str
            "molar density" or "mass density"

        Returns
        -------
        jsum : astropy.Quantity
            the total flux

        """
        j = self.calculate_flux(include_stable=include_stable, return_final_flux_only=return_final_flux_only,
                                quantity=quantity)
        return j.sum(axis=0)

    def calculate_cumulative_loss_from_flux(self, quantity="mass density", include_stable=False,
                                            return_final_flux_only=True):
        """
        calculate the cumulative loss summed over species per node and time step

        Parameters
        ----------
        quantity : str
            "molar density" or "mass density"
        include_stable : bool
            if True, include the stable species in the calculation
        return_final_flux_only : bool
            if True, only return the flux in the last timestep

        Returns
        -------
        total : astropy.Quantity
            the total loss

        """
        j = self.calculate_total_flux(quantity=quantity, include_stable=include_stable, return_final_flux_only=False)
        t = self.results["t"].to(u.s).value
        dts = np.diff(t) * u.s
        total = np.cumsum(j * dts[:, None], axis=0) * self.geometry.area_cell
        if return_final_flux_only:
            return total[-1]

        return total

    def calculate_flux(self, include_stable=False, return_final_flux_only=True,
                       quantity="mass density"):
        """"
        calculate the flux that passes each gridcell midpoint over time for all species in the model

        Parameters
        ----------
        include_stable : bool, default=False
            whether to include stable species in the calculation of the cumulative flux or not
        return_final_flux_only: bool, default=True
            only return the cumulative flux at the last timestep
        quantity : str
            either mass density or molar density


        Returns
        -------
        j : array-like
            the flux at each cell midpoint for all species and all timesteps. the array has shape (nspecies, nt-1, nx-1)
            if return_final_flux_only=True this returns the cumulative flux at the last model timestep.
        """

        y = self.results["c_fluid"] * u.mol / u.m ** 3
        yoverR = y / self.R.T[:, None, :]
        ydiff = np.diff(yoverR, axis=2)
        dxs = np.diff(self.x) * u.m
        D_mid = self.De_mid.T[:, None, 1:-1] * u.m ** 2 / u.s
        jd = - D_mid * ydiff / dxs
        adv = self.q.T[:, None, :] * yoverR * u.m / u.s
        j = jd + (adv[:, :, 1:] + adv[:, :, :-1]) / 2.0

        j = j[:, :-1, :]

        if not include_stable and "stable" in self.species:
            istable = self.species.index("stable")
            j[istable] = 0.0
        if quantity not in ["mass density", "molar density", "dose density", "activitiy density"]:
            raise NotImplementedError
        if quantity == "mass density":
            j *= self.mass_conversion_factor[:, None, None]
        elif quantity == "activity density":
            j *= self.activity_conversion_factor[:, None, None]
        elif quantity == "dose density":
            j *= self.dose_conversion_factor[:, None, None]

        if return_final_flux_only:
            return j[:, -1:, ]

        return j

    def find_transport_length_with_flux(self, repo_index, total, from_side="right", threshold=0.5e-4,
                                        quantity="mass density", repo_width=1):

        """
        new flux-based calculation of transport length, i.e., the distance at which the total mass flux exceeds a
        threshold

        Parameters
        ----------
        repo_index : int
            the left-most cell index hosting the deposit
        total : astropy.units.Quantity
            the total mass or amount of substance in the model
        from_side : str
            "left", "right", or "both". "both" will return the maximum of the other two.
        threshold : astropy.units.Quantity
            the mass fraction used as threshold
        quantity : str
            "mass density" or "molar density"
        repo_width: int
            width of the deposit in cells

        Returns
        -------
        threshold_distance : astropy.units.Quantity
            transport length

        """
        if from_side not in ["left", "right"]:
            raise NotImplementedError()
        if quantity not in ["mass density", "molar density"]:
            raise NotImplementedError()

        f = self.calculate_cumulative_loss_from_flux(quantity=quantity)

        # take absolute fluxes
        abs_f = np.abs(f)

        # determine the total value for cumulative flux (kg/m2)
        if quantity == "mass density":
            c_threshold = total.to(u.kg) * threshold
        else:
            c_threshold = total.to(u.mol) * threshold

        if c_threshold.unit != abs_f.unit:
            msg = "error, units of totalMass and calculated fluxes are not compatible"
            raise ValueError(msg)

        # find points left and right of the repository where mass flux crosses threshold
        xmid = (self.x[1:] + self.x[:-1]) / 2.0
        # for the total threshold, we interpolate
        xt_left = np.interp([c_threshold.value], abs_f[:repo_index].value,
                            xmid[:repo_index])[0] * u.m
        xt_right = np.interp([c_threshold.value], abs_f[repo_index + repo_width:][::-1].value,
                             xmid[repo_index + repo_width:][::-1])[0] * u.m

        deposit_loc_left = self.x[repo_index] * u.m - self.dxs[repo_index] / 2. * u.m
        deposit_loc_right = self.x[repo_index + repo_width - 1] * u.m + self.dxs[repo_index + repo_width - 1] / 2. * u.m

        if from_side == "left":
            return deposit_loc_left - xt_left
        if from_side == "right":
            return xt_right - deposit_loc_right
        return None

    def find_max_yearly_rates(self, quantity="mass density"):
        """
        find the maximum yearly rates (over time) at each grid midpoint
        Parameters
        ----------
        quantity : str
            either "mass density" or "molar density"

        Returns
        -------
        max_rate : astropy.Quantity.Quantiy
            the maximum rate at each grid midpoint

        """
        if quantity == "mass density":
            unit = u.kg / u.yr
        else:
            unit = u.mol / u.yr

        if hasattr(self, "fluxes"):
            if quantity == "mass density":
                j = self.mass_fluxes
            else:
                j = self.fluxes
        else:
            logging.info("calculating yearly rates from snapshots")
            j = self.calculate_total_flux(quantity=quantity, return_final_flux_only=False)

        rate = (j * self.geometry.area_cell).to(unit)
        return np.max(abs(rate), axis=0)

    def find_negative_values(self, species=None, threshold=-1e-20 * u.mol / u.m ** 3):
        """
        find any negative values in the concentration curves, and return an array with the species and the time index
        at which it becomes zero.

        Parameters
        ----------
        species : string or int
            name or index of species
        threshold : astropy.units.Quantity
            threshold value to compare the concentrations with

        Returns
        -------
        res : dict
            dict storing the nuclides whose concentration became negative at which time indizes
        """
        if species is None:
            species = self.species
        res = {}
        for nuclid in species:
            if nuclid not in self.species or nuclid == "stable":
                continue
            for itime in range(self.N):
                _, c = self.get_concentrations_x(nuclid, itime=itime)
                if np.any(c < threshold):
                    if nuclid in res:
                        res[nuclid].append(itime)
                    else:
                        res[nuclid] = [itime]
        return res

    def setup_decays(self):
        """
        create auxiliary structures to speed up calculations when including decays

        Parameters
        ----------
        include_decay : bool
            whether or not to include decays in the calculations

        Returns
        -------

        """
        from transpyrend.utils.nuclides import get_daughters

        lambda_plus = np.zeros([self.nspecies, self.nspecies])
        lambda_minus = np.zeros(self.nspecies)
        decays_to_stable_idx = []
        if self.options.include_decay:
            for i, species in enumerate(self.species):
                if species == "stable":
                    daughters = {}
                else:
                    daughters = get_daughters(species, self.options.nuclid_database)
                lambda_minus[i] = sum([daughters[key] for key in daughters])
                for daughter in daughters:
                    if daughter in self.species:
                        j = self.species.index(daughter)
                        if daughter == "stable" and "stable" in self.species:
                            decays_to_stable_idx.append(i)
                        lambda_plus[i, j] = daughters[daughter]  # *self.R[j]
        # a matrix A, where A_ij gives the decay constant for the decay from species i to species j
        self.lambda_plus = lambda_plus
        # a vector B, where Bi gives the total decay constant for species i
        self.lambda_minus = lambda_minus
        self.decays_to_stable_idx = decays_to_stable_idx

    def setup_parameters(self):
        """
        setup the physical parameters of the model.
        """
        phi = np.zeros([self.N, self.nspecies])
        De = np.zeros([self.N, self.nspecies]) * u.m ** 2 / u.s
        Kd = np.zeros([self.N, self.nspecies]) * u.m ** 3 / u.kg
        K = np.zeros(self.N) * u.m / u.s
        dhdx = np.zeros(self.N)
        bulk_density = np.zeros(self.N) * u.kg / u.m ** 3

        # set properties of the "stable" species that is there to keep track of total mass in the system
        for p in self.parameters_transport_material:
            if "stable" in p:
                p["stable"]["effective_diffusion_coefficient"] = 0.0 * u.m ** 2 / u.s

        nunits = len(self.parameters_rock)
        # first check if the thickness specification exists in each layer,
        # and whether the thicknesses sum up to self.xmax
        thicks = np.zeros(nunits) * u.m
        for i in range(nunits):
            thicks[i] = (self.parameters_rock[i]["thickness"])
        if abs(thicks.sum() - self.xmax) > 1e-4 * u.m:
            msg = (
                f"thicknesses of layers = {thicks.sum()} but needs to sum to the total thickness "
                f"of the model {self.xmax}"
            )
            raise NotImplementedError(msg)

        thicks_cells = self._cells_in_layers(thicks)
        startindex = 0
        for i in range(nunits):
            endindex = startindex + thicks_cells[i]
            phi, De, K, Kd, bulk_density, dhdx = self._setup_parameters(phi, De, K, Kd, bulk_density, dhdx, i, startindex,
                                                             endindex)
            startindex = endindex

        if bulk_density.size == 1:
            R = 1. + (Kd * bulk_density / phi).to(u.dimensionless_unscaled).value
        else:
            R = 1. + (Kd * bulk_density[:, np.newaxis] / phi).to(u.dimensionless_unscaled).value
        D = De / phi
        v = (-K[:, np.newaxis] * dhdx[:, np.newaxis] / phi).to(u.m / u.s).value

        # get the diffusion coefficient at the midpoints.
        D = D.to(u.m ** 2 / u.s).value

        # store v, D, and R as attributes.
        self.v = v
        self.D = D
        self.R = R

        # calculate midpoint value for De
        # note that this is an array with all midpoints plus one point at the left hand side and one point at the right
        # hand side.
        self.De_mid = self.average_D(De)

        self.q = v * phi
        self.phi = phi
        self.De = De


    def _setup_parameters(self, phi, De, K, Kd, bulk_density, dhdx, i, startindex, endindex):
        """
        helper function to set the material parameters of a certain layer

        Parameters
        ----------
        phi : array-like
            porosity of all cells
        De : astropy.units.Quantity
            effective diffusion coefficient porosity of all cells
        K : astropy.units.Quantity
            hydraulic permeability porosity of all cells
        Kd : astropy.units.Quantity
            sorption coefficient porosity of all cells
        bulk_density : astropy.units.Quantity
            bulk density porosity of all cells
        dhdx : array-like
            hydraulic gradient porosity of all cells
        i : int
            index of the layer
        startindex :int
            startindex of the cells that belong to this layer
        endindex : int
            endindex of the cells that belong to this layer

        Returns
        -------
        phi : array-like
            porosity of all cells
        De : astropy.units.Quantity
            effective diffusion coefficient porosity of all cells
        K : astropy.units.Quantity
            hydraulic permeability porosity of all cells
        Kd : astropy.units.Quantity
            sorption coefficient porosity of all cells
        bulk_density : astropy.units.Quantity
            bulk density porosity of all cells
        dhdx : array-like
            hydraulic gradient porosity of all cells
        """
        dhdx[startindex:endindex] = self.parameters_rock[i]["head_gradient"]
        phi[startindex:endindex] = self.parameters_rock[i]["porosity"]
        K[startindex:endindex] = self.parameters_rock[i]["hydraulic_conductivity"]
        for j in range(self.nspecies):
            my_params = self.parameters_transport_material[i]
            if self.species[j] not in my_params:
                msg = f"could not find effective_diffusion_coefficient for unit {i}, specie {self.species[j]}"
                raise KeyError(msg)
            De[startindex:endindex, j] = my_params[self.species[j]]["effective_diffusion_coefficient"]

            if "accessible_porosity" in my_params[self.species[j]]:
                phi[startindex:endindex, j] = my_params[self.species[j]]["accessible_porosity"]
            if self.options.include_sorption and "sorption_coefficient" in my_params[self.species[j]]:
                Kd[startindex:endindex, j] = my_params[self.species[j]]["sorption_coefficient"]
            if np.any(Kd[:, j].value > 0.0):
                bulk_density[startindex:endindex] = self.parameters_rock[i]["bulk_density"]
        return phi, De, K, Kd, bulk_density, dhdx

    def _cells_in_layers(self, thicks):
        """
        helper function to figure out which cells live in which layer

        Parameters
        ----------
        thicks : astropy.units.Quantity
            thicknesses of all layers

        Returns
        -------
        thick_cells : array-like, dtype int
            the number of cells belonging to each layer
        """
        temp = np.cumsum(thicks).to(u.m).value
        tcum = np.zeros(thicks.size + 1, dtype=int)
        tcum[1:] = temp
        thicks_cells = np.zeros_like(thicks.value, dtype=int)
        is_used = [False for i in range(self.x.size)]
        for i in range(thicks.size):
            for j in range(self.x.size):
                if tcum[i] <= self.x[j] < tcum[i + 1]:
                    if is_used[j]:
                        logging.warning(f"cell {j} used twice!")
                        raise RuntimeError
                    is_used[j] = True
                    thicks_cells[i] += 1
        # the last cell always belongs to the last layer
        if is_used[-1] is False:
            thicks_cells[-1] += 1
            is_used[-1] = True
        if np.any(np.array(is_used) is False):
            msg = "some cells were not assigned to a unit."
            raise RuntimeError(msg)
        if thicks_cells.sum() > self.N:
            msg = (
                "number of cells in all layer dont sum to the total number of cells "
                "in the model. check thicknesses of layers."
            )
            raise NotImplementedError(msg)
        return thicks_cells

    @cached_property
    def mass_conversion_factor(self):
        """
        coefficients for converting amount of substance to mass for each species
        """
        mcf = np.zeros(self.nspecies) * u.kg / u.mol
        for ispecies, species in enumerate(self.species):
            if species == "stable":
                mcf[ispecies] = 1. * u.kg / u.mol
            else:
                mcf[ispecies] = mol2mass(1. * u.mol / u.m ** 3, species, units=u.kg / u.m ** 3).value * u.kg / u.mol
        return mcf

    @cached_property
    def activity_conversion_factor(self):
        """
        coefficients for converting amount of substance to activity
        """
        acf = np.zeros(self.nspecies) * u.Bq / u.mol
        for ispecies, species in enumerate(self.species):
            acf[ispecies] = mol2activity_vsg(1. * u.mol, species,
                                                                         unit=u.Bq).value * u.Bq / u.mol
        return acf

    def check_number_conservation_species(self):
        """
        retrieve the total amount of substance per specie as a function of time, and the 'true' amount of substance from
        the solution of the single-zone decay problem.

        Returns
        -------
        myspecies: list of strings
            the species considered
        amounts: astropy.units.Quantity
            the amount of substance(i,j) per species i and time index j
        amounts_comp: astropy.units.Quantity
            the amount of substance(i,j) per species i and time index j as calculated by the package radioactivedecay
        """
        from transpyrend.utils.nuclides import evolve_to_age

        ntimes = self.results["t"].size
        nspecies = len(list(set(self.species)))
        if "stable" in self.species:
            nspecies -= 1
        amounts = np.zeros([nspecies, ntimes]) * u.mol / u.m ** 3
        myspecies = []
        for itime in range(ntimes):
            ispecies = 0
            for species in self.species:
                if species == "stable":
                    continue
                _, csum = self.get_concentrations_x(species, itime=itime, per_rock_volume=True,
                                                    quantity="molar density")
                amounts[ispecies, itime] = csum.sum()
                ispecies += 1
                if itime == 0:
                    myspecies.append(species)
        inv = {}
        for i, species in enumerate(myspecies):
            inv[species] = amounts[i, 0] * u.m ** 3
        data = evolve_to_age(inv, self.results["t"], quantity="amount")
        amounts_comp = np.zeros_like(amounts)
        for i, key in enumerate(data):
            amounts_comp[i, :] = data[key] / u.m ** 3
        return myspecies, amounts, amounts_comp

    def setup_bateman(self, chains, states):
        """
        setup the bateman solver

        Parameters
        ----------
        chains : list
            a list of list of the linear subchains to consider in the calculation
        states : list
            a list of indizes, indicating which members of subchains need to be set to zero in consecutive calculations
            to avoid double counting

        Returns
        -------

        """
        chain_indizes = [] * len(chains)
        chain_lambdas = [] * len(chains)
        for chain in chains:
            cidx = []
            lams = []
            for spec in chain:
                if spec not in self.species:
                    continue
                for i in range(len(self.species)):
                    if spec == self.species[i]:
                        cidx.append(i)
                daughters = get_daughters(spec, self.options.nuclid_database)
                keys = list(daughters.keys())
                assert len(keys) < 2
                if len(keys) == 1:
                    lams.append(daughters[keys[0]])
                else:
                    lams.append(1e-30)
            chain_indizes.append(cidx)
            chain_lambdas.append(np.array(lams))
        self.chain_indizes = chain_indizes
        self.chain_lambdas = chain_lambdas
        self.num_states = states
        self.options.use_bateman = True

    def convert_ics(self, ics):
        """
        convert the ics given to run() to a common format

        Parameters
        ----------
        ics : dict or astropy.units.Quantity
            the initial conditions
        Returns
        -------
        c : array-like
            the processed initial conditions

        """
        # set up array for initial concentrations
        c = np.zeros((self.nspecies, self.N))
        # set up initial concentrations if specified
        # c has shape (n_species, nx)
        need_conversion = False
        ic_mass_units = u.kg / u.m ** 3
        ic_amount_units = u.mol / u.m ** 3
        units = ic_amount_units
        need_conversion = self._convert_ics(c, ic_mass_units, ics, need_conversion, units)
        # we need to convert to concentrations
        if need_conversion:
            for i, species in enumerate(self.species):
                myic = c[i, :] * ic_mass_units
                myic = mass2mol(myic, species, units=ic_amount_units)
                c[i, :] = myic.value

        # divide the initial concentrations by the retention factors to obtain the concentration in the fluid phase only
        c /= self.R.T
        # divide by the porosity to retrieve the concentration per fluid volume (and not per rock volume)
        c /= self.phi.T
        return c

    def _convert_ics(self, c, ic_mass_units, ics, need_conversion, units):
        if ics is not None:
            # first, check if ics are mass density or molar density
            physical_type = {u.quantity.Quantity: lambda ic: ic.unit.physical_type,
                             list: lambda ic: ic[0].unit.physical_type,
                             dict: lambda ic: ic[list(ic.keys())[0]].unit.physical_type}
            my_physical_type = physical_type[type(ics)](ics)
            need_conversion = False
            if my_physical_type == "mass density":
                need_conversion = True
                units = ic_mass_units
            if type(ics) == u.quantity.Quantity:
                c[:, :] = ics.to(units).T.value
            elif type(ics) == list:
                c[:, :] = np.array([ic.to(units) for ic in ics])
            elif type(ics) == dict:
                for i, specie in enumerate(self.species):
                    c[i, :] = ics[specie].to(units).value
            else:
                msg = f"error, ics is type {type(ics)}, which is not supported (yet)"
                raise ValueError(msg)
        return need_conversion

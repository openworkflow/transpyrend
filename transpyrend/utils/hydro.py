"""
tools to deal with hydrogeological quantities
"""

def get_consistent_hydraulic_gradient(qref, K):
    """
    calculate the hydraulic gradient in a layer such that the water flux
    in each layer is the same, given some reference value.

    Parameters
    ----------
    qref : astropy.units.Quantity
        the reference darcy velocity
    K : astropy.units.Quantity
        the hydraulic conductivity

    Returns
    -------
    dhdx : astropy.units.Quantity
        the hydraulic gradient

    """
    return qref/K

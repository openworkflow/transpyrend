"""
tools for comparing parameter sets
"""

import logging
import os

from pandas.api.types import is_numeric_dtype
from transpyrend.parameters.model import TransPyREndParameters
from transpyrend.parameters.tables import DiffusionParametersTable, SorptionParametersTable
from transpyrend.parameters.tables import GeologicalUnitParametersTable


def compare_parameter_table(fname1, fname2, table_type, primary_index,
                            secondary_index, name, rtol=1e-4, atol=1e-6):
    """
    compares a BaseParameter-style table with another BaseParameter-style table. Logs differences.

    Parameters
    ----------
    fname1 : str
        filename of first table
    fname2 : str
        filename of first table
    table_type : BaseTable or derived class
        the table type to be considered
    primary_index : str
        the primary column indexing the table
    secondary_index : str
        the secondary column indexing the table; can be None
    name : str
        the name to display for the table in the logger
    rtol : float
        relative tolerance for numeric differences between the tables
    atol : float
        abolute tolerance for numeric differences between the tables

    Returns
    -------
    are_identical : bool
        true if content of files are equal, false otherwise

    """
    table1 = table_type(fname=fname1)
    table2 = table_type(fname=fname2)
    if table1.units != table2.units:
        cols1 = list(table1.units.keys())
        cols2 = list(table2.units.keys())
        if len(cols1) != len(cols2):  #
            # this is the trivial case in which the units differ because the columns do.
            # this will be caught below anyway, so dont warn here.
            pass
        else:
            logging.warning(f"[{name}] units differ between table 1 and table 2!")
            for col in table1.units:
                if table1.units[col] != table2.units[col]:
                    logging.warning(f"for column {col}: table1 {table1.units[col]}, table2 {table2.units[col]}")

    return compare_table(table1.df, table2.df, primary_index, secondary_index, rtol=rtol, atol=atol, name=name)


def compare_diffusion_parameter_table(fname1, fname2, rtol=1e-4, atol=1e-6):
    """
    compare two DiffusionParametersTable objects read from disk.

    Parameters
    ----------
    fname1 : str
        filename of first table
    fname2 : str
        filename of first table
    rtol : float
        relative tolerance for numeric differences between the tables
    atol : float
        abolute tolerance for numeric differences between the tables

    Returns
    -------
    are_identical : bool
        true if content of files are equal, false otherwise

    """
    return compare_parameter_table(fname1, fname2, DiffusionParametersTable, "nuclide", "nuclide_param_key",
                                   "diffusion_parameters", rtol, atol)


def compare_sorption_parameter_table(fname1, fname2, rtol=1e-4, atol=1e-6):
    """
    compare two SorptionParametersTable objects read from disk.

    Parameters
    ----------
    fname1 : str
        filename of first table
    fname2 : str
        filename of first table
    rtol : float
        relative tolerance for numeric differences between the tables
    atol : float
        abolute tolerance for numeric differences between the tables

    Returns
    -------
    are_identical : bool
        true if content of files are equal, false otherwise

    """
    return compare_parameter_table(fname1, fname2, SorptionParametersTable, "nuclide", "nuclide_param_key",
                                   "sorptions_parameters", rtol, atol)


def compare_geo_parameter_table(fname1, fname2, rtol=1e-4, atol=1e-6):
    """
        compare two GeologicalUnitParametersTable objects read from disk.

        Parameters
        ----------
        fname1 : str
            filename of first table
        fname2 : str
            filename of first table
        rtol : float
            relative tolerance for numeric differences between the tables
        atol : float
            abolute tolerance for numeric differences between the tables

        Returns
        -------
        are_identical : bool
            true if content of files are equal, false otherwise

    """

    return compare_parameter_table(fname1, fname2, GeologicalUnitParametersTable, "geological_unit_code",
                                   None, "geological_layers", rtol, atol)


def compare_table(table1, table2, primary_index, secondary_index=None, rtol=1e-4, atol=1e-6, name=""):
    """
        compares two DataFrames. Logs differences.

        Parameters
        ----------
        table1 : pandas.DataFrame
            first DataFrame
        table2 : pandas.DataFrame
            second DataFrame
        primary_index : str
            the primary column indexing the table
        secondary_index : str
            the secondary column indexing the table; can be None
        rtol : float
            relative tolerance for numeric differences between the tables
        atol : float
            abolute tolerance for numeric differences between the tables
        name : str
            the name to display for the table in the logger

        Returns
        -------
        are_identical : bool
            true if content of files are equal, false otherwise

        """

    are_identical = True

    # compare columns
    for col in table1.columns:
        if col not in table2.columns:
            logging.warning(f"[{name}] column {col} in table 1, but not in table 2!")
            are_identical = False
    for col in table2.columns:
        if col not in table1.columns:
            logging.warning(f"[{name}] column {col} in table 2, but not in table 1!")
            are_identical = False

    # compare index occurences
    index_values_1 = set(table1[primary_index].values.tolist())
    index_values_2 = set(table1[primary_index].values.tolist())
    for v in index_values_1:
        if v not in index_values_2:
            logging.warning(f"[{name}] entry {v} in table 1, but not in table 2!")
            are_identical = False
    for v in index_values_2:
        if v not in index_values_1:
            logging.warning(f"[{name}] entry {v} in table 2, but not in table 1!")
            are_identical = False
    # if index is not unique, see if the entries have the same order
    if index_values_1 != index_values_2:
        logging.warning("[{name}] order of rows is not the same in table 1 and table 2")
        are_identical = False

    # compare contents:
    if secondary_index is not None:
        s_index_values_1 = set(table1[secondary_index].values.tolist())

        for pindex in index_values_1:
            for sindex in s_index_values_1:
                mask1 = (table1[primary_index] == pindex) & (table1[secondary_index] == sindex)
                mask2 = (table2[primary_index] == pindex) & (table2[secondary_index] == sindex)
                are_identical = _compare_row(are_identical, atol, mask1, mask2, name, pindex, primary_index, rtol,
                                             table1,
                                             table2)
    else:
        for pindex in index_values_1:
            mask1 = (table1[primary_index] == pindex)
            mask2 = (table2[primary_index] == pindex)
            are_identical = _compare_row(are_identical, atol, mask1, mask2, name, pindex, primary_index, rtol, table1,
                                         table2)
    return are_identical


def _compare_row(are_identical, atol, mask1, mask2, name, pindex, primary_index, rtol, table1, table2):
    row1 = table1[mask1]
    row2 = table2[mask2]
    for col in row1.columns:
        if row1[col].values.size == 0 and row2[col].values.size == 0:
            continue
        if row2[col].values.size == 0:
            logging.warning(f"[{name}] missing entry {primary_index}={pindex} in table 2")
            are_identical = False
            continue
        if row1[col].values.size == 0:
            logging.warning(f"[{name}] missing entry {primary_index}={pindex} in table 1")
            are_identical = False
            continue
        are_identical = _compare_values(are_identical, atol, col, name, pindex, primary_index, row1, row2,
                                        rtol, None, 0)
    return are_identical


def _compare_values(are_identical, atol, col, name, pindex, primary_index, row1, row2, rtol, secondary_index, sindex):
    if is_numeric_dtype(row1[col]):
        ratio = row1[col].values / row2[col].values - 1.
        diff = abs(row1[col].values - row2[col].values)
        if ratio > rtol or diff > atol:
            logging.warning(
                f"in [{name}]  {primary_index}={pindex}, {secondary_index}={sindex}, {col}: \n\
                table1 {row1[col].values} table2 {row2[col].values}")
            are_identical = False
    else:
        for v1, v2 in zip(row1[col].values, row2[col].values):
            if v1 != v2:
                logging.warning(
                    f"in [{name}] {primary_index}={pindex}, {secondary_index}={sindex}, {col}:\n\
                    table1 {row1[col].values} table2 {row2[col].values}")
                are_identical = False
    return are_identical


def compare_yaml_parameters(fname1, fname2):
    """
    compare the content of two TransPyREndParameters object read from disk.

    Parameters
    ----------
    fname1 : str
        filename of first yaml file
    fname2 : str
        filename of second yaml file

   Returns
    -------
    are_identical : bool
        true if content of files are equal, false otherwise

    """
    are_identical = True
    t1 = TransPyREndParameters(filename=fname1, read_shallow=True)
    t2 = TransPyREndParameters(filename=fname2, read_shallow=True)
    for key in TransPyREndParameters._allowed_args:
        if key in TransPyREndParameters._arg_tables:
            continue
        v1 = getattr(t1, key)
        v2 = getattr(t2, key)
        if v1 != v2:
            logging.warning(f"[yaml] {key}: file1 {v1} file2 {v2}")
            are_identical = False
    return are_identical


def get_param_filenames(fname):
    """
    return the pathes to the parameter tables as indicated in a TransPyREndParameters objekt read frorm disk

    Parameters
    ----------
    fname : str
        filename of yaml file

    Returns
    -------
    files : list of str
        list of the parameter files names

    """
    t = TransPyREndParameters(filename=fname, read_shallow=True)
    return t.files


def diff_params(root1, root2, rtol=1e-4, atol=1e-7):
    """
    compares two Transpyrend parameter sets as provided by a TransPyREndParameters object.

    Parameters
    ----------
    root1 : str
        either root directory in which a model_parameters.yaml resides, or the direct path to such a file
    root2 : str
        either root directory in which a model_parameters.yaml resides, or the direct path to such a file

    Returns
    -------
    is_identical : bool
        True if contents are identical
    """
    fyaml1 = root1
    fyaml2 = root2
    if os.path.isdir(root1):
        assert (os.path.isdir(root2))
        fyaml1 = os.path.join(root1, "model_parameters.yaml")
        fyaml2 = os.path.join(root2, "model_parameters.yaml")

    fparams1 = get_param_filenames(fyaml1)
    fparams2 = get_param_filenames(fyaml2)

    is_identical = compare_yaml_parameters(fyaml1, fyaml2)

    res = compare_diffusion_parameter_table(fparams1["diffusion_parameters"],
                                            fparams2["diffusion_parameters"], rtol=rtol, atol=atol)
    is_identical = is_identical and res

    res = compare_sorption_parameter_table(fparams1["sorption_parameters"],
                                           fparams2["sorption_parameters"],
                                           rtol=rtol, atol=atol)
    is_identical = is_identical and res

    res = compare_geo_parameter_table(fparams1["geological_layers"],
                                      fparams2["geological_layers"], rtol=rtol, atol=atol)
    return is_identical and res

"""
contains tools to deal with radionuclides
"""
import logging
import os

import numpy as np
import pandas as pd
import radioactivedecay as rd
from astropy import constants
from astropy import units as u
# define additional units
from transpyrend.utils.nudata import Nudata2020Wrapper

_BASE_PATH = os.path.dirname(os.path.abspath(__file__))
_DATA_PATH = os.path.join(_BASE_PATH, "../data")

# note: if this fails, the cause is likely incompatibility of the code with newer versions of radioactivedecay
try:
    VSGDECAYDATA = rd.decaydata.load_dataset("vsg", os.path.join(_DATA_PATH, "vsg_rd_decaydata"),
                                         load_sympy=True)
except Exception as e:
    logging.warning(f"{e}: Could not load VSGDECAYDATA")


"""the decaydata object used in radioactivedecay"""

_cache_nu = Nudata2020Wrapper()

vsg_tab411_data = {"Be-10": {"stable": 1.6e6 * u.yr},
                   "C-14": {"stable": 5730. * u.yr},
                   "Cl-36": {"stable": 3e5 * u.yr},
                   "Ca-41": {"stable": 1.03e5 * u.yr},
                   "Ni-59": {"stable": 7.5e4 * u.yr},
                   "Ni-63": {"stable": 100. * u.yr},
                   "Se-79": {"stable": 4.8e5 * u.yr},
                   "Rb-87": {"stable": 4.8e10 * u.yr},
                   "Sr-90": {"stable": 28.64 * u.yr},
                   "Zr-93": {"stable": 1.5e6 * u.yr},
                   "Nb-94": {"stable": 2.e4 * u.yr},
                   "Mo-93": {"stable": 3.5e3 * u.yr},
                   "Tc-99": {"stable": 2.1e5 * u.yr},
                   "Pd-107": {"stable": 6.5e6 * u.yr},
                   "Ag-108m": {"stable": 418. * u.yr},
                   "Sn-126": {"stable": 2.35e5 * u.yr},
                   "I-129": {"stable": 1.57e7 * u.yr},
                   "Cs-135": {"stable": 2e6 * u.yr},
                   "Cs-137": {"stable": 30.17 * u.yr},
                   "Sm-147": {"stable": 1.06e11 * u.yr},
                   "Sm-151": {"stable": 93. * u.yr},
                   "Ho-166m": {"stable": 1200. * u.yr},
                   # Thorium chain
                   "Cm-248": {"Pu-244": 3.4e5 * u.yr},
                   "Pu-244": {"Pu-240": 8e7 * u.yr},
                   "Pu-240": {"U-236": 6563. * u.yr},
                   "U-236": {"Th-232": 2.34e7 * u.yr},
                   "Th-232": {"stable": 1.41e10 * u.yr},
                   "U-232": {"stable": 68.9 * u.yr},
                   # Neptunium chain
                   "Cm-245": {"Am-241": 8500. * u.yr},
                   "Am-241": {"Np-237": 432.2 * u.yr},
                   "Np-237": {"U-233": 2.14e6 * u.yr},
                   "U-233": {"Th-229": 1.59e5 * u.yr},
                   "Th-229": {"stable": 7880. * u.yr},
                   # Uranium chain
                   "Cm-246": {"Pu-242": 4730. * u.yr},
                   "Pu-242": {"U-238": 3.75e5 * u.yr},
                   "U-238": {"U-234": 4.47e9 * u.yr},
                   "Am-242m": {"Pu-238": 141. * u.yr},
                   "Pu-238": {"U-234": 87.74 * u.yr},
                   "U-234": {"Th-230": 2.46e5 * u.yr},
                   "Th-230": {"Ra-226": 7.54e4 * u.yr},
                   "Ra-226": {"stable": 1600. * u.yr},
                   # Actinium chain
                   "Cm-247": {"Am-243": 1.56e7 * u.yr},
                   "Am-243": {"Pu-239": 7370. * u.yr},
                   "Pu-239": {"U-235": 2.41e4 * u.yr},
                   "U-235": {"Pa-231": 7.04e8 * u.yr},
                   "Pa-231": {"Ac-227": 3.28e4 * u.yr},
                   "Ac-227": {"stable": 21.773 * u.yr}
                   }
"""contains the nuclid chain data from table 4.11 in VSG-289. The numbers are the half lifes"""

vsg_halflifes = {}
"""a simplified table with half lifes of all VSG nuclides."""
for nuclid in vsg_tab411_data:
    _daughter = list(vsg_tab411_data[nuclid].keys())[0]
    vsg_halflifes[nuclid] = vsg_tab411_data[nuclid][_daughter]

vsg_tab411_activation_products = ["Be-10", "C-14", "Cl-36", "Ca-41", "Ni-59", "Ni-63", "Se-79", "Rb-87", "Sr-90",
                                  "Zr-93", "Nb-94", "Mo-93", "Tc-99", "Pd-107", "Ag-108m", "Sn-126", "I-129", "Cs-135",
                                  "Cs-137", "Sm-147", "Sm-151", "Ho-166m"]
"""
list of the activation and decay products in VSG
"""
_lifetimes = []
# sort the activation products by their half lifes
for nuclide in vsg_tab411_activation_products:
    _daughters = vsg_tab411_data[nuclide]
    _lifetimes.append(_daughters[list(_daughters.keys())[0]].to(u.yr).value)
_order = np.argsort(_lifetimes)
vsg_tab411_activation_products = list(np.array(vsg_tab411_activation_products)[_order])

vsg_tab411_thorium_chain = ["Cm-248", "Pu-244", "Pu-240", "U-236", "Th-232", "U-232"]
"""
list of the names of the chain members
"""
vsg_tab411_neptunium_chain = ["Cm-245", "Am-241", "Np-237", "U-233", "Th-229"]
"""
list of the names of the chain members
"""
vsg_tab411_uranium_chain = ["Cm-246", "Pu-242", "U-238", "Am-242m", "Pu-238", "U-234", "Th-230", "Ra-226"]
"""
list of the names of the chain members
"""
vsg_tab411_actinium_chain = ["Cm-247", "Am-243", "Pu-239", "U-235", "Pa-231", "Ac-227"]
"""
list of the names of the chain members
"""
vsg_tab411_uranium_chain_bateman_setup = {"chains": [['Cm-246', 'Pu-242', 'U-238', 'U-234', 'Th-230', 'Ra-226'],
                                                     ['Am-242m', 'Pu-238', 'U-234', 'Th-230', 'Ra-226']],
                                          "states": [-1, 2]}
"""
setup for the uranium chain required for using the Bateman solution
"""

vsg_tab411_neptunium_chain_bateman_setup = {"chains": [vsg_tab411_neptunium_chain],
                                            "states": [-1]}
"""
setup for the neptunium chain required for using the Bateman solution
"""
vsg_tab411_actinium_chain_bateman_setup = {"chains": [vsg_tab411_actinium_chain],
                                           "states": [-1]}
"""
setup for the actinium chain required for using the Bateman solution
"""
vsg_tab411_thorium_chain_bateman_setup = {"chains": [['Cm-248', 'Pu-244', 'Pu-240', 'U-236', 'Th-232'], ['U-232']],
                                          "states": [-1, 1]}
"""
setup for the thorium chain required for using the Bateman solution
"""


def evolve_to_age(inventory, time_span, nuclide_scheme="VSG", quantity="mass"):
    """
    takes an inventory and evolves it to some age. Uses the radioactivedecay library internally. nuclide_scheme can be
    used to specify a simplified scheme, ignoring many of the resulting nuclides.

    Parameters
    ----------
    inventory : dict
        relates nuclide names to mass values
    time_span : astropy.units.Quantity
        the time span over which the inventory should be evolved. If this is an array, calculation for all the times are
        done and appended nuclide wise.
    nuclide_scheme: None or "VSG"
        if "VSG", use simplified nuclide scheme
    quantity: "mass" or "moles"
        the physical quantity to return

    Returns
    -------
    inv_w_units : dict
        a dict relating nuclide names to mass values after the decay

    """
    assert quantity in ["mass", "amount"]
    inv_wo_units = {}
    # remove units
    if quantity == "mass":
        unit_str = "kg"
        unit = u.kg
    else:
        unit_str = "mol"
        unit = u.mol

    for key, value in inventory.items():
        M = value.to(unit)
        inv_wo_units[key] = M.value
    if nuclide_scheme not in [None, "VSG"]:
        msg = "Unknown nuclide scheme"
        raise NameError(msg)
    if nuclide_scheme is None:
        inv = rd.Inventory(inv_wo_units, unit_str)
    elif nuclide_scheme == "VSG":
        inv = rd.Inventory(inv_wo_units, unit_str, decay_data=VSGDECAYDATA)
    was_scalar = False
    if time_span.isscalar:
        was_scalar = True
        time_span = [time_span.value] * time_span.unit
    # CAUTION: We dont sanitize negative values here!
    inv_w_units = {}
    for key in inventory:
        inv_w_units[key] = np.zeros(time_span.size)

    for i, t in enumerate(time_span):
        if quantity == "mass":
            new_inv = inv.decay(t.to(u.s).value, "s").masses(unit_str)
        else:
            new_inv = inv.decay(t.to(u.s).value, "s").moles(unit_str)
        # apply units
        for key, value in new_inv.items():
            if key not in inv_w_units:
                inv_w_units[key] = np.zeros(time_span.size)
            inv_w_units[key][i] = value
    for key in inv_w_units:
        inv_w_units[key] *= unit
        if was_scalar:
            inv_w_units[key] = inv_w_units[key][0]

    return (inv_w_units)


def evolve_to_age_VSG(inventory, time_span):
    """
    take an inventory an evolve it to some age. Uses the radioactivedecay library internally and the VSG nuclide scheme
    internally.

    Parameters
    ----------
    inventory : dict
        relates nuclide names to mass values
    time_span : astropy.units.Quantity
        the time span over which the inventory should be evolved. If this is an array, calculation for all the times are
        done and appended nuclide wise.

    Returns
    -------
    inv_w_units : dict
        a dict relating nuclide names to mass values after the decay

    """
    return evolve_to_age(inventory, time_span, nuclide_scheme="VSG")


class VSGInventoryReader:
    """
    a wrapper to deal with the data from GRS-289, namely the initial inventory with the reduced set of nuclides.
    taken from GRS-289, table A.15

    Parameters
    ----------
    path : str
        the path for the inventory data

    """

    # NOTE:
    # There is a typo in Am-243 (in the csv, it was Am-234 which does not exist) in the table of the original report.
    #
    def __init__(self, path=None):
        nu = _cache_nu
        if path is None:
            path = os.path.join(_DATA_PATH,"vsg_inventory.csv")
        activity_data = pd.read_csv(path, delimiter=";", comment="#")
        mass_data = activity_data.copy()
        activity_data["Gesamt"] = 0.0
        cols = mass_data.columns.tolist()[1:]
        for col in cols:
            activity_data["Gesamt"] += activity_data[col]

        nuclides = mass_data["Nuklid"]
        mass_data["Konvertierung zu Masse"] = 0.
        for i, nuclid in enumerate(nuclides):
            d = vsg_tab411_data[nuclid]
            keys = list(d.keys())
            lam = (np.log(2) / d[keys[0]]).to(1. / u.s).value
            mass_data["Konvertierung zu Masse"].values[i] = nu[nuclid].value / lam
        cols = mass_data.columns.tolist()[1:-1]
        for col in cols:
            mass_data[col] = activity_data[col] * mass_data["Konvertierung zu Masse"] * (constants.u).to(u.kg).value
        # create some new column with various totals.
        mass_data["Gesamt"] = 0.0
        for col in cols:
            mass_data["Gesamt"] += mass_data[col]

        cols_haw = ["DWR-/SWR-/WWER-Brennelemente", "CSD-V", "CSD-C", "CSD-B", "AVR", "THTR", "FRM II, BER II, KNK"]
        mass_data["Gesamt HAW"] = 0.0
        for col in cols_haw:
            mass_data["Gesamt HAW"] += mass_data[col]
        self.mass_data = mass_data

    def get_haw_inventory_fractions(self):
        """
        returns a dictionary with the relative masses of isotopes in HAW waste and the total mass.
        """
        nuclids = self.mass_data["Nuklid"].tolist()
        masses = self.mass_data["Gesamt HAW"].to_numpy()
        tot_mass = masses.sum()
        rel_masses = masses / tot_mass
        out = {}
        for i in range(len(nuclids)):
            out[nuclids[i]] = rel_masses[i]
        return out, (tot_mass * u.kg).to(u.Mg)

    def get_haw_inventory(self, quantity="mass"):
        """
        returns a dictionary masses of isotopes in HAW waste.
        """
        nuclides = self.mass_data["Nuklid"].tolist()
        masses = self.mass_data["Gesamt HAW"].to_numpy()
        out = {}
        for i in range(len(nuclides)):
            out[nuclides[i]] = masses[i] * u.kg
        if quantity == "amount":
            nu = Nudata2020Wrapper()
            for nuc in nuclides:
                out[nuc] = (out[nuc].to(u.g).value / nu[nuc].value) * u.mol
        return out

    def get_haw_total_mass(self):
        """
        return the total mass in the HAW inventory
        """
        nuclids = self.mass_data["Nuklid"].tolist()
        masses = self.mass_data["Gesamt HAW"].to_numpy()
        s = 0. * u.kg
        for i in range(len(nuclids)):
            s += masses[i] * u.kg
        return s

    def get_haw_total_amount(self):
        """
        return the total amout of atoms in the HAW inventory (in Mol).
        This treats all contents as atomic (no compounds, no molecules)

        Returns
        -------
        data : astropy.units.Quantity
            total amount of atoms in the HAW inventory

        """
        nu = Nudata2020Wrapper()
        nuclides = self.mass_data["Nuklid"].tolist()
        masses = self.mass_data["Gesamt HAW"].to_numpy() * u.kg
        total_mol = 0 * u.mol
        for nuc, mass in zip(nuclides, masses):
            # use the fact that g/mol = u
            total_mol += (mass.to(u.g).value / nu[nuc].value) * u.mol
        return total_mol


def get_species_VSG(by_chain=False):
    """
    returns a list of all the names of the species in VSG

    Parameters
    ----------
    by_chain :
        if true, return the names chain by chain

    Returns
    -------
    l : list
        either a list of species names, or a list of a list of names in each chain

    """
    sets = [vsg_tab411_activation_products, vsg_tab411_neptunium_chain,
            vsg_tab411_uranium_chain, vsg_tab411_thorium_chain, vsg_tab411_actinium_chain]
    if by_chain:
        return sets.copy()

    lst = []
    for s in sets:
        for nuclid in s:
            lst.append(nuclid)
    return lst


def _convert_mass_mol(quantity, species, units, direction):
    """
    convert masses to amounts of substance and vice versa

    Parameters
    ----------
    quantity: astropy.units.quantity.Quantity or dict of astropy.units.quantity.Quantity
        the mass/amount of substance to convert
    species: str or list of str
        if quantity is not a dict, specifies the names of the species in question
    units: astropy.units.quantity.Quantity.Unit
        desired output unit
    direction: "to mol" or "to mass"
        desired direction of conversion

    Returns
    -------
    data : dict or astropy.units.quantity.Quantity
        the converted data

    """
    import astropy.constants as constants

    isscalar = False
    if isinstance(quantity, dict):
        data = quantity
    else:
        if isinstance(species, str):
            species = [species]
            quantity = np.array(quantity) * quantity.unit
            isscalar = True
        data = {}
        if isscalar:
            data[species[0]] = quantity
        else:
            for i in range(len(species)):
                data[species[i]] = quantity[i]
    data_out = {}
    for key, q in data.items():
        if key == "stable":
            data_out[key] = q.value * units
            continue
        atomic_mass = _cache_nu[key]
        if direction == "to mol":
            q2 = ((q / atomic_mass) / constants.N_A).to(units)
        elif direction == "to mass":
            q2 = (q * constants.N_A * atomic_mass).to(units, equivalencies=u.molar_mass_amu())
        else:
            raise RuntimeError()
        data_out[key] = q2

    if isinstance(quantity, dict):
        return data_out

    if isscalar:
        return data_out[species[0]]

    return [data_out[key] for key in species]


def mass2mol(masses, species=None, units=u.mol):
    """
    convert masses to amounts of substance

    Parameters
    ----------
    species: str or list of str
        if quantity is not a dict, specifies the names of the species in question
    units: astropy.units.quantity.Quantity.Unit
        desired output unit

    Returns
    -------
    data : dict or astropy.units.quantity.Quantity
        the converted data

    """
    return _convert_mass_mol(masses, species, units, direction="to mol")


def mol2mass(masses, species=None, units=u.kg):
    """
    convert amounts of substance to masses

    Parameters
    ----------
    species: str or list of str
        if quantity is not a dict, specifies the names of the species in question
    units: astropy.units.quantity.Quantity.Unit
        desired output unit

    Returns
    -------
    data : dict or astropy.units.quantity.Quantity
        the converted data

    """
    return _convert_mass_mol(masses, species, units, direction="to mass")


def mol2activity_vsg(mols, species=None, unit=u.Bq):
    """
    convert amount of substance to activity, specifically for the decay data provided in VSG.

    Parameters
    ----------
    mols : dict or astropy.units.Quantity
        the amount(s) of substance(s)
    species : list of str
        if mols is not a dict, specify species names here
    unit : astropy unit
        the desired output unit

    Returns
    -------
    activity : astropy.units.Quantity
        the activity vector

    """

    isscalar = False
    if isinstance(mols, dict):
        data = mols
    else:
        if isinstance(species, str):
            species = [species]
            isscalar = True
        data = {}
        if isscalar:
            data[species[0]] = mols
        else:
            for i in range(len(species)):
                data[species[i]] = mols[i]
    data_out = {}

    for key, q in data.items():
        if key == "stable":
            data_out[key] = 0. * unit
        else:
            data_out[key] = (q * constants.N_A * np.log(2) / vsg_halflifes[key]).to(u.Bq).to(unit)
    if isscalar:
        data_out = data_out[key]
    return data_out


def get_daughters(name, nuclide_database):
    """
    retrieve daughter nuclides for nuclide given its name.

    Parameters
    ----------
    name : str
        name of nuclide

    Returns
    -------
    d : dict
        dict with structure {str : float} relating daughter nuclides to their decay constant

    """
    if name == "stable":
        return {}
    if nuclide_database == "rd":
        return get_daughters_rd(name)
    if nuclide_database == "vsg":
        return get_daughters_vsg(name)

    msg = "nuclid_database must be rd or vsg"
    raise NotImplementedError(msg)

def get_daughters_rd(name):
    """
    retrieve daughter nuclides for nuclide given its name from the radioactive_decay data

    Parameters
    ----------
    name : str
        name of nuclide

    Returns
    -------
    d : dict
        dict with structure {str : float} relating daughter nuclides to their decay constant

    """
    import radioactivedecay as rd

    # dealing with syntax change radioactivedecay. not entirely sure at which version number the syntax changed
    if float(rd.__version__.replace('.', "")) >= 46.0:
        nuclide_command = rd.Nuclide
    else:
        nuclide_command = rd.Radionuclide

    nuc = nuclide_command(name)
    daughters = nuc.progeny()
    branching = nuc.branching_fractions()
    lam = np.log(2) / nuc.half_life()
    res = {}
    for i, daughter in enumerate(daughters):
        res[daughter] = lam * branching[i]
    return res

def get_daughters_vsg(name):
    """
    retrieve daughter nuclides for nuclide given its name. Implements the nuclid model used in VSG
    see VSG-278 page 98/99
    decays labeled as "stable"

    Parameters
    ----------
    name : str
        name of nuclide

    Returns
    -------
    d : dict
        dict with structure {str : float} relating daughter nuclides to their decay constant

    """
    if name in vsg_tab411_data:
        mydaughter = vsg_tab411_data[name].copy()
        for key in mydaughter:
            mydaughter[key] = np.log(2) / mydaughter[key].to(u.s).value
        return mydaughter
    return {}

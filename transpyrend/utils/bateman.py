"""
Functions to solve radioactive decay in linear decay chains.

The code has been adapted from
https://github.com/bjodah/batemaneq, commit 00ba061737151a5eb46bd2281a8351e5c5e7966c.
The copyright notice has been retained as required by the license."""
import numpy as np
from numba import jit
"""Copyright (c) 2015-2016, Björn Dahlgren
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

  Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""


@jit(nopython=True)
def bateman_parent(lmbd, t, one=1, zero=0, exp=np.exp):
    """
    Calculate daughter concentrations (number densities) from single parent
    Assumes an initial parent concentraion of one (and zero for all daughters)

    Parameters
    ----------
    lmbd: array_like
        decay constants (one per species)
    t: float
        time
    one: object
        object corresponding to one, default(int(1)),
        could e.g. be :py:func:`sympy.S('1')`
    zero: object
        object corresponding to zero, default(int(0))
        could e.g. be :py:func:`sympy.S('0')`
    exp: callback
        Callback calculating the exponential of an argument
        default: numpy.exp or math.exp, could e.g. be :py:func:`sympy.exp`

    Returns
    -------
    N : array_like
        the calculated concentration

    """
    n = lmbd.size
    N = np.zeros(n)
    lmbd_prod = one

    for i in range(n):
        if i > 0:
            lmbd_prod *= lmbd[i - 1]
        sum_k = zero
        for k in range(i + 1):
            prod_j = one
            for j in range(i + 1):
                if j == k:
                    continue
                prod_j *= lmbd[j] - lmbd[k]
            sum_k += exp(-lmbd[k] * t) / prod_j
        N[i] = lmbd_prod * sum_k
    return N


@jit(nopython=True)
def bateman_full(y0s, lmbd, t, one=1, zero=0, exp=np.exp):
    """
    Calculates a linear combination of single-parent chains
    Generalized helper function for when y0 != [1, 0, 0, ... ]

    Note: if we deal with a single linear chain, simply provide the ICs for each member of the chain, the decay
    constants and the time at which we want to evaluate the problem. In case the chain is branched (with convergent
    branches), call the function on every linear subchain.

    E.g., if we have:

    >>> A -> B -> D -> E
    ...      C -> D -> E

    Call twice with A..., and C... Make sure that each you consider the IC for each species only once. In this example,
    this can be achieved by calling for the first subchain will all relevant initial concentrations y0_A, y0_B, y0_D,
    y0_E. For the second subchain, use y0_C, but set y0_D, y0_E to zero to avoid consindering them twice.


    Parameters
    ----------
    y0s: array_like
        Initial concentrations
    t: float
        time
    one: object
        object corresponding to one, default(int(1)),
        could e.g. be :py:func:`sympy.S('1')`
    zero: object
        object corresponding to zero, default(int(0))
        could e.g. be :py:func:`sympy.S('0')`
    exp: callback
        Callback calculating the exponential of an argument
        default: math.exp, could e.g. be :py:func:`sympy.exp`

   Returns
   -------
   N : array_like
        the calculated concentration
    """
    n = lmbd.size
    if y0s.shape[0] != n:
        msg = (
            "Please pass equal number of decay"
            " constants as initial concentrations"
            " (you may want to pad lmbd with zeroes)"
        )
        raise ValueError(msg)
    N =  np.zeros_like(y0s)
    for i, y0 in enumerate(y0s):
        Ni = bateman_parent(lmbd[i:], t, one, zero, exp)
        for j, yj in enumerate(Ni, i):
            N[j] += y0*yj
    return N

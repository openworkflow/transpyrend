"""
contains some other helper routines
"""
import astropy.units
import h5py
import numpy as np
import functools

from astropy import units as u


def find_diagonal_indizes(a):
    """
    find the indices of the diagonal elements in a sparse matrix

    Parameters
    ----------
    a : matrix
        a sparse matrix

    Returns
    -------
    datainds : array-like
        the indices

    """
    datainds = []
    indices = a.indices # column indices of filled values
    indptr = a.indptr   # auxiliary "pointer" to data indices
    for irow in range(a.shape[0]):
        rowinds = indices[indptr[irow]:indptr[irow+1]] # column indices of the row
        if irow in rowinds:
            # then we've got a diagonal in this row
            # so let's find its index
            datainds.append(indptr[irow] + np.flatnonzero(irow == rowinds)[0])
    return np.array(datainds)


def average_midpoint_variable(param, dxs, averaging_method="harmonic_mean"):
    """
    calculate values at midpoints between cells from cell-centered values

    Parameters
    ----------
    param : astropy.units.Quantity
        values considered
    dxs : array-like
        the grid spacings
    averaging_method : string, default="harmonic_mean"
        the averaging method. choose "arithmetic_mean", "geometric_mean" or "harmonic_mean"

    Returns
    -------
    param_mid : astropy.units.Quantity
        values at cell mid-points
    """
    if param.ndim > dxs.ndim:
        dxs = dxs[:, np.newaxis]
    if isinstance(param, astropy.units.quantity.Quantity):
        param = param.value
    if averaging_method == "arithmetic_mean":
        return (dxs[1:] * param[1:] + dxs[:-1] * param[:-1]) / (dxs[1:] + dxs[:-1])
    if averaging_method == "harmonic_mean":
        paramdiv = np.divide(dxs, param, out=np.zeros_like(param), where=param != 0.)
        return np.divide(dxs[1:] + dxs[:-1], paramdiv[1:] + paramdiv[:-1],
                              where=(paramdiv[1:] + paramdiv[:-1]) != 0.,
                              out=np.zeros_like(paramdiv[1:]))
    if averaging_method == "geometric_mean":
        return np.exp((np.log(param[1:]) * dxs[1:] + np.log(param[:-1]) * dxs[:-1]) / (dxs[1:] + dxs[:-1]))

    raise NotImplementedError


def interpolator(func):
    """
    a wrapping funcion/decorator to interpolate concentrations in BaseTransportModel1D
    """
    @functools.wraps(func)
    def inner(*args, **kwargs):
        """wrapping function"""
        smooth = False
        with_units = True
        if "with_units" in kwargs:
            with_units = kwargs.pop("with_units")
        if "smooth" in kwargs:
            smooth = kwargs.pop("smooth")

        x, c = func(*args, **kwargs)
        xunit = x.unit

        from scipy.interpolate import interp1d
        interp = interp1d(x, c)
        if smooth:
            from scipy.ndimage import gaussian_filter1d
            c = gaussian_filter1d(c, 1.)
        if with_units:
            def interp1d_with_units(x):
                """
                wrapper to maintain units for interpolator

                Parameters
                ----------
                x : astropy.Quantity

                Returns
                -------
                interp : function
                    interpolator with unit support
                """
                return interp(x.to(xunit).value) * c.unit

            return x, interp1d_with_units

        def interp1d_no_units(x):
            """
            wrapper for interpolator

            Parameters
            ----------
            x : astropy.Quantity

            Returns
            -------
            interp : function
                interpolator
            """
            return interp(x)

        return x.value, interp1d_no_units

    return inner


def summed(func):
    """a wrapper/decorator to wrap radionuclide summation in BaseTransportModel1D"""
    @functools.wraps(func)
    def inner(*args, **kwargs):
        """wrapping function"""
        get_data = func(*args, **kwargs)
        self = args[0]
        ignore_stable = True
        if "ignore_stable" in kwargs:
            ignore_stable = kwargs.pop("ignore_stable")

        csum = np.zeros(self.results["t"].size)
        c = None
        for ispecies in range(self.nspecies):
            if ignore_stable and self.species[ispecies] == "stable":
                continue
            if c is None:
                x, c = get_data(ispecies, **kwargs)
                csum = c
            else:
                csum += get_data(ispecies, **kwargs)[1]
        return x, csum
    return inner

def is_hdf5(fname):
    """
    check if fname is a valid hdf5 file

    Parameters
    ----------
    fname : str
        file name

    Returns
    -------
    b : bool
        True if fname is an hdf5 file
    """
    import h5py
    b = False
    try:
        with h5py.File(fname,"r"):
            b = True
    except OSError:
        b = False
    return b


class HDF5Results:
    """
    a wrapper for dealing with the hdf5 results from a transpyrend ensemble run
    """
    def __init__(self, fname):
        self.fname = fname
        with h5py.File(fname, "r") as f:
            nruns = 0
            for key in f:
                if "model" in key:
                    nruns += 1
            self.nruns = nruns

    @property
    def maximum_transport_length(self):
        """
        collate the maximum transport length from each run in the file

        Returns
        -------
        result : astropy.units.Quantity
            the resulting array
        """
        result = np.zeros(self.nruns)
        with h5py.File(self.fname, "r") as f:
            for i in range(self.nruns):
                result[i] = f[f"model{i}"]["maximum_transport_length"][:]
        return result * u.m

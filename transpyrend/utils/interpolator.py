import numpy as np
from astropy import units as u
from numba import types

DEFAULT_CONFIG = {"type": "linear"}


class Interpolator1D:
    """
    Linear interpolation of a vector-valued function of some scalar x; can be stored and loaded from disk;
    uses astropy units and has a fast jit based implementation.
    """

    def __init__(self, filename=None, xdata=None, ydata=None, config=DEFAULT_CONFIG, group="/"):
        """

        Parameters
        ----------
        filename : str
            if not none, hdf5 filename from which to load the data
        xdata : astropy.units.Quantity
            x coordinates of the data with shape (n)
        ydata : astropy.units.Quantity
            y coordinates of the data with shape (m,n), where m is the number of components of y.
        config : dict
            obsolete by now
        group: string
            if filename is given, the group in the hdf5 file where the data is stored
        """
        self.interpolate = None
        self.interpolate_no_units = None

        self.xunit = None
        self.yunit = None
        self.ndim = None
        if filename is not None:
            assert xdata is None
            assert ydata is None
            self.from_disk(filename, group)
        elif xdata is not None:
            assert ydata is not None
            self.from_data(xdata, ydata)
        else:
            raise NotImplementedError

    def from_disk(self, filename, group="/"):
        """
        read Interpolator1D object from disk

        Parameters
        ----------
        filename : str
            the filename
        group : str
            the group inside the hdf5 file in which our data resides
         """
        import h5py
        with h5py.File(filename, "r") as f:
            g = f[group]
            self.xdata = g["xdata"][:]
            self.ydata = g["ydata"][:]
            self.ns = g.attrs["ns"]
            self.xunit = u.Unit(g.attrs["xunit"].strip("\""))
            self.yunit = u.Unit(g.attrs["yunit"].strip("\""))
        self.xdata *= self.xunit
        self.ydata *= self.yunit
        self.setup()

    def _to_disk(self, f, creator="", comment="", version="", group=""):
        """
        write Interpolator1D to disk as hdf5 file

        Parameters
        ----------
        f : h5py.File
            the open file handle
        creator : str
            metadata
        comment : str
            metadata
        version : str
            metadata
        group : str
            the group in which the data will be stored in the hdf5 file


        """
        if group != "/":
            g = f.create_group(group)
        f.create_dataset(f"{group}/xdata", data=self.xdata.value)
        f.create_dataset(f"{group}/ydata", data=self.ydata.value)
        if group == "/":
            g = f
        g.attrs["ns"] = self.ns
        g.attrs["xunit"] = str(self.xunit)
        g.attrs["yunit"] = str(self.yunit)
        g.attrs["comment"] = comment
        g.attrs["creator"] = creator
        g.attrs["version"] = version

    def to_disk(self, filename, creator="", comment="", version="", group="/"):
        """
        write Interpolator1D to disk as hdf5 file

        Parameters
        ----------
        filename : str
           the filename
        creator : str
           metadata
        comment : str
           metadata
        version : str
           metadata
        group : str
           the group in which the data will be stored in the hdf5 file
       """
        import h5py
        with h5py.File(filename, "w") as f:
            self._to_disk(f, creator=creator, comment=comment, version=version, group=group)

    def from_data(self, xdata, ydata):
        """
        create Interpolator1D from existing data

        Parameters
        ----------
        xdata : astropy.units.Quantity
            x data we want to interpolate, shape [n]
        ydata : astropy.units.Quantity
            y data we want to interpolate, shape [m,n], with m the number of values in a single vector

        """
        self.xunit = xdata.unit
        self.yunit = ydata.unit

        if ydata.ndim == 1:
            ydata = np.array([ydata, ]) * self.yunit
        ns = ydata.shape[0]
        self.ydata = ydata
        self.ns = ns
        self.xdata = xdata
        self.setup()

    def setup(self):
        """
        setup the interpolator

        Returns
        -------

        """
        xdata = self.xdata.value
        ydata = self.ydata.value
        ns = self.ns

        def ip(x):
            x = x.to(self.xunit).value
            if np.any(x > xdata.max()):
                msg = "x value above interpolation range"
                raise NotImplementedError(msg)
            if np.any(x < xdata.min()):
                msg = "x value below interpolation range"
                raise NotImplementedError(msg)
            y = _interpolator(x)
            return y * self.yunit

        def _interpolator_scalar(t):
            y = np.zeros(ns)
            for i in range(ns):
                y[i] = np.interp(t, xdata, ydata[i])
            return y

        def _interpolator_array(t):
            y = np.zeros((ns, t.size))
            for i in range(ns):
                for j in range(t.size):
                    y[i, j] = np.interp(t[j], xdata, ydata[i])
            return y
        # turned off jit here because of a memory leak.
        #@generated_jit(nopython=True)
        def _interpolator(t):
            if isinstance(t, (types.Float, np.floating)):
                return _interpolator_scalar(t)

            return _interpolator_array(t)

        self.interpolate = ip
        self.interpolate_no_units = _interpolator

    def __call__(self, x, with_units=True):
        """
        return the interpolated value at x

        Parameters
        ----------
        x : astropy.units.Quantity
            the x value(s) at which we want to interpolate, shape [n]
        with_units : bool
            if true, return interpolated values with units

        Returns
        -------
        y : astropy.units.Quantity or np.array
            the interpolated values
        """
        if with_units:
            return self.interpolate(x)

        return self.interpolate_no_units(x)

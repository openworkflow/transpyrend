import numpy as np
import scipy
from astropy import units as u


def erfc(x):
    return scipy.special.erfc(x)

def diffusion_advection_eq_soler2001(x, t, v, D, c0):
    """
    analytic solution for 1D cartesian transport with fixed BC c=c0 at x=0 for all t, eq. 24 in Soler+2001
    Arguments:
    c0 = bc concentration
    t = time
    x = distance from origin
    v = Darcy velocity
    D = diffusion coefficient
    """
    return c0 * 0.5 * (erfc((x - v * t) / (2 * np.sqrt(D * t))) \
                    + np.exp(v * x / D) * erfc((x + v * t) / (2 * np.sqrt(D * t))))


def v_eq_soler2001(De, K, dhdx, porosity, sigma=0., kT=0. * u.m ** 2 / u.s / u.K, dSdx=0.0, dTdx=0.0 * u.K / u.m,
                   s=0.0 / u.K):
    """
    Equation 23 in Soler (2001)

    TODO numpy docstring

    De = effective diffusion coefficient
    K = permeability
    dhdx = head gradient
    sigma = osmotic efficiency
    s = soret coefficient
    kT = thermo-osmotic permeability
    dTdx = temperature gradient
    dSdx = osmotic pressure head
    """
    return ((-K * dhdx + sigma * K * dSdx + sigma * K * dhdx - De * s * dTdx - kT * dTdx) / porosity).to(u.m / u.s)


import os

import pandas as pd
import pickle
from functools import cached_property
from astropy import units as u, constants
_base_path = os.path.dirname(os.path.abspath(__file__))
_data_path = os.path.join(_base_path, "../data")


class Nudata2020Wrapper(dict):
    """
    reads and converts the atomic mass data from the Nubase 2020 data, to be found here:
    https://www-nds.iaea.org/amdc/
    Usage:

    >>> nu = Nudata2020Wrapper()
    ... print(nu["Am-242m"])

    NOTE: To speed things up, we are caching the result from reading in the table. We also write the content of the
    original nubase text file into a binary file for faster I/O.

    """
    @cached_property
    def raw_data(self):
        cache_file = os.path.join(_data_path, "nu.pickle")
        if os.path.isfile(cache_file):
            with open(cache_file, "rb") as f:
                return pickle.load(f)
        else:
            raw_data = pd.read_fwf(os.path.join(_data_path, "nubase_3.mas20.txt"),
                                   colspecs=[(0, 3), (4, 8), (11, 16), (16, 17), (18, 31), (31, 42)],
                                   skiprows=25, dtype=str,
                                   names=["Mass Number", "Atomic Number", "Name", "Isomer", "Mass Excess",
                                          "Mass Excess Uncertainty"], header=None)
            for i in range(len(raw_data)):
                if (raw_data["Isomer"].values[i]) != (raw_data["Isomer"].values[i]):
                    raw_data["Isomer"].values[i] = ""
            raw_data["Mass Number"] = raw_data["Mass Number"].astype(int)
            raw_data["Mass Excess Measured"] = True
            raw_data["Mass Excess Uncertainty Measured"] = True
            for i in range(len(raw_data)):
                v = raw_data["Mass Excess"].values[i]
                if isinstance(v, float):
                    continue
                if "#" in v:
                    raw_data["Mass Excess Measured"].values[i] = False
                raw_data["Mass Excess"].values[i] = v.strip("#")

                v = raw_data["Mass Excess Uncertainty"].values[i]
                if isinstance(v, float):
                    continue
                if "#" in v:
                    raw_data["Mass Excess Uncertainty Measured"].values[i] = False
                raw_data["Mass Excess Uncertainty"].values[i] = v.strip("#")
            raw_data["Mass Excess"] = raw_data["Mass Excess"].astype(float)
            raw_data["Mass Excess Uncertainty"] = raw_data["Mass Excess Uncertainty"].astype(float)
            import re
            raw_data["Name2"] = ""
            for i in range(len(raw_data)):
                s = raw_data["Name"].values[i]
                suffix = raw_data["Isomer"].values[i]
                n, elem = (re.findall("([0-9]+)(.*)", s))[0]
                name2 = elem + "-" + n + suffix
                raw_data["Name2"].values[i] = name2
            raw_data["Atomic Mass"] = 0.0
            raw_data["Atomic Mass Uncertainty"] = 0.0
            for i in range(len(raw_data)):
                mass_excess = raw_data["Mass Excess"].values[i] * u.keV
                mass_number = raw_data["Mass Number"].values[i]
                mass_excess_unc = raw_data["Mass Excess Uncertainty"].values[i] * u.keV
                raw_data["Atomic Mass"].values[i] = mass_number + (
                        mass_excess.to(u.femtogram, u.mass_energy()) / constants.u).to(
                    u.dimensionless_unscaled).value
                raw_data["Atomic Mass Uncertainty"].values[i] = (
                        mass_excess_unc.to(u.femtogram, u.mass_energy()) / constants.u).to(
                    u.dimensionless_unscaled).value
            with open(cache_file, "wb") as f:
                pickle.dump(raw_data, f)
            return raw_data



    def __init__(self):
        super().__init__()
        for i, name in enumerate(self.raw_data["Name2"].values):
            self[name] = self.raw_data["Atomic Mass"].values[i] * u.u

"""
some routines to plot date from transport models.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import transpyrend.core.ensemble
from transpyrend.core.ensemble import VSGEnsembleModel1D
from transpyrend.utils.language import translate_label
from transpyrend.utils.nuclides import get_species_VSG
import astropy.units as u


def illustrate_units(model, ax=None, print_labels=True, label_loc=0.0, alternate_by=0.0,
                     use_colors=False, rotation="horizontal", ha="right", va="top"):
    """
    For models with multiple geological layers: signify the extent of each layer and its name
    in a x,y = length, concentration plot.

    Parameters
    ----------
    model : BaseTransportModel1D
        the underlying model
    ax : axis object
        if given, draw on this axis
    print_labels : bool
        if true, draw labels for all units
    label_loc : float
        location of labels relative to axvlines in y direction
    alternate_by : float
        alternate location of labels by the amount. If the plot is in logscale, alternate_by is used multiplicative
    use_colors : bool
        if true, use different colors for the units
    rotation : str
        rotation of labels, horizontal or vertical
    ha : str
        horizontal alignment of the ax.text label
    va : str
        vertical alignment of the ax.text label

    Returns
    -------

    """
    if ax is None:
        ax = plt.gca()
    start = 0. * u.m
    cmap = mpl.cm.get_cmap('tab10')

    colors = [cmap(i) for i in np.linspace(0, 1.0, 10)]

    ncolors = len(colors)
    for i, unit in enumerate(model.parameters_rock):

        if ax.get_yscale() == "log":
            # if this is logscale, interpret alternate_by as a factor, not an offset.
            if i % 2 == 0:
                actual_label_loc = label_loc * alternate_by
            else:
                actual_label_loc = label_loc / alternate_by
        else:
            if i % 2 == 0:
                actual_label_loc = label_loc + alternate_by
            else:
                actual_label_loc = label_loc - alternate_by
        end = start + unit["thickness"]
        if use_colors:
            color = colors[i % ncolors]
            ax.axvspan(start.value, end.value, color=color, alpha=0.2)
        else:
            color = "k"
            ax.axvline(start.value, color="0.8")
            ax.axvline(end.value, color="0.8")
        if print_labels:
            # ax.text(start.value + margin, actual_label_loc, unit["name"], color=color, rotation=rotation,
            #        ha=ha, va=va)
            mid = (start.value + end.value) / 2.0
            ax.text(mid, actual_label_loc, unit["name"], color=color, rotation=rotation,
                    ha=ha, va=va)
        start = end


def plot_parameter(model, parameter, show_legend=True, species=None, show_units=True):
    """
    make a plot of De or R as a function of x for each selected species (or all).

    Parameters
    ----------
    model : BaseTransportModel1D
        the underlying model
    parameter : str
        the parameter to plot, either "effective_diffusion_coefficient", "retardation_factor", "v", or "D/R"
    show_legend : bool
        if true, show the legend
    species : list of str
        if provided, only plot parameter for given species
    show_units : bool
        if true, overplot the units

    Returns
    -------
    fig : figure object
        the figure created
    ax : axis object
        the axis
    """
    if isinstance(model, transpyrend.core.ensemble.BaseEnsembleModel1D):
        return plot_parameter_ensemble(model, parameter, show_legend, species, show_units)

    if parameter == "effective_diffusion_coefficient":
        y = model.D * model.R
    elif parameter == "retardation_factor":
        y = model.R
    elif parameter == "v":
        y = model.v
    elif parameter == "D/R":
        y = model.D
    else:
        raise NotImplementedError()
    x, _ = model.get_concentrations_x(0)
    mean_y_plotted = []
    if species is None:
        species = model.species
    for i, nuclid in enumerate(species):
        if nuclid not in model.species:
            continue
        plt.plot(x, y[:, i], label=nuclid)
        mean_y_plotted.append(np.mean(y[:, i]))
    mean_y = -1
    if len(mean_y_plotted) != 0:
        mean_y = np.mean(mean_y_plotted)
    plt.yscale("log")
    plt.xlabel("x in m")
    plt.ylabel(parameter)
    if show_units and mean_y > 0.:
        illustrate_units(model, print_labels=True, alternate_by=1.1, label_loc=1. * mean_y)
    if show_legend:
        plt.legend()
    return plt.gcf(), plt.gca()


def plot_parameter_ensemble(model, parameter, show_legend=True, species=None, show_units=True):
    """
    make a plot of De or R as a function of x for each selected species (or all).

    Parameters
    ----------
    model : BaseTransportModel1D
        the underlying model
    parameter : str
        the parameter to plot, either "effective_diffusion_coefficient", "retardation_factor", "v", or "D/R"
    show_legend : bool
        if true, show the legend
    species : list of str
        if provided, only plot parameter for given species
    show_units : bool
        if true, overplot the units

    Returns
    -------
    fig : figure object
        the figure created
    ax : axis object
        the axis

    """
    for m in model.models:
        _, ax = plot_parameter(m, parameter, species=species, show_legend=show_legend,
                                     show_units=show_units)
    if show_units:
        # workaround to place the unit labels at a location always within the plot range
        ydata = []
        for line in ax.lines:
            ydata.append(line.get_ydata())
        mean_y = np.mean(ydata)
        illustrate_units(model, print_labels=True, alternate_by=1.1, label_loc=1. * mean_y)
    plt.legend()
    return plt.gcf(), plt.gca()


def illustrate_units_local(model, ax=None, print_labels=True, label_loc=0.0, alternate_by=0.0,
                           use_colors=False, rotation="horizontal", ha="center", va="top", default_color="gray",
                           label_bounds=True, fontsize="large",
                           highlight_units=["jmopt", "jmopz"],
                           highlight_label="Wirtsgestein",
                           highlight_color="lightgrey",
                           ze_language="german"):
    """
    For models with multiple geological layers: signify the extent of each layer and its name
    in a x,y = length, concentration plot.

    Parameters
    ----------
    model : BaseTransportModel1D
        the underlying model
    ax : axis object
        if given, draw on this axis
    print_labels : bool
        if true, draw labels for all units
    label_loc : float
        location of labels relative to axvlines in y direction
    alternate_by : float
        alternate location of labels by the amount. If the plot is in logscale, alternate_by is used multiplicative
    use_colors : bool
        if true, use different colors for the units
    rotation : str
        rotation of labels, horizontal or vertical
    ha : str
        horizontal alignment of the ax.text label
    va : str
        vertical alignment of the ax.text label
    default_color : str or other matplotlib color specification
        default color for the lines
    label_bounds : bool
        if true, add a single label to the legend for all the unit boundaries
    fontsize : str or int
        fontsize used
    highlight_units : list of str
        highlight the geological units in this list with the highlight_color
    highlight_label : str
        a single label used for all the highlighted units
    highlight_color : str or other matplotlib color specification
        the color used for highlighting
    ze_language : str
        the language to be used for the labels

    Returns
    -------

    """
    bnd_label = translate_label(ze_language, "boundary_geological_units")

    if ax is None:
        ax = plt.gca()
    start = 0. * u.m
    cmap = mpl.cm.get_cmap('tab10')
    colors = [cmap(i) for i in np.linspace(0, 1.0, 10)]
    ncolors = len(colors)

    hl_count = 0

    for i, unit in enumerate(model.parameters_rock):

        if ax.get_yscale() == "log":
            # if this is logscale, interpret alternate_by as a factor, not an offset.
            if i % 2 == 0:
                actual_label_loc = label_loc * alternate_by
            else:
                actual_label_loc = label_loc / alternate_by
        else:
            if i % 2 == 0:
                actual_label_loc = label_loc + alternate_by
            else:
                actual_label_loc = label_loc - alternate_by
        end = start + unit["thickness"]

        if label_bounds and i == 0:
            label = bnd_label
        else:
            label = None

        if use_colors:
            color = colors[i % ncolors]
            ax.axvspan(start.value, end.value, color=color, label=label,
                       alpha=0.2, zorder=1)
        else:
            color = default_color
            ax.axvline(start.value, color="0.8", label=label, zorder=1)
            ax.axvline(end.value, color="0.8", zorder=1)

        if unit["name"] in highlight_units:
            if hl_count == 0:
                label = highlight_label
            else:
                label = None
            ax.axvspan(start.value, end.value, color=highlight_color, label=label, zorder=0)
            hl_count += 1

        if print_labels:
            # ax.text(start.value + margin, actual_label_loc, unit["name"], color=color, rotation=rotation,
            #        ha=ha, va=va)
            mid = (start.value + end.value) / 2.0
            if i == 0:
                label = "geological unit"
            else:
                label = None
            ax.text(mid, actual_label_loc, unit["name"], color=color, rotation=rotation,
                    ha=ha, va=va, zorder=2, fontsize=fontsize, label=label)
        start = end


def plot_overview_c_x(model, index_time=-1, only_chains=None, only_species=None,
                      figsize=(12, 16), fontsize_legend="small", fontsize_legend_title="medium",
                      compact=True, specify_sorbed_species=False, legend_pos="top", ze_language="german",
                      quantity="mass density",
                      repo_width=2, label_depth_on_xaxis=True, per_rock_volume=False):
    """
    plot the concentration as a function of location for a single index_time for a VSG-scheme model

    Parameters
    ----------
    model : VSGEnsembleModel1D
        the underlying model
    index_time : int
        the index of the snapshot to be used for the plot
    only_chains : list
        list of the chains to be considered. If None, use all chains
    only_species : list
        list of the species to be considered. If None, use all species
    figsize : tuple (int, int)
        the figure size
    fontsize_legend : int or str
        fontsize of the legend
    fontsize_legend_title : int or str
        title of the legend title
    compact : bool
        if true, squeeze the plot
    specify_sorbed_species : bool
        if true, write out the names of sorbed species in the legend
    legend_pos : str
        position of the legend
    ze_language : str
        language for the plot labels
    quantity : str
        either mass density or molar density
    repo_width : int
        width of the repository
    label_depth_on_xaxis : bool
        if true, conver the x labels to depth
    per_rock_volume : bool
        if true, plot concentrations per rock volume

    Returns
    -------
    fig : figure object
        the figure created
    ax : axis object
        the axis
    """
    if not isinstance(model, VSGEnsembleModel1D):
        msg = "this works only for a model of type VSGEnsembleModel1D"
        raise NotImplementedError(msg)
    non_sorbed_label = translate_label(ze_language, "non_sorbed_label")
    chain_names = translate_label(ze_language, "chain_names")
    xlabel = 'x (m)'
    xlabel_d = translate_label(ze_language, "depth") + " (m)"
    if quantity == "mass density":
        ylabel = r'C ($kg \; m^{-3}$)'
    elif quantity == "molar density":
        ylabel = r'C ($mol \; m^{-3}$)'
    elif quantity == "activity density":
        ylabel = r'C ($Bq \; m^{-3}$)'
    else:
        raise NotImplementedError()
    from cycler import cycler

    custom_cycler = (cycler(ls=["-", "--", ":", "-."]) *
                     plt.rcParams['axes.prop_cycle'])
    chains = get_species_VSG(by_chain=True)
    nchains = len(chains)
    if only_chains is not None:
        nchains = len(only_chains)
    if compact is True:
        nsubplots = 1
        subplot_ix = 0
        sorbed_species_label = ""
        specie_label_count = 0
        sorbed_specie_count = 0
    else:
        nsubplots = nchains

    fig, axs = plt.subplots(nsubplots, 1, figsize=figsize, sharex=True)

    if nsubplots == 1:
        axs = [axs]

    for i, species_chain in enumerate(chains):

        if compact is False:
            subplot_ix = i

        if only_chains is not None and chain_names[i] not in only_chains:
            continue

        axs[subplot_ix].set_prop_cycle(custom_cycler)

        for j, nuclid in enumerate(species_chain):
            if only_species is not None and nuclid not in only_species:
               continue
            x, c = model.get_concentrations_x(nuclid, itime=index_time, quantity=quantity,
                                              per_rock_volume=per_rock_volume)

            if compact is False or model.check_if_significant_transport(c,
                                                                        repo_grid_cell_distance=repo_width) is True:
                axs[subplot_ix].plot(x, c, label=nuclid)
            elif compact is True:
                if len(sorbed_species_label) == 0:
                    sorbed_species_label += f"{nuclid}"
                else:
                    sorbed_species_label += f",{nuclid}"
                specie_label_count += 1
                if specie_label_count > 4:
                    sorbed_species_label += os.linesep
                    specie_label_count = 0
                axs[subplot_ix].plot(x, c, color="black", lw=0.25)

                sorbed_specie_count += 1
            lastj = j
        axs[subplot_ix].set_yscale("log")

    # plot last sorbed species to get a label
    nuclid = chains[i][lastj]
    x, c = model.get_concentrations_x(nuclid, itime=index_time)
    if specify_sorbed_species is True:
        label = sorbed_species_label
    else:
        label = f"{sorbed_specie_count} " + non_sorbed_label
    axs[subplot_ix].plot(x, c, label=label, color="black", lw=0.5)

    for ax in axs:
        label_loc = 1e-2
        if quantity == "activity density":
            label_loc = 1e2
        illustrate_units_local(model, ax, label_loc=label_loc, alternate_by=1,
                                     rotation="vertical", ze_language=ze_language)
    for i, ax in enumerate(axs):
        if compact is False:
            title = chain_names[i]
        else:
            title = None

        if legend_pos == "side":
            if compact is True:
                ncol = 1
            else:
                ncol = 3
            bbox = (1.03, 1.0)
            loc = "upper left"
        else:
            ncol = 4
            bbox = (0.0, 1.03)
            loc = "lower left"

        legend = ax.legend(fontsize=fontsize_legend, ncol=ncol, bbox_to_anchor=bbox, loc=loc,
                           title=title)

        plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
        if quantity in ["mass density", "molar density"]:
            ax.set_ylim(1e-10, 1e-1)
        else:
            ax.set_ylim(1, 1e10)

    if label_depth_on_xaxis is True:
        xmax = x.max().to(u.m).value
        x_int = 200.0  # * u.m
        ds = np.arange(0, xmax, x_int)
        dl = ["%0.0f" % d for d in ds]
        xl = xmax - ds
        axs[-1].set_xticks(xl)
        axs[-1].set_xticklabels(dl)
        axs[-1].set_xlabel(xlabel_d)
    else:
        axs[-1].set_xlabel(xlabel)
    for ax in axs:
        ax.set_ylabel(ylabel)

    fig.tight_layout()

    return fig, axs


def plot_animated_evolution(model, fname, plot_only=None, plot_every=10, highlight_only=None,
                            quantity="mass density", ze_language="german"):
    """
    make an animated plot of the evolution of the concentration

    Parameters
    ----------
    model : BaseTransportModel1D
        the underlying model
    fname: str
        filename of the generated video
    plot_only: list, default None
        list of radionuclides to plot. If None, all species will be plotted.
    plot_every: int, default 10
        plot every nth output only
    highlight_only: list, default None
        label only these nuclides individually (the others will be shown with all the same color).
        If None, all nuclides will be labeled.
    quantity: str, default "mass density"
        "mass density" or "molar density"
    ze_language: str, default "english"
        the language for the plot labels

    Returns
    -------
    anim: FuncAnimator
        the generated animation

    """
    nrepeats_last_frame = 50
    nrepeats_first_frame = 50
    valid_species = []
    if plot_only is None:
        for nuc in model.species:
            if nuc != "stable":
                valid_species.append(nuc)
        plot_only = valid_species

    n = len(plot_only)
    if highlight_only is None:
        highlight_only = plot_only
    # put the highlighted nuclides in front
    temp = highlight_only.copy()
    for nuc in plot_only:
        if nuc not in temp:
            temp.append(nuc)
    plot_only = temp

    if quantity == "mass density":
        ylabel = r'C ($kg \; m^{-3}$)'
    elif quantity == "molar density":
        ylabel = r'C ($mol \; m^{-3}$)'
    elif quantity == "activity density":
        ylabel = r'C ($Bq \; m^{-3}$)'
    else:
        raise NotImplementedError()
    xlabel = translate_label(ze_language, "depth") + " (m)"
    lines = []
    # First set up the figure, the axis, and the plot element we want to animate
    fig = plt.figure(dpi=300)
    ax = plt.axes(xlim=(0, model.xmax.value + 50), ylim=(1e-10, 1))
    ax.set_yscale("log")
    first_grey = True
    for i in range(n):
        if plot_only[i] in highlight_only:
            line, = ax.plot([], [], lw=2, label=plot_only[i])
        else:
            if first_grey:
                ngrey = len(plot_only) - len(highlight_only)
                line, = ax.plot([], [], lw=2, c="0.1",
                                label=f"{ngrey} " + translate_label(ze_language, "non_sorbed_label"), alpha=0.2)
                first_grey = False
            else:
                line, = ax.plot([], [], lw=2, c="0.1", alpha=0.2)
        lines.append(line)
    plt.legend(loc="lower right", fontsize=5)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    illustrate_units_local(model, ax, label_loc=1e-1, alternate_by=2,
                                 rotation="vertical", ze_language=ze_language)
    x, _ = model.get_concentrations_x(0, -1)
    xmax = x.max().to(u.m).value
    x_int = 200.0  # * u.m
    ds = np.arange(0, xmax, x_int)
    dl = ["%0.0f" % d for d in ds]
    xl = xmax - ds
    ax.set_xticks(xl)
    ax.set_xticklabels(dl)

    def init():
        """initialization function: plot the background of each frame"""
        for i in range(n):
            lines[i].set_data([], [])

        return lines,

    #
    def animate(i, *fargs):
        """
        animation function, called sequentially to create frame by frame
        """
        model = fargs[0]
        plot_only = fargs[1]
        plot_every = fargs[2]
        quantity = fargs[4]
        nframes_net = fargs[5]
        nrepeats_first_frame = fargs[6]
        if i < nrepeats_first_frame:
            itime = 0
        elif i < nframes_net + nrepeats_first_frame:
            itime = (i - nrepeats_first_frame) * plot_every
        else:
            itime = (nframes_net) * plot_every
        t = round(model.results["t"][itime].to(u.yr).value)
        plt.title(f"t = {t} yr")
        for j in range(len(plot_only)):
            x, y = model.get_concentrations_x(plot_only[j], itime=itime, quantity=quantity)
            lines[j].set_data(x, y)
        return lines,

    nframes_net = model.results["t"].size // plot_every
    nframes = nframes_net + nrepeats_last_frame + nrepeats_first_frame
    # call the animator.  blit=True means only re-draw the parts that have changed.
    from matplotlib import animation
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=nframes, interval=20, blit=False, fargs=
                                   [model, plot_only, plot_every, highlight_only, quantity, nframes_net,
                                    nrepeats_first_frame])

    anim.save(fname, fps=15)
    return anim
    # plt.show()

"""
provides some functionality to translate the labels in plots
"""
_english_chain_names=chain_names = ["Activation products",
                           "Neptunium chain",
                           "Uranium chain",
                           "Thorium chain",
                           "Actinium chain"]
"""
the actual translation dictionary.
"""
_translation_plot_labels = {
    "boundary_geological_units":["boundary of geological units","Grenze geologischer Einheiten",
                                 "grens geologische eenheden"],
    "initial_mass":["initial mass","Anfangsmasse","initiele massa"],
    "initial_amount":["initial amount","Anfangsmenge","initiele massa"],
    "transport_distance":["transport distance","Transportlänge","transportlengte"],
    "depth":["Depth","Teufe","Diepte"],
    "cumulative_flux":["Cumulative flux","Kumulativer Fluss","Cumulatieve flux"],
    "non_sorbed_label":["nuclides\nwithout significant transport","Nuklide\nohne signifikanten Transport",
                        "nucliden\nzonder significant transport"],
    "chain_names":
        [_english_chain_names,
         _english_chain_names,
         ["Aktivierungsprodukte","Neptunium Reihe","Uranium Reihe","Thorium Reihe","Actinium Reihe"]
        ]
    }
_lang_idx = {"english":0,"german":1,"nederlands":2}


def translate_label(language, label):
    """
    return a label in chosen language

    Parameters
    ----------
    language : str
        the desired language
    label : str
        the (english) label from _translation_plot_labels

    Returns
    -------
    label : str
        the translated label
    """
    return _translation_plot_labels[label][_lang_idx[language]]

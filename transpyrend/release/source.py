"""implements helper classes to deal with source functions that feed radionuclides to transpyrend"""
from transpyrend.utils.interpolator import Interpolator1D
from astropy import units as u
import numpy as np
import logging
class BaseSourceTerm:
    """
    A class for describing a generic source term used in the near- and far field model.
    """
    def __init__(self, filename=None, nuclides=None, t=None, cum_rates=None, cum_rates_no_irf=None, interpolator=None,
                 interpolator_no_irf=None, comment=None, irf_time=None, irf_inventory=None):
        """

        Parameters
        ----------
        filename: str
            if present, load BaseSourceTerm from filename
        nuclides : list of str
            if present, specifies names of the nuclides that were considered in the calculation
        t : astropy.units.Quantity
            if present, times at which the cumulative rater were evaluated, shape [n].
        cum_rates : astropy.units.Quantity
            if present, cumulative rates, shape [m,n], where m is the number of nuclides considered
        interpolator : Interpolator1D object
            if present, interpolator object from which the BaseSourceTerm is generated
        comment : str
            comment string
        irf_time: astropy.units.Quantity
            if present, specifies a point in time at which the instant release fraction will be released
        irf_inventory:
            must be present if irf_time is present; specifies the inventory that is instanteneously released
        """
        self.interpolator = None
        self.comment = comment
        self.irf_time = None
        self.irf_inventory = None
        if filename is not None:
            assert nuclides is None
            assert cum_rates is None
            assert t is None
            self.from_disk(filename)
        elif interpolator is not None:
            assert nuclides is not None
            assert cum_rates is None
            assert t is None
            self.interpolator = interpolator
            self.interpolator_no_irf = interpolator_no_irf
            self.t = interpolator.xdata
            self.cum_rates = interpolator.ydata
            self.cum_rates_no_irf = interpolator_no_irf.ydata
            self.nuclides = nuclides
            self.irf_time = irf_time
            self.irf_inventory = irf_inventory
        elif cum_rates is not None:
            assert t is not None
            assert filename is None
            assert interpolator is None
            self.from_data(nuclides, t, cum_rates, cum_rates_no_irf)
            self.irf_time = irf_time
            self.irf_inventory = irf_inventory

    def from_disk(self, filename):
        """
        load source term from disk

        Parameters
        ----------
        filename : str
            filename

        """
        import h5py
        with h5py.File(filename, "r") as f:
            self.nuclides = f["nuclides"]
            self.comment = f.attrs["comment"]
            if "irf_time" in f:
                self.irf_time = f["irf_time"][()]*u.yr
                self.irf_inventory = f["irf_inventory"][:] * u.mol
        self.interpolator = Interpolator1D(filename=filename, group="interpolator")
        self.interpolator_no_irf = Interpolator1D(filename=filename, group="interpolator_no_irf")
        self.t = self.interpolator.xdata
        self.cum_rates = self.interpolator.ydata
        self.cum_rates_no_irf = self.interpolator_no_irf.ydata

    def to_disk(self, filename, comment=""):
        """
        write BaseSourceTerm object to disk

        Parameters
        ----------
        filename : str
            filename
        comment : str
            comment string
        """
        import h5py
        with h5py.File(filename, "w") as f:
            self.interpolator._to_disk(f, group="interpolator", creator="BaseSourceTerm")
            self.interpolator_no_irf._to_disk(f, group="interpolator_no_irf", creator="BaseSourceTerm")
            f.attrs["comment"] = comment
            f.create_dataset("nuclides", data=self.nuclides)
            if self.irf_time is not None:
                f.create_dataset("irf_time", data=self.irf_time.to(u.yr).value)
                f.create_dataset("irf_inventory", data=self.irf_inventory.to(u.mol).value)

    def from_data(self, nuclides, t, cum_rates, cum_rates_no_irf):
        """
        initialize from existing data

        Parameters
        ----------
        nuclides : list of str
            specifies names of the nuclides that were considered in the calculation
        t : astropy.units.Quantity
            times at which the cumulative rater were evaluated, shape [n].
        cum_rates : astropy.units.Quantity
            cumulative rates, shape [m,n], where m is the number of nuclides considered
        cum_rates : astropy.units.Quantity
            cumulative rates not including the IRF contribution, shape [m,n], where m is the number of nuclides
            considered

        """
        self.interpolator = Interpolator1D(xdata=t, ydata=cum_rates)
        self.interpolator_no_irf = Interpolator1D(xdata=t, ydata=cum_rates_no_irf)
        self.nuclides = nuclides
        self.cum_rates = cum_rates
        self.cum_rates_no_irf = cum_rates_no_irf
        self.t = t

    def get_rate(self, t, stencil_width=1*u.yr, ignore_instant_release=True):
        """
        returns the release rate at time t. Uses central differences on the interpolated, cumulative rates.

        Parameters
        ----------
        t : astropy.units.Quantity
            time at which the rate is desired. Must be scalar.
        stencil_width : astropy.units.Quantity
            the width of the stencil for the central differentiation
        ignore_instant_release : bool, default True
            if true, do not include the IRF in the rate

        Returns
        -------
        r : astropy.units.Quantity
            the rates at time t

        """
        # shaby trick to make this work with scalar input
        was_scalar = False
        if t.size==1:
            was_scalar = True
            t = [t.value, t.value]*t.unit
        ns = len(self.nuclides)

        myinterp = self.interpolator
        if ignore_instant_release:
            myinterp = self.interpolator_no_irf
        # scan for values of t where central difference would require evaluations outside of the domain

        y0 = np.zeros([ns, t.size])*myinterp.ydata.unit
        y1 = np.zeros([ns, t.size])*myinterp.ydata.unit
        deltat = np.zeros(t.size)*u.yr
        deltat[:] = 2*stencil_width
        #these should be fine, they are in the middle of the domain
        mask = (t - stencil_width > myinterp.xdata.min()) & (t + stencil_width < myinterp.xdata.max())
        y1[:,mask] = myinterp(t[mask] + stencil_width)
        y0[:,mask] = myinterp(t[mask] - stencil_width)
        #these are in the left corner of the domain
        deltat[~mask] /= 2.
        mask = (t - stencil_width <= myinterp.xdata.min())
        y1[:,mask] = myinterp(t[mask] + stencil_width)
        y0[:,mask] = myinterp(t[mask] - stencil_width)
        #these are in the right corner of the domain
        mask = (t + stencil_width >= myinterp.xdata.max())
        y1[:,mask] = myinterp(t[mask])
        y0[:,mask] = myinterp(t[mask] - stencil_width)

        d = (y1-y0)/deltat
        if was_scalar:
            return d[:,0]
        return d

    def get_rate_function(self, stencil_width=1*u.yr, ignore_irf=True):
        """
        return a jitted function to retrieve rates with better performance. Used to calculate the source term inside
        solve_ivp where the integrand is a jitted function.
        Parameters
        ----------
        stencil_width : astropy.units.Quantity
            the width of the stencil for the central differentiation
        ignore_instant_release : bool, default True
            if true, do not include the IRF in the rate

        Returns
        -------
        f : jitted function
            the rate function
        """
        from numba import jit
        myinterp = self.interpolator
        if ignore_irf:
            myinterp = self.interpolator_no_irf
        xx = myinterp.xdata.to(u.s).value
        yy = myinterp.ydata.to(u.mol).value
        ns = len(self.nuclides)
        dt = stencil_width.to(u.s).value
        @jit(nopython=True)
        def f(t):
            y = np.zeros(ns)
            for i in range(ns):
                if t-dt <= xx[0]:
                    y1 = np.interp(t+dt, xx, yy[i])
                    y2 = np.interp(t, xx, yy[i])
                    y[i] = (y2 - y1) / (dt)
                else:
                    y0 = np.interp(t - dt, xx, yy[i])
                    y2 = np.interp(t + dt, xx, yy[i])
                    y[i] = (y2 - y0)/(2*dt)
            return y
        return f

    def get_average_rate_over_timestep(self, ts):
        """
        return the averaged rate over a set of timesteps

        Parameters
        ----------
        ts : astropy.units.Quantity
            the times we are interested in; np.diff(ts) returns the actual time step sizes

        Returns
        -------
        rates : astropy.units.Quantity
            The average rates at the timesteps in question
        """
        #TODO if the release happens at t=0, we will miss this. Correct for it.
        dts = np.diff(ts)
        return (self.interpolator(ts[1:])-self.interpolator(ts[:-1]))/dts

    def get_subset(self, nuclides):
        """
        returns a new source term which contains only the nuclides given as argument.
        Parameters
        ----------
        nuclides : list of str
            the nuclides to consider

        Returns
        -------
        interp : BaseSourceTerm
            the reduced source term

        """
        cum_rates = np.zeros([len(nuclides),self.cum_rates.shape[1]])*self.cum_rates.unit
        cum_rates_no_irf = np.zeros([len(nuclides), self.cum_rates.shape[1]]) * self.cum_rates.unit
        irf_inventory = np.zeros([len(nuclides)]) * self.irf_inventory.unit
        for i,nuc in enumerate(nuclides):
            if nuc not in self.nuclides:
                if nuc=="stable":
                    continue
                msg = f"Unknown nuclide {nuc}"
                raise RuntimeError(msg)
            idx = self.nuclides.index(nuc)
            cum_rates[i] = self.cum_rates[idx,:]
            cum_rates_no_irf[i] = self.cum_rates_no_irf[idx, :]
            irf_inventory[i] = self.irf_inventory[idx]
        return BaseSourceTerm(nuclides=nuclides, t=self.t, cum_rates=cum_rates, cum_rates_no_irf=cum_rates_no_irf,
                              irf_inventory=irf_inventory, irf_time=self.irf_time)


class CompositeSourceTerm(BaseSourceTerm):
    """
    A source term consisting of a sum of source terms

    Parameters
    ----------
    source_terms : list of BaseSourceTerms
        the source terms to compose
    weights : array-like
        the weight of the individual BaseSourceTerms

    """
    def __init__(self, source_terms, weights=None):


        if weights is None:
            weights = np.ones(len(source_terms))
        #figure out the points in time we need to consider
        ts = []
        #figure out the total set of nuclides we consider
        from collections import OrderedDict
        mynucs = OrderedDict()
        for st in source_terms:
            for t in st.interpolator.xdata:
                ts.append(t.to(u.yr).value)
                for nuc in st.nuclides:
                    mynucs[nuc]=None

        ts = np.array(sorted(set(ts)))*u.yr
        nuclides = list(mynucs.keys())
        cum_rates = np.zeros([len(nuclides), ts.size])*u.mol
        cum_rates_no_irf = np.zeros([len(nuclides), ts.size]) * u.mol
        irf_inventory = np.zeros(len(nuclides))*u.mol
        irf_time = source_terms[0].irf_time
        for imodel,st in enumerate(source_terms):
            data1 = st.interpolator(ts)
            data2 = st.interpolator_no_irf(ts)
            for i in range(len(st.nuclides)):
                if st.nuclides[i] in nuclides:
                    j = nuclides.index(st.nuclides[i])
                    cum_rates[j,:] += data1[i,:] * weights[imodel]
                    cum_rates_no_irf[j, :] += data2[i, :] * weights[imodel]
                    irf_inventory[j] += st.irf_inventory[i] * weights[imodel]
            if irf_time != st.irf_time:
                logging.warning("Varying IRF times in CompositeSourceTerms - this will NOT work with the external solver \
                              interface!")

        super().__init__(nuclides=nuclides, t=ts, cum_rates = cum_rates, cum_rates_no_irf=cum_rates_no_irf,
                         irf_time=irf_time, irf_inventory=irf_inventory)




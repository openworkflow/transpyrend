"""implements the release model"""
import logging

import numpy as np
import astropy.units as u
import transpyrend.utils.nuclides
from numba import jit
from transpyrend.utils.nuclides import VSGDECAYDATA, evolve_to_age
import radioactivedecay as rd
import scipy.integrate

from transpyrend.utils.interpolator import Interpolator1D


class BaseReleaseModel:
    """
    implements base class for a release model, e.g. a model that yields the release of various radionuclides as a
    function of time. The release rate for a nuclide with index i is given by:

    .. math::
        \\dot{N}_i = (\\delta(t-t_b) f_{\\mathrm{IRF}} + (1 - f_{\\mathrm{IRF}})(f_{\\mathrm{matrix}} r_{\\mathrm{matrix}} +
        f_{\\mathrm{cladding}} r_{\\mathrm{cladding}} + f_{\\mathrm{vitrified}} r_{\\mathrm{vitrified}} ))N(t)
    where :math:`f_j` are the fractions of the nuclide in the instant release fraction, the cladding, matrix,
    and vitrified waste.

     :math:`t_b` is the time at which the canister fails.  :math:`r_{j}` are the rates of release for the individual
      components after failure.
    For matrix and cladding, the rates are constant over time until the component is completely dissolved after
    :math:`r_{j}^{-1}` yr, for the vitrified waste, the rate reads:

    .. math::
        r_{\\mathrm{vitrified}} = \\frac{3}{\\tau} (1-\\frac{t-t_b}{\\tau})^2

    Note that by definition,

    .. math::
        1 = f_{\\mathrm{IRF}} + (1 - f_{\\mathrm{IRF}}) (f_{\\mathrm{matrix}} +
         f_{\\mathrm{cladding}} + f_{\\mathrm{vitrified}})

    Parameters
    ----------
    nuclides : iterable of str
        list of nuclides to consider, in the format e.g. "I-129"
    inventory : dict
        nuclide vector at t=0, in mol.
    nuclide_parameters: dict
        the release parameters per nuclide. Each key is a nuclide, each value a dictionary, specifying values for
        "irf fraction", "matrix fraction", "cladding fraction", and "vitrified fraction".
    matrix_release_rate: astropy.units.Quantity
        the rate at which the matrix is dissolved, in 1/time
    cladding_release_rate: astropy.units.Quantity
        the rate at which the cladding is dissolved, in 1/time
    vitrified_lifetime: astropy.units.Quantity
        the characteristic life time of the vitrified waste matrix
    breaching_time: astropy.units.Quantity
        the time after which the canister fails and release begins
    nuclide_database: str
        if "vsg", use the decay data for the VSG nuclide scheme. If "default", use radioactivedecay's standard, IRCP-107
    ignore_decay: bool
        ignore decay in calculation; useful for illustration

    """

    def __init__(self, nuclides, nuclide_parameters, release_parameters, nuclide_database="VSG", ignore_decay=False,
                 storage_year=None, reference_year=None):

        super().__init__()
        #
        self.ignore_decay = ignore_decay
        # nuclides to consider
        if nuclides == "VSG":
            nuclides = transpyrend.utils.nuclides.get_species_VSG()
        self.nuclides = nuclides
        # number of nuclides
        self.n = len(nuclides)
        # the time at which the canister fail
        self.breaching_time = release_parameters["breaching_time"].to(u.yr)
        # setup the decay
        from radioactivedecay.decaydata import DEFAULTDATA
        if nuclide_database == "default":
            self.decay_data = DEFAULTDATA
            self.scheme = None
        elif nuclide_database == "VSG":
            self.decay_data = VSGDECAYDATA
            self.scheme = "VSG"
        else:
            raise NotImplementedError()

        # instant release fraction
        self.irf = np.zeros(len(nuclides))
        # matrix fraction
        self.mf = np.zeros(len(nuclides))
        # cladding fraction
        self.cf = np.zeros(len(nuclides))
        # vitrified fraction
        self.vf = np.zeros(len(nuclides))
        # total inventory at t=0 (not at t=t_breach!)
        self.inventory = np.zeros(len(nuclides)) * u.mol

        inventory = {}
        for key in nuclide_parameters:
            inventory[key] = nuclide_parameters[key]["inventory"]
        self.inventory_dict = inventory

        for i, nuc in enumerate(nuclides):
            self.irf[i] = nuclide_parameters[nuc]["instant_release_fraction"]
            self.mf[i] = nuclide_parameters[nuc]["matrix_fraction"]
            self.cf[i] = nuclide_parameters[nuc]["cladding_fraction"]
            self.vf[i] = nuclide_parameters[nuc]["vitrified_fraction"]
            self.inventory[i] = inventory[nuc].to(u.mol)
            s = self.mf[i] + self.cf[i] + self.vf[i]
            # the fractions should sum up to s=1.0. They might not do exactly, because of truncation errors.
            # check if s has a sensible value
            assert abs(s - 1.0)<1e-6
            if abs(s - 1.0)>1e-11:
                logging.debug(f"material fractions do not sum up exactly to one for {nuc}. "
                                f"Rescaling to ensure conservation of mass.")
                # rescale to get rid of any truncation errors
                self.mf[i] /= s
                self.cf[i] /= s
                self.vf[i] /= s
                s2 = self.mf[i] + self.cf[i] + self.vf[i]
                assert abs(s2 - 1.0)<1e-11

        # the release rate for the matrix
        self.matrix_release_rate = release_parameters["matrix_release_rate"].to(1. / u.yr)
        # the release rate for the cladding
        self.cladding_release_rate = release_parameters["cladding_release_rate"].to(1. / u.yr)
        # characteristic life time for the vitrified waste
        self.vitrified_lifetime = release_parameters["vitrified_lifetime"].to(u.yr)
        # the reference year of the inventory
        self.reference_year = reference_year
        logging.info(f"Inventory is at reference year {reference_year}")
        if storage_year is None:
            storage_year = reference_year
        # the storage year
        self.storage_year = storage_year
        inp = {}
        for i, nuc in enumerate(self.nuclides):
            inp[nuc] = self.inventory[i]
        from transpyrend.parameters.releasemodel import ReleaseModelParameters
        aged_inv = ReleaseModelParameters.evolve_inventory_to_storage_year(inp, nuclide_database,
                                                                                 reference_year, storage_year, True)
        self.inventory = [aged_inv[key].value for key in self.nuclides] * u.mol

    def get_average_rate_over_timestep(self, ts, interpolator=None, **kwargs):
        """
        calculate the average rate between the times ts, :math:`\\frac{N(t_i)-N(t_{i-1}}{dt}`

        Parameters
        ----------
        ts : astropy.units.Quantity
            the time(s) used for calculating the average rate
        interpolator : utils.interpolator.Interpolator1D
            the integrated rate function as Interpolator1D. If None, it will be calculated on the fly

        Returns
        -------
        average_rate : astropy.units.Quantity
            the average rate in mol/yr in an [nspecies, ntimes] array

        """
        if interpolator is not None:
            integrated = interpolator(ts)
        else:
            integrated = self.integrate_rate(ts, **kwargs)
        dts = np.diff(ts.to(u.yr))
        average_rate = (integrated[:, 1:] - integrated[:, :-1]) / dts
        # if the release happens at t=0, we will miss this. Correct for it.
        indizes = np.where(ts == 0.0*u.yr)[0]
        if len(indizes) > 1:
            msg = "Found t=0 multiple times in ts!"
            raise NotImplementedError(msg)
        if len(indizes) == 1:
            index = indizes[0]
            average_rate[:, index] = integrated[:, index] / dts[index]

        return average_rate

    def get_fraction_at_time(self, t):
        """
        The fractional release at time t

        Parameters
        ----------
        t : astropy.units.Quantity
            the time(s) at which to evaluate the fractional release

        Returns
        -------
        f : astropy.units.Quantity
            the fractional release rate(s) (1/time)

        """
        isscalar = False
        if t.size == 1:
            isscalar = True
            t = [t.value, t.value] * t.unit
        f = np.zeros([self.n, t.size]) * 1. / u.yr
        mask1 = (t < (1. / self.cladding_release_rate) + self.breaching_time) & (t >= self.breaching_time)
        f[:, mask1] += (self.cf * self.cladding_release_rate * (1. - self.irf))[:, np.newaxis]
        mask2 = (t < (1. / self.matrix_release_rate) + self.breaching_time) & (t >= self.breaching_time)
        f[:, mask2] += (self.mf * self.matrix_release_rate * (1. - self.irf))[:, np.newaxis]
        mask3 = (t < self.vitrified_lifetime + self.breaching_time) & (t >= self.breaching_time)
        fglass = (3./self.vitrified_lifetime * (1. - (t[mask3] - \
                  self.breaching_time)/self.vitrified_lifetime)**2).to(1./u.yr)
        f[:, mask3] += (self.vf * (1. - self.irf))[:, np.newaxis]* fglass
        if isscalar:
            return f[:, 0]
        return f

    def get_rate_at_time(self, t, include_irf=True):
        """
        the release rate at time t

        Parameters
        ----------
        t : astropy.units.Quantity
            the time(s) at which to evaluate the release rate
        include_irf : bool
            if true, include the irf contribution

        Returns
        -------
        f : astropy.units.Quantity
            the release rate(s) (mol/yr)

        """
        isscalar = False
        if t.size == 1:
            isscalar = True
            t = [t.value, t.value] * t.unit
        if self.ignore_decay:
            inv = self.inventory_dict
        else:
            inv = evolve_to_age(self.inventory_dict, t, nuclide_scheme=self.scheme, quantity="amount")
        myinv = np.zeros([self.n, t.size])
        for i, nuc in enumerate(self.nuclides):
            myinv[i] = inv[nuc]
        f = self.get_fraction_at_time(t)

        rate = f * myinv
        if include_irf:
            mask = (t == self.breaching_time)
            rate[:, mask] += self.irf[:, np.newaxis] * myinv[:, mask] / u.s
        if isscalar:
            return rate[:, 0]
        return rate



    def integrate_rate(self, ts, include_irf=True):
        """
        integrate the rate equation using a Runge-Kutta order 8 scheme.

        Mathematically, we solve the following two equations for each nuclide:

        .. math::
            \\dot{N}_{i} = \\sum_j \\lambda_{i,j} - \\Lambda_i
        with :math:`\\lambda_{i,j}` the decay rate from nuclide j to nuclide i, and :math:`\\Lambda_{i}`
        the total decay rate of nuclide i, and

        .. math::
            \\dot{N}_{i}^{\\mathrm{out}} = (\\delta(t-t_b) f_{\\mathrm{IRF}} + (1 - f_{\\mathrm{IRF}})(f_{\\mathrm{matrix}}
             r_{\\mathrm{matrix}} + f_{\\mathrm{cladding}} r_{\\mathrm{cladding}} +
             f_{\\mathrm{vitrified}} r_{\\mathrm{vitrified}} ))N(t)
        with the symbols as defined above.




        Parameters
        ----------
        ts : astropy.units.Quantity
            the time(s) at which to evaluate the integral
        include_irf : bool
            if true, include the irf contribution

        Returns
        -------
        invs : astropy.units.Quantity
            the inventories at times ts, given as array with shape (self.n, ts.size)

        """
        t0 = 0.0 * u.yr
        # a matrix A, where A_ij gives the decay constant for the decay from species i to species j
        lambda_plus = np.zeros([self.n, self.n])
        # a vector B, where B_i gives the total decay constant for species i
        lambda_minus = np.zeros(self.n)
        if not self.ignore_decay:
            for i, nuc in enumerate(self.nuclides):
                mynuc = rd.Nuclide(nuc, decay_data=self.decay_data)
                daughters = mynuc.progeny()
                lambda_minus[i] = np.log(2) / mynuc.half_life("s")  # *self.R[i]
                for idaughter, daughter in enumerate(daughters):
                    if daughter in self.nuclides:
                        j = self.nuclides.index(daughter)
                        lambda_plus[i, j] = np.log(2) / mynuc.half_life("s") * mynuc.branching_fractions()[idaughter]
        inv0 = np.zeros([2, self.n])
        t0_integration = self.breaching_time
        if (t0 == 0.0 * u.yr and self.breaching_time == 0.0*u.yr) or self.ignore_decay:
            inv0[0, :] = self.inventory
        else:
            inv = evolve_to_age(self.inventory_dict, self.breaching_time, nuclide_scheme=self.scheme, quantity="amount")

            for i, nuc in enumerate(self.nuclides):
                inv0[0, i] = inv[nuc].to(u.mol).value
        if include_irf:
            inv0[1, :] = self.irf * inv0[0, :]
        tb = self.breaching_time.to(u.s).value
        tm = (1. / self.matrix_release_rate).to(u.s).value
        tc = (1. / self.cladding_release_rate).to(u.s).value
        tv = self.vitrified_lifetime.to(u.s).value
        mf = self.mf
        cf = self.cf
        vf = self.vf
        irf = self.irf
        n = self.n

        @jit(nopython=True)
        def func(t, y):
            rhs = np.zeros((2, n))
            y = y.reshape((2, n))

            rhs[0] = y[0] @ lambda_plus - y[0] * lambda_minus
            if t >= tb:
                if t < tm + tb:
                    rhs[1] += (1. - irf) * mf / tm * y[0]
                if t < tc + tb:
                    rhs[1] += (1. - irf) * cf / tc * y[0]
                if t < tv + tb:
                    rhs[1] += (1. - irf) * vf * y[0] * (3./tv * (1. - (t - tb)/tv)**2)
            return rhs.reshape(y.size)

        def _integrate(t0, t1, ts, y0):
            res = scipy.integrate.solve_ivp(func, (t0.to(u.s).value, t1.to(u.s).value),
                                             y0.reshape(self.n * 2), method="DOP853", rtol=1e-12, atol=1e-12,
                                             t_eval=ts.to(u.s).value)
            if res["success"] is not True:
                msg = "Integrator failed!"
                raise RuntimeError(msg)
            return res["y"].reshape(2, self.n, ts.size) * u.mol  # indizes: (component, species, time)

        # integration will start from t0_integration, only time frames with t > t0_integration can be calculated
        t_eval = ts[ts>=t0_integration]

        # we add some time frames in order for the solver to touch the steps in the release rate
        t_eval_actual = t_eval
        idx_additional_frames = []
        for tbreak in sorted([1./self.cladding_release_rate, 1./self.matrix_release_rate]):
            if t0_integration < tbreak < ts[-1]:
                if tbreak in t_eval_actual:
                    continue
                idx = np.searchsorted(t_eval_actual, tbreak)
                t_eval_actual = np.insert(t_eval_actual, idx, tbreak)
                idx_additional_frames.append(idx)
        data = _integrate(t0_integration, ts[-1], t_eval_actual, inv0)
        invs = data[1, :, :]
        # kick out the additional frames
        invs = invs[:, ~np.isin(np.arange(t_eval_actual.size),idx_additional_frames)]
        # glue together the frames before and after breach of canister
        temp = np.zeros([invs.shape[0],ts.size])*u.mol
        temp[:,ts<t0_integration] = 0.0 * u.mol
        temp[:,ts>=t0_integration] = invs
        return temp


    def _jit_get_interpolated_rate_for_species(self, species, trange, n=10000, include_irf=True):
        """
        a faster version of get_interpolated_rate_for_species to be used in the external solvers.

        Parameters
        ----------
        species : iterable of str
            the desired species
        trange : astropy.units.Quantity
            array of length 2, specifying start and end of interpolation range in time
        n : int
            number of samples used for generating the interpolation. Default 10000
        include_irf : bool
            if true, include the IRF contribution. Default True.


        Returns
        -------
        finterp : function
            the jitted interpolating function

        """
        from numba import jit
        ts = np.linspace(trange[0], trange[1], n)
        # add frames at jumps in the source term
        for tbreak in [self.breaching_time, 1. / self.cladding_release_rate, 1. / self.matrix_release_rate]:
            # noinspection PyArgumentList
            if tbreak > ts.max() or tbreak in ts:
                continue
            for offset in [0., 1.]:
                idx = np.searchsorted(ts, tbreak + offset * u.d)
                ts = np.insert(ts, idx, tbreak + offset * u.d)

        rates = self.get_average_rate_over_timestep(ts, include_irf=include_irf)
        myrates = np.zeros([len(species), ts.size - 1])
        for i, s in enumerate(species):
            if s!="stable":
                myrates[i] = rates[self.nuclides.index(s)].to(u.mol / u.s).value
        tt = ts.to(u.s).value[:-1]
        ns = len(species)

        @jit(nopython=True)
        def interpolator(t):
            y = np.zeros(ns)
            for i in range(ns):
                y[i] = np.interp(t, tt, myrates[i])
            return y

        return interpolator


    def _jit_get_interpolator_integrated(self, trange, n, include_irf=True):
        """
        interpolates the intergrated rate via jit

        Parameters
        ----------
        trange : astropy.units.Quantity
            array of length 2, specifying start and end of interpolation range in time
        n : int
            number of samples used for generating the interpolation
        include_irf : bool
            if true, include the IRF contribution. Default True.

        Returns
        -------
        ip: function
            the interpolating function

        """
        ts = np.linspace(trange[0], trange[1], n)
        for tbreak in [self.breaching_time, 1. / self.cladding_release_rate, 1. / self.matrix_release_rate]:
            # noinspection PyArgumentList
            if tbreak > ts.max() or tbreak in ts:
                continue
            for offset in [-1, 0., 1.]:
                idx = np.searchsorted(ts, tbreak + offset * u.d)
                ts = np.insert(ts, idx, tbreak + offset * u.d)
        integrated = self.integrate_rate(ts, include_irf=include_irf)

        len(self.nuclides)
        myrates = np.zeros([len(self.nuclides), ts.size])
        for i, s in enumerate(self.nuclides):
            if s != "stable":
                myrates[i] = integrated[self.nuclides.index(s)].to(u.mol).value
        myrates *= u.mol
        tt = ts.to(u.yr)
        return Interpolator1D(xdata=tt, ydata=myrates)


    def get_output_source_term(self, trange, n):
        """
        calculate the cumulative release of radionuclides and return a BaseOutputSource object with it.
        Parameters
        ----------
        trange : astropy.units.Quantity
            array with length 2, indicating the start and the end of the time under consideration for the source term
        n : int
            the number of points in time, homogeneously distributed within trange, at which the cumulative rate will be
            calculated.

        Returns
        -------
        bst : BaseSourceTerm
            the source term

        """
        from .source import BaseSourceTerm
        ip = self._jit_get_interpolator_integrated(trange, n)
        ip_no_irf = self._jit_get_interpolator_integrated(trange, n, include_irf=False)
        # TODO last argument below is fragile.
        return BaseSourceTerm(nuclides=self.nuclides, interpolator=ip, interpolator_no_irf=ip_no_irf,
                             irf_time=self.breaching_time, irf_inventory=ip(self.breaching_time))

class CompositeReleaseModel:
    """
    A Release model containing contributions from several (possibly weighted) BaseReleaseModels. Useful for modelling
    the summed contributions from a number of different canister types. Bundles several release models into a single one


        Parameters
        ----------
        model_params : list of ReleaseModelParameters
            the model parameters of the individual release models
        names : list of str
            the names of the individual models
        weights : array-like
            the weight of each model
        ignore_decay : bool
            if true, ignore decay
        nuclide_database: str
            if "VSG", use the decay data for the VSG nuclide scheme. If "default", use radioactivedecay's standard,
             IRCP-107
        nuclides : str or list
            if "VSG", use the the VSG nuclides
    """
    def __init__(self, model_params=None, names=None, weights=None, ignore_decay=False, nuclide_database="VSG",
                 nuclides="VSG", storage_year=None):
        self.model_params = model_params
        self.names = names
        self.weights = weights
        self.ignore_decay = ignore_decay
        self.nuclide_database = nuclide_database
        if nuclides == "VSG":
            nuclides = transpyrend.utils.nuclides.get_species_VSG()
        self.nuclides = nuclides
        self.models = []
        for model_param in model_params:
            args = model_param.make_args()
            # override arguments in base release models to be the same everywhere.
            args["nuclides"] = self.nuclides
            args["ignore_decay"] = self.ignore_decay
            args["nuclide_database"] = self.nuclide_database
            bs = BaseReleaseModel(**args)
            self.models.append(bs)
        self.storage_year = storage_year


    def get_output_source_term(self, trange, n):
        #run the models
        from .source import CompositeSourceTerm
        source_terms = []
        for model in self.models:
            source_terms.append(model.get_output_source_term(trange, n))
        #combine them:
        #CompositeSourceTerm can do that for us
        return CompositeSourceTerm(source_terms, self.weights)

    def get_rate_at_time(self, t, include_irf=True):
        r = self.models[0].get_rate_at_time(t, include_irf=include_irf) * self.weights[0]
        for i, model in enumerate(self.models[1:]):
            r+= model.get_rate_at_time(t, include_irf=include_irf) * self.weights[1+i]
        return r

    def get_average_rate_over_timestep(self, ts, interpolator=None, **kwargs):
        r = self.models[0].get_average_rate_over_timestep(ts, interpolator, **kwargs) * self.weights[0]
        for i, _ in enumerate(self.models[1:]):
            r += self.models[0].get_average_rate_over_timestep(ts, interpolator, **kwargs) * self.weights[1+i]
        return r

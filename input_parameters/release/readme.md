# Documentation of the parameter file format for the release models
The parameter files used by the `ReleaseModelParameters` class are documented here.

The release models are parametrized by two files: a yaml file controlling the general behavior and the release rates, 
and a csv file referred to by the yaml file which sets the inventory and the distribution of the inventory onto the different
waste components

## Parameters of the BaseReleaseModel

### release_parameters.yaml

The following code block shows a documented example:

    #the first fields contain meta data that are only interpreted by humans
    #the name of the model
    model:  test1
    #date of creation
    date:   22.07.22
    #author
    author: name of the author
    #comment
    comment: a comment
    
    #the list of nuclides to consider; if not VSG, a list has to be given, 
    #i.e. 
    #nuclides: [I-129, Cl-36]
    nuclides: VSG
    
    #parameters steering the release from waste forms
    release_parameters:
       #time before the canisters break
       breaching_time: 1e4 yr
       #the release rate of radionuclides from the fuel matrix
       matrix_release_rate: 1e-6/yr
       #the release rate of radionuclides from the cladding
       cladding_release_rate: 1e-5/yr
       #the release rate of radionuclides from vitrified waste
       vitrified_lifetime: 1e5 yr

    #parameters for the inventory and the distribution of radionuclides onto the different waste types are given here
    nuclide_parameters:
        #specifies the csv file to read the parameters from
        from_file: nuclide_parameters.csv
     
    #options
    #if true, ignore decay inside the waste forms. Note that this does not affect the treatment of decay in the near/far field.
    ignore_decay: False
    #the nuclide database with the half-lifes, decay chains, ... to be used.
    nuclide_database: VSG

### nuclide_parameters.csv

The file is a semicolon-separated csv file with commas as decimal seoarator (German number format, e.g `1,76` for 1.76). 
The following columns must be defined:  

- `nuclide`: name of the nuclide
- `matrix_fraction`: the fraction of the nuclide that is present in the fuel matrix
- `cladding_fraction`: the fraction of the nuclide that is present in the cladding
- `vitrified_fraction`: the fraction of the nuclide that is present as vitrified waste
- `instant_release_fraction`: the fraction of the inventory to be release instantaneously
- `inventory`: the total amount of this radionuclide in the deposit, in mol

Note that matrix, cladding, and vitrified fraction sum up to one; the instant release fraction is considered separately.


## Parameters of the CompositeReleaseModel

the `CompositeReleaseModel` bundles two or more `BaseReleaseModel`. This can be useful if one wants to consider
different types of waste canister. 

### composite_release_model.yaml
The following shows an example of the parameter file.

    model:  composite_test
    date:   25.10.22
    author: name of author
    comment: this is a short test
    
    # global parameters for all release models considered in this composite.
    nuclides: VSG
    ignore_decay: False
    nuclide_database: VSG
    
    # a list of BaseReleaseModel parameter sets, providing the path to the relevant yaml file 
    release_models:
        - model1: release_parameters.yaml
        - model2: release_parameters.yaml
    
    # the weight of each release model
    weights:
        - 0.5
        - 0.5

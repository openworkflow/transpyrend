# Documentation of the parameter file format

The parameter files used by the `ParametersSets` and `TranspyrendParameters` classes are documented here. Conceptually, the
parameter files should specify a complete model, however, there are meta parameters like the employed solver method that
are not (yet) specified by parameter files.

## Overview

(At most) four files specify a model.

- `model_parameters` are global parameters like the total time interval to simulate, the orientation of the model or the extent of the deposit.
- `geo_profile` specifies the geological layers considered by the model and their ordering, and their 
  global parameters like the extent of each layer, hydraulic permeability, and porosity. It also defines their names and
  links each unit to a (potentially different) set of diffusion/sorption parameters.
- `nuclide_diffusion_parameters` defines the effective diffusion coefficients for some elements.
- `nuclide_sorption_parameters` defines the sorption coefficients for some elements. In case sorption is not relevant,
  this file can be ommitted.


## General format

The model_parameters come as yaml file. The other files are currently CSV, with commas for decimal, and semicolon as
separator.

## The files

This section details the format of the individual parameter files.

### model_parameters.yaml

This is the master file. It is written in yaml, and may contain the following parameters:

#### Mandatory parameters

- `dx`: the width of a cell in m
- `width_repository`: width of the repository in m
- `height_repository`: height of the repository in m
- `location_repository`: the position (in the model) in m at which the repository begins in model coordinates (from left,
  i.e. the left corner of the repository)
- `diffusion_parameters`: contains a single key-value pair, "from_file:<file>", pointing to the csv file specifying the
  diffusion parameters, see below.
- `sorption_parameters`: contains a single key-value pair, "from_file:<file>", pointing to the csv file specifying the
  sorption parameters, see below.
- `geological_layers`: contains a single key-value pair, "from_file:<file>", pointing to the csv file specifying the
  geological layers, see below.
- `model_orientation`: orientation of the model with respect to the orientation of the repository: either `horizontal` or `vertical`

#### Optional parameters

- `time_interval`: the total duration of the simulation in yr. Default 10^6 yr
- `area_repository`: total area of the repository in m^2. Default 1 m^2.
- `mass_transport_threshold`: the mass threshold to be applied in processing the results, as a fraction of the initial
  mass. Default 10^-4.
- `cfl_limit`: the CFL limit. Default 0.25.
- `hl_limit`: limits the timestep to dt < hl_limit * tau, where tau is the shortest half life in each submodel. Default 0.5.
- `dt_max`: if set > 0, forces the timestep to be smaller than this value (in years). Ignored otherwise.
- `output_interval`: the interval between output times in yr. Default 1000 yr.
- `snap_to_units`: true if the nodes should be aligned thus there is a node on each unit-to-unit boundary. If false,
  the user needs to make sure that the units have thicknesses that can be divided by dx. Default true.
- `release_model`: the relative path to the parameter file of the chosen release model. None if no release modle is used.
- `include_decay`: true if radioactive decay should be included. Default true.
- `use_bateman`: true if operator splitting using the Bateman solution should be used. Default true.
- `store_flux`: true if fluxes should be stored in each time step. Not available for DOP853. Default false.
- `storage_year`: the year in which the storage begins. Used for evolving the inventory prior to the transport simulation.
### geo_profile.csv

The file defines the geological units for each profile and their global properties. The first line of the file contains
the headers and should never be modified. The second line contains information on units that is only for human use, i.e.
that is ignored when processing the files. Each consecutive line defines an individual geological unit. Each unit is assigned to a
profile according to the first column (profile_name). The order of units in the file determines the order of the units
in the model, starting with deepest one up going upwards. Each units links to a set of diffusion and sorption coefficients (
if present) defined in the respective files (described further below).

The columns are:

- `geological_unit_name`: the full name of this unit (e.g. for labels and legends).
- `geological_unit_code`: a short name for the unit.
- `thickness`: the thickness of this unit in m.
- `nuclide_param_key_diffusion`: a key specifying the set of diffusion coefficients for this unit.
- `nuclide_param_key_sorption`: a key specifying a set of sorption coefficients for this unit. Can be set to "\_none\_" to
  specify no sorption in this unit.
- `porosity`: the porosity of the unit.
- `bulk_density`: the bulk density of the unit in kg/kg^3.
- `log_kf`: the (base 10) logarithm of the hydraulic conductivity of the unit in m/s.

### nuclide_diffusion_parameters.csv

The file defines sets of diffusion parameters for different geologies and chemical elements. The first line of the file contains
the headers and should never be modified. The second line contains information on units that is only for human use, i.e.
that is ignored in processing the files. Each consecutive line defines the effective diffusion coefficient for an element
in a certain set. The membership to a certain set is specified by the column `nuclide_param_key`. Note that at the moment,
the diffusion coefficient is thought to be a property of chemical elements, where it is in fact a property of a specific chemical species.
Also note that our definition of the effective diffusion coefficient includes the porosity.

The columns are:

- `nuclide`: unused right now
- `element`: the chemical symbol for the element
- `nuclide_param_key`: the set to which this value belongs
- `De`: the effective diffusion coefficient in m^2/s

### nuclide_sorption_parameters.csv

The file defines sets of sorption parameters for different geologies and chemical elements. The first line of the file contains
the headers and should never be modified. The second line contains information on units that is only for human use, i.e.
that is ignored in processing the files. Each following line defines the sorption coefficient for an element in a
certain set. Note that at the moment, the sorption coefficient is thought to be a property of chemical elements, where it is in
fact a property of a specific chemical species. The membership to a certain set is specified by the column `nuclide_param_key`.

The columns are:

- `nuclide`: unused right now
- `element`: the chemical symbol for the element
- `nuclide_param_key`: the set to which this value belongs
- `Kd`: the sorption coefficient in m^3/kg

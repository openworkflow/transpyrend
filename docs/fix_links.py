"""
replaces relative links in the markdown files imported from the root directory.
"""

import sys
import re
import os
def replace_relative_links(fn, root):
    """
    fix the pathes for included links with respect to the doc home
    """
    with open(fn, encoding='utf-8', errors='ignore') as f:
        lines = f.readlines()

    for i, line in enumerate(lines):
        m=re.findall(r"(<span class=\"xref myst\">(.*?)<\/span>)",lines[i])
        if m != []:
            for match in m:
                original_target = match[1]
                original_link = match[0]
                new_target = os.path.join(root + original_target)
                new_link = "<a class=\"reference external\" href=\""+new_target+"\">"+original_target+"</a>"
                lines[i] = re.sub(original_link,new_link,lines[i])

    with open(fn, "w", encoding='utf-8', errors='ignore') as f:
        for line in lines:
            f.write(line)

if __name__ == '__main__':
    print(f"fixing relative links in {sys.argv[1]}")
    replace_relative_links(sys.argv[1], sys.argv[2])
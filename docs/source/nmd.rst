A longer introduction
=====================
TransPyREnd is short for "*Trans*\portmodell in *Py*\thon für den Austrag von *R*\adionukliden aus einem *End*\lager". This code is being used to
estimate radionuclide transport in the subsurface as part of the safety
assessments that are currently on the way in the site selection process for a
nuclear waste repository in Germany. The code is accessible at
https://gitlab.opengeosys.org/owf/transpyrend.

The motivation for developing TransPyREnd was the desire to use an
open-source code, which is beneficial for a transparent workflow in the
safety assessments of proposed nuclear waste repository sites. In addition, in
the current stage of the site selection in Germany there is a need for a
relatively simple and fast code. The code that is presented here
simulates transport in one dimension only and does not contain any
overhead such as a graphical user interface. This means that its increased 
numerical performance allows for a large number of model
experiments. This is helpful for the evaluation of a large number of
potential repositories and to quantify the effects of uncertainty of model parameters and
geological developments over the lifetime of the repository. The code
was written in Python, which enables a modular object oriented structure
and allows the code to benefit from the large number of Python libraries
for input and output, visualization, multi-processing and statistics.

in the following, the basic equations that represent the main
transport mechanisms of radionuclides in the subsurface, diffusion,
advection and sorption, as well as radioactive decay (section
`2 <#sec:equations>`__) are described. Detail of the implementation can be found in Behrens et al. 2023.

.. _sec:equations:

Mathematical representation of transport processes
--------------------------------------------------

The transport of radionuclides in the subsurface can be described by the
following equation:

.. math::
   :label: eq1

   %\underbrace{ R_i}_
        %{\text{Retardation}}
       \underbrace{ \phi  R_i \frac{\partial c_i}{\partial t} }_
       {\substack{\text{Concentration}\\\text{change}}}
     {=}
     \underbrace{
       -\frac{\partial j_i}{\partial x}}_
         {\text{Diffusion}}
     {-}
     \underbrace{  \frac{\partial (q c_i)}{\partial x}}_
       {\substack{\text{Advection}}}
       {+}
       \underbrace{ W}_
       {\text{Decay}}



where :math:`\phi` is porosity (dimensionless), :math:`R_i` is the
retardation factor of nuclide :math:`i` (dimensionless), :math:`c_i` is
the concentration of nuclide :math:`i` in the pore fluid
(:math:`mol \; m^{-3}`), :math:`t` is time (:math:`s`), :math:`j_i` is the
diffusive flux of radionuclide :math:`i`
(:math:`mol\; m^{-2} \; s^{-1}`) and :math:`q` is the Darcy velocity
(:math:`m \; s^{-1}`).

The term :math:`W` represents the source/sink term, which is defined as:

.. math:: W = \sum_{j} R_j c_j \lambda_{j,i} - R_i c_i \Lambda_i + S

The source/sink term describes the change rate of nuclide :math:`i` from
decays of other species :math:`j` (first term), and from loss of nuclide
:math:`i` due to decay (second term). :math:`\lambda_{j,i}` is the
matrix of decay constants for the decay of species :math:`j` to species
:math:`i`. :math:`\Lambda_i` is the total decay rate of species
:math:`i` (note the flipped indices):

.. math:: \Lambda_i = \sum_j \lambda_{i,j}

:math:`S` represents an additional source or sink
(:math:`kg\;m^{-3}\;s^{-1}`) that can for instance represent the release
of a nuclide over time.

The diffusive flux of radionuclide :math:`i` is defined as:

.. math::

   j_i = -D_{e,i} \dfrac{\partial c_i}{\partial x}

where :math:`D_{e,i}` is the effective diffusion coefficient of nuclide
:math:`i` (:math:`m^2 \; s^{-1}`). Combining this with equation :math:numref:`eq1`, we arrive at:

.. math::
   :label: fulleq

   %\underbrace{ R_i}_
        %{\text{Retardation}}
       \underbrace{ \phi  R_i \frac{\partial c_i}{\partial t} }_
       {\substack{\text{Concentration}\\\text{change}}}
     {=}
     \underbrace{
      \frac{\partial}{\partial x}  \left(D_{e,i} \frac{\partial c_i}{\partial x} \right)}_
         {\text{Diffusion}}
     {-}
     \underbrace{ \frac{\partial (q c_i)}{\partial x}}_
       {\substack{\text{Advection}}}
       {+}
       \underbrace{ W }_
       {\text{Decay}}

This equation represents the physical processes diffusion, advection and
the decay of radionuclides. The left-hand side of this equation and the
diffusion and advection terms follow standard solute transport
formulation (Diersch and Kolditz 2002; Ingebritsen, Sanford, and Neuzil
2006). The full equation, including the decay terms, is similar to the
mathematical formulations in other model codes that are frequently used
to simulate radioactive transport, such as TOUGH2 (Oldenburg and Pruess
1995) and PFLOTRAN (Hammond et al. 2012).

The sorption of nuclides to the solid rock matrix is described by the
retardation factor :math:`R_i`, which is defined as:

.. math:: R_i = 1 + \frac{K_{d,i}\rho}{\phi}

where :math:`K_{d,i}` the sorption coefficient (:math:`m^3 \; kg^{-1}`)
and :math:`\rho` is the bulk density (:math:`kg \; m^{-3}`). If we
divide equation :math:numref:`fulleq` by
:math:`R_i`, we can easily see the effective transport is indeed slowed
down by a factor of 1/:math:`R_i`, hence the name.

Numerical implementation
------------------------
TransPyREnd solves the transport equations with a finite-difference approach.
Time evolution is done using the second-order accurate Crank-Nicolson scheme.
Additionally, higher-order schemes from the scipy library can be used, e.g. DOP853,
an 8th-order accurate Runge-Kutta scheme. In principle, the semi-implicit
Crank-Nicolson method has a higher degree of numerical stability at the cost of
numerical accuracy due to numerical dispersion. The explicit method is a
higher-order method, which ensures a relatively high degree of
accuracy. However this method has a lower degree of numerical stability,
i.e. it is only stable at relatively small timesteps, which makes this
method computationally more demanding.

The implicit method was based on the existing model code PyBasin
(Luijendijk et al. 2011; Luijendijk 2012, 2020) with modifications to
the advection component and the addition of sorption and decay.


.. _sec:implicit:

Crank-Nicolson method
~~~~~~~~~~~~~~~~~~~~~

We first derive a discretized form of the transport equation :math:numref:`fulleq`. Our domain consists of point :math:`x_k`, with the index
index :math:`_k` and distance :math:`h_k = x_k - x_{k-1}` between the nodes.
The diffusive flux can be discretized as

.. math::

   \frac{\partial}{\partial x} D_{e,i} \frac{\partial c}{\partial x} \approx \frac{2}{h_{k+1}+h_{k}}\left( \frac{1}{h_{k}}D_{e,i,k-\frac{1}{2}} c_{i,k-1} - \frac{h_{k+1}+h_{k}}{h_{k}h_{k+1}} (D_{e,i,k-\frac{1}{2}}+D_{e,i,k+\frac{1}{2}}) c_{i,k} + \frac{1}{h_{k+1}} D_{e,i,k+\frac{1}{2}} c_{i,k+1}   \right)

where subscript i denotes radionuclide i, subscript :math:`_k` denotes
the grid node for which the equation is evaluated, :math:`k-1` denotes
the node one position to the left and :math:`k+1` denotes the node one
position to the right, and an index :math:`k\pm \frac{1}{2}` denotes the interface between the respective nodes.

Similarly, the advective flux reads

.. math::
   q \frac{\partial c}{\partial x} \approx q \frac{c_{i,k+1} - c_{i,k-1}}{x_{k+1} - x_{k-1}}

The Crank-Nicolson scheme can be written as

.. math::
   \frac{y^{m+1}-y^m}{\Delta t}  = \frac{1}{2} F^{m+1} + \frac{1}{2} F^{m}

with :math:`F` an appropriate discretization of a given differential equation

.. math::
   \frac{\partial y}{\partial t}  = f(x, y, t)

and :math:`m` the index of the considered time step.

The PDE in equation :math:numref:`fulleq` can therefore be approximated as

.. math::
   R_{i, k} \phi_{i, k} \frac{c_{i,k}^{m+1}-c_{i,k}^m}{\Delta t}  &= \theta H_k \left( H_k^- D_{e,i,k-\frac{1}{2}} c_{i,k-1}^{m+1} - G_k (D_{e,i,k-\frac{1}{2}}+D_{e,i,k+\frac{1}{2}}) c_{i,k}^{m+1} + H_k^+ D_{e,i,k+\frac{1}{2}} c_{i,k+1}^{m+1} \right)  \\\\
   &- \theta q \frac{c_{i,k+1}^{m+1} - c_{i,k-1}^{m+1}}{x_{k+1} - x_{k-1}} \\\\
   &+ (1-\theta) H_k \left(H_k^- D_{e,i,k-\frac{1}{2}} c_{i,k-1}^{m} - G_k (D_{e,i,k-\frac{1}{2}}+D_{e,i,k+\frac{1}{2}}) c_{i,k}^{m} + H_k^+ D_{e,i,k+\frac{1}{2}} c_{i,k+1}^{m} \right) \\\\
   &- (1-\theta) q \frac{c_{i,k+1}^{m} - c_{i,k-1}^{m}}{x_{k+1} - x_{k-1}} \\\\
   &+ \sum_j \left(R_{j, k} \phi_{j, k} c^m_{j,k} \lambda_{i,j}\right) - R_{i, k}  \phi_{i, k} c^m_{i,k} \Lambda_i + W^m_{i,k}

with the following definitions

.. math::
   H_k &= \frac{2}{h_{k+1}+h_{k}}\\\\
   H_k^- &= \frac{1}{h_{k}}\\\\
   H_k^+ &= \frac{1}{h_{k+1}}\\\\
   G_k &= \frac{h_{k+1}+h_{k}}{h_{k}h_{k+1}}

and

.. math::
   s &= \dfrac{\Delta t}{\phi_{i, k} R_{i, k}}\\\\
   u_k &= \dfrac{qs}{x_{k+1} - x_{k-1}} \\\\
   v_{k}^{-} &= H_k H_k^- D_{e,i,k-1/2} s\\\\
   v_k &= H_k G_k (D_{e,i,{k-1/2}} + D_{e,i,{k+1/2}}) s\\\\
   v_{k}^{+} &= H_k H_k^+ D_{e,i,k+1/2}s

We can simplify the equation to

.. math::
   &\theta\left( - u_k - v_{k}^{-} \right) c_{i,k-1}^{m+1} + \left( 1 + \theta v_k \right) c_{i,k}^{m+1}  + \theta\left( u_k - v_{k}^{+} \right) c_{i,k+1}^{m+1} =\nonumber\\\\
   &\theta'\left( u_k + v_{k}^{-} \right) c_{i,k-1}^{m} + \left( 1 - \theta' v_{k} \right) c_{i,k}^{m}  + \theta'\left( -u_k + v_{k}^{+} \right) c_{i,k+1}^{m} + s \left( \sum_j \left(R_j \phi_i c^m_{j,k} \lambda_{i,j}\right) - R_i  \phi_i c^m_{i,k} \Lambda_i + W^m_{i,k}\right)

This linear system of equations can be solved by standard methods; TransPyREnd uses a sparse matrix solver from scipy.



References
----------

.. container:: references csl-bib-body hanging-indent
   :name: refs

   .. container:: csl-entry
      :name: ref-Baeyens2014

      Baeyens, B, T Thoenen, MH Bradbury, and M Marques Fernandes. 2014.
      “Sorption Data Bases for Argillaceous Rocks and Bentonite for the
      Provisional Safety Analyses for SGT-E2.” Paul Scherrer Institute
      (PSI).

   .. container:: csl-entry
      :name: ref-Bateman1910

      Bateman, Henry. 1910. “The Solution of a System of Differential
      Equations Occurring in the Theory of Radioactive Transformations.”
      In *Proc. Cambridge Philos. Soc.*, 15:423–27.

   .. container:: csl-entry
      :name: ref-Behrens2023

     Behrens, C. and Luijendijk, E. and Kreye, P. and Panitz, F. and Bjorge, M. and Gelleszun, M. and Renz, A. and Miro, S. and Rühaak, W.. 2023. "TransPyREnd: a code for modelling the transport of radionuclides on geological timescales" *Advances in Geosciences* 58 (109-119)

   .. container:: csl-entry
      :name: ref-Diersch2002

      Diersch, H. Jg, and Olaf Kolditz. 2002. “Variable-Density Flow and
      Transport in Porous Media: Approaches and Challenges.” *Advances
      in Water Resources* 25 (8-12): 899–944.

   .. container:: csl-entry
      :name: ref-Hammond2012

      Hammond, G. E., P. C. Lichtner, Chuan Lu, and Richard T. Mills.
      2012. “PFLOTRAN: Reactive Flow & Transport Code for Use on Laptops
      to Leadership-Class Supercomputers.” *Groundwater Reactive
      Transport Models* 5: 141–59.

   .. container:: csl-entry
      :name: ref-Huysmans2007

      Huysmans, Marijke, and Alain Dassargues. 2007. “Equivalent
      Diffusion Coefficient and Equivalent Diffusion Accessible Porosity
      of a Stratified Porous Medium.” *Transport in Porous Media* 66
      (3): 421–38. https://doi.org/10.1007/s11242-006-0028-6.

   .. container:: csl-entry
      :name: ref-Ingebritsen2006

      Ingebritsen, Steven E, Ward E Sanford, and Christopher E Neuzil.
      2006. *Groundwater in Geologic Processes*. Cambridge University
      Press.

   .. container:: csl-entry
      :name: ref-Larue2013

      Larue, Jürgen, Bruno Baltes, Heidermarie Fischer, Gerd Frieling,
      Ingo Kock, Martı́n Navarro, and Holger Seher. 2013. “Radiologische
      Konsequenzenanalyse.” Gesellschaft für Anlagen-und
      Reaktorsicherheit.

   .. container:: csl-entry
      :name: ref-Li2005

      Li, Xiaoye S. 2005. “An Overview of SuperLU: Algorithms,
      Implementation, and User Interface.” *ACM Trans. Math. Softw.* 31
      (3): 302–25. https://doi.org/10.1145/1089014.1089017.

   .. container:: csl-entry
      :name: ref-Luijendijk2012

      Luijendijk, Elco. 2012. “The role of fluid flow in the thermal
      history of sedimentary basins: Inferences from thermochronology
      and numerical modeling in the Roer Valley Graben, southern
      Netherlands [Ph.D. thesis].” PhD, Vrije Universiteit Amsterdam.

   .. container:: csl-entry
      :name: ref-Luijendijk2020

      Luijendijk, Elco. 2020. *PyBasin: Numerical model of basin history, heat flow
      and thermochronology* (version v0.91-alpha). Zenodo.
      https://doi.org/10.5281/zenodo.4263427.

   .. container:: csl-entry
      :name: ref-Luijendijk2011b

      Luijendijk, Elco, R. T. Van Balen, Marlies Ter Voorde, and P. A.
      M. Andriessen. 2011. “Reconstructing the Late Cretaceous inversion
      of the Roer Valley Graben (southern Netherlands) using a new model
      that integrates burial and provenance history with fission track
      thermochronology.” *Journal of Geophysical Research* 116 (B06402):
      1–19. https://doi.org/10.1029/2010JB008071.

   .. container:: csl-entry
      :name: ref-Navarro2019

      Navarro, Martin, Torben Weyand, Jens Eckel, and Heidemarie
      Fischer. 2019. “Indikatoren Zur Bewertung Des Einschlusses Und Der
      Isolation Mit Exemplarischer Anwendung Auf Ein Generisches
      Endlagersystem Mit Dem Wirtsgestein Tongestein.” Gesellschaft für
      Anlagen und Reaktorsicherheit (GRS) gGmbH.

   .. container:: csl-entry
      :name: ref-Norsett1987

      Norsett, E Hairer SP, and G Wanner. 1987. “Solving Ordinary
      Differential Equations i: Nonstiff Problems.” Springer-Verlag.

   .. container:: csl-entry
      :name: ref-Oldenburg1995

      Oldenburg, Curtis M, and Karsten Pruess. 1995. “Eos7r:
      Radionuclide Transport for Tough2.” Lawrence Berkeley National
      Lab.(LBNL), Berkeley, CA (United States).

   .. container:: csl-entry
      :name: ref-VanLoon2014

      Van Loon, L. R. 2014. “Effective Diffusion Coefficients and
      Porosity Values for Argillaceous Rocks and Bentonite: Measured and
      Estimated Values for the Provisional Safety Analyses for SGT-E2.”
      \*Report 1015-2636.
      http://inis.iaea.org/search/search.aspx?orig_q=RN:48088310.

   .. container:: csl-entry
      :name: ref-2020SciPy-NMeth

      Virtanen, Pauli, Ralf Gommers, Travis E. Oliphant, Matt Haberland,
      Tyler Reddy, David Cournapeau, Evgeni Burovski, et al. 2020.
      “SciPy 1.0: Fundamental Algorithms for Scientific Computing in
      Python.” *Nature Methods* 17: 261–72.
      https://doi.org/10.1038/s41592-019-0686-2.

"""
replaces relative links in the markdown files imported from the root directory.
"""

import sys
import re
import os
def replace_relative_links(fn, root):
    """
    fix the pathes for relative images with respect to the doc home
    """
    with open(fn, encoding='utf-8', errors='ignore') as f:
        lines = f.readlines()

    for i, line in enumerate(lines):

        m = re.findall(r"<p><img alt=\".*?\" src=\"(.*?)\"", lines[i])

        if m != []:
            for match in m:
                original_target = match
                new_target = root + match
                lines[i] = re.sub(original_target, new_target, lines[i])

    with open(fn, "w", encoding='utf-8', errors='ignore') as f:
        for line in lines:
            f.write(line)

if __name__ == '__main__':
    print(f"fixing images in {sys.argv[1]}")
    replace_relative_links(sys.argv[1], sys.argv[2])
"""
some statistics functions
"""
import numpy as np


def RMSE(y, true):
    """
    returns the RMSE of y wrt true

    Parameters
    ----------
    y : array-like
        the data
    true : array-like
        the "true" data

    Returns
    -------
    rmse : float
        the RMSE

    """
    return np.sqrt(((y - true) ** 2).sum()) / y.size

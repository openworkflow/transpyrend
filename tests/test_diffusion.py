"""
implements some tests of the diffusion process

"""

import unittest
import numpy as np
from transpyrend.core.externalsolver import DOP853TransportModel1D
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D

import astropy.units as u

_atol = 1e-4
_rtol = 1e-2


def compare_diffusion(N, N0, dx, D, t_eval, norm):
    """
    get the analytic solution for diffusion of a delta peak

    Parameters
    ----------
    N : int
        number of cells
    N0 : int
        location (index) of delta peak
    dx  : float
        cell size
    D  : float
        diffusion coefficient
    t_eval : array-like
        times at which to evaluate the solution

    Returns
    -------
    res : array_like
        an (N, M)-shaped array, with N the number of grid points and M the number of times 
        at which the concentrations were evaluated

    """
    import scipy.stats as stats
    nd = stats.norm.pdf  # (x, mu, sigma)
    x = range(N) * dx
    mu = N0 * dx
    res = np.zeros((x.size, t_eval.size))
    for i, t in enumerate(t_eval):
        sigma = np.sqrt(2 * D.value * t)
        res[:, i] = nd(x, mu, sigma) * norm * dx
    return res


def do_test_diffusion(modeltype, nuclide, D):
    """
    Run the diffusion test for a given modeltype

    Parameters
    ----------
    modeltype : BaseTransportModel1D
        the type of model concerned
    nuclide : str
        the nuclide considered
    D : astropy.units.Quantity
        the diffusion coefficient for said nuclide

    Returns
    -------
    True on success

    """
    dx = 10 * u.m
    xmax = 2000. * u.m
    porosity = 0.07
    depo_index = int(xmax / dx / 2)
    parameters_rock = [{"porosity": porosity,
                        "head_gradient": -1,
                        "hydraulic_conductivity": 0 * u.m / u.s,
                        "bulk_density": 2350. * u.kg / u.m ** 3,
                        "thickness": xmax
                        }]
    params0 = {"effective_diffusion_coefficient": D}
    parameters_transport_material = [{nuclide: params0}]


    model_exp = modeltype([nuclide], parameters_rock, parameters_transport_material,
                          geometry={"dx": dx}, time_interval=1e6 * u.yr,
                          options={"nuclid_database": "vsg", "include_sorption": False,
                                   "external_solver_method": "DOP853", "include_decay": False})
    ics = np.zeros((model_exp.N, 1)) * u.mol / u.m ** 3
    ics[depo_index] = 1.0 * u.mol / u.m ** 3
    t_eval = model_exp.default_t_eval()
    model_exp.run(ics=ics, t_eval=t_eval)
    truesol = compare_diffusion(model_exp.N, depo_index, model_exp.dxs[1], D / porosity, model_exp.results["t"], 1.)
    return model_exp, truesol


class MyTestCase(unittest.TestCase):
    def test_decay_explicit(self):
        nuclide = "I-129"
        exclude = 10
        model_exp, truesol = do_test_diffusion(DOP853TransportModel1D, nuclide, 1e-10 * u.m ** 2 / u.s)
        i = -1
        x, mysol = model_exp.get_concentrations_x(nuclide, i, per_rock_volume=True, quantity="molar density")
        # import matplotlib.pyplot as plt
        # plt.plot(x, mysol, label="DOP853")
        # plt.plot(x, truesol[:,i], label="true solution",ls=":")
        # plt.yscale("log")
        # plt.legend()
        # plt.show()
        if (not np.allclose(mysol.value[exclude:-exclude],
                            truesol[:, i][exclude:-exclude], rtol=_rtol, atol=_atol)):
            np.testing.assert_allclose(mysol.value[exclude:-exclude],
                                       truesol[:, i][exclude:-exclude], rtol=_rtol, atol=_atol)

    def test_decay_implicit(self):
        nuclide = "I-129"
        exclude = 10
        model_exp, truesol = do_test_diffusion(EulerBackwardsModel1D, "I-129", 1e-10 * u.m ** 2 / u.s)
        i = -1
        x, mysol = model_exp.get_concentrations_x(nuclide, i, per_rock_volume=True, quantity="molar density")
        # import matplotlib.pyplot as plt
        # plt.plot(x, mysol, label="implicit")
        # plt.plot(x, truesol[:, i], label="true solution", ls=":")
        # plt.yscale("log")
        # plt.legend()
        # plt.show()
        if (not np.allclose(mysol.value[exclude:-exclude],
                            truesol[:, i][exclude:-exclude], rtol=_rtol, atol=_atol)):
            np.testing.assert_allclose(mysol.value[exclude:-exclude],
                                       truesol[:, i][exclude:-exclude], rtol=_rtol, atol=_atol)


if __name__ == '__main__':
    unittest.main()

"""
implements some tests for the the nuclide-related utilities
"""
import unittest

import numpy as np
import astropy.units as u

from transpyrend.utils.nuclides import evolve_to_age, VSGInventoryReader


class NuclideTests(unittest.TestCase):
    def test_evolve_to_age(self):
        inv = {"I-129": 1.0 * u.mol}
        inv2 = evolve_to_age(inv, 1.57e7 * u.yr, quantity="amount")
        np.testing.assert_allclose(inv2["I-129"].value, 0.5, rtol=1e-4)

    def test_vsg_reader(self):
        r = VSGInventoryReader()
        tot = r.get_haw_total_amount()
        np.testing.assert_allclose(tot.value, 45673956.99286024, rtol=1e-8)


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)
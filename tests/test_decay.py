"""
implements some tests of the decay processes

"""

import unittest
import logging

import astropy.units as u
import numpy as np
from parameterized import parameterized
from stats import RMSE
from transpyrend.core.cranknicolson import CrankNicolsonTransportModel1D
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D
from transpyrend.core.externalsolver import DOP853TransportModel1D
from transpyrend.utils.nuclides import vsg_tab411_actinium_chain, vsg_tab411_actinium_chain_bateman_setup
from transpyrend.utils.nuclides import vsg_tab411_neptunium_chain, vsg_tab411_neptunium_chain_bateman_setup
from transpyrend.utils.nuclides import vsg_tab411_uranium_chain, vsg_tab411_uranium_chain_bateman_setup

actininum_chain = [vsg_tab411_actinium_chain, vsg_tab411_actinium_chain_bateman_setup["chains"],
                   vsg_tab411_actinium_chain_bateman_setup["states"]]
neptunium_chain = [vsg_tab411_neptunium_chain, vsg_tab411_neptunium_chain_bateman_setup["chains"],
                   vsg_tab411_neptunium_chain_bateman_setup["states"]]
uranium_chain = [vsg_tab411_uranium_chain, vsg_tab411_uranium_chain_bateman_setup["chains"],
                 vsg_tab411_uranium_chain_bateman_setup["states"]]


def compare_decay(nuclide, t_eval):
    """
    compute the mass fraction left of a number of nuclides, using the radioactive_decay package. Will use the VSG data
    in order to have consistent datasets.

    Parameters
    ----------
    nuclides : list
        list of nuclid names
    t : array-like
        time at which to evaluate the inventory

    Returns
    -------
    list of mass fractions

    """

    import radioactivedecay as rd
    from transpyrend.utils.nuclides import VSGDECAYDATA
    d = {}
    for nuc in nuclide:
        if nuc == "stable":
            continue
        else:
            d[nuc] = 1.
    inv = rd.Inventory(d, units="mol", decay_data=VSGDECAYDATA)
    res = np.zeros([len(nuclide) - 1, t_eval.size])
    for i, t in enumerate(t_eval.to(u.s).value):
        inv2 = inv.decay(t)
        j = 0
        for nuc in nuclide:
            if nuc == "stable":
                continue
            if nuc in inv2.contents:
                out = (inv2.moles()[nuc])
            else:
                out = (0.0)
            res[j, i] = out
            j += 1
    return res


def compare_chain(nuclides, t_eval):
    """
    compute the mass fraction left of a number of nuclides, using the radioactive_decay package. Will use the VSG data
    in order to have consistent datasets.

    Parameters
    ----------
    nuclides :list
        list of nuclid names
    t  : array-like
        time at which to evaluate the inventory

    Returns
    -------
    list of mass fractions

    """

    import radioactivedecay as rd
    from transpyrend.utils.nuclides import VSGDECAYDATA
    d = {}
    for nuclide in nuclides:
        if nuclide == "stable":
            continue
        else:
            d[nuclide] = 1.
    inv = rd.Inventory(d, units="mol", decay_data=VSGDECAYDATA)
    res = np.zeros((t_eval.size, len(nuclides)))
    for i, t in enumerate(t_eval.to(u.s).value):
        inv2 = inv.decay(t)
        out = []
        for nuc in nuclides:
            if nuc in inv2.contents:
                out.append(inv2.moles()[nuc])
            else:
                out.append(0.0)
        res[i, :] = out
    return res


def do_test_decay_chain(modeltype, chain, chain_intel, num_states, use_bateman):
    """
    Run the decay test for a given modeltype (either ExplicitTransportModel1D or ImplicitTransportModel1D)

    Parameters
    ----------
    modeltype : BaseTransportModel1D
        the type of model concerned
    chain : list
        the decay chain considered

    Returns
    -------
    model_exp : BaseTransportModel1D
        the processed model
    truesol : array-like
        the true solution

    """

    dx = 1.0 * u.m
    xmax = 10. * u.m
    parameters_rock = [{"porosity": 0.05,
                        "head_gradient": 0.,
                        "hydraulic_conductivity": 0. * u.m / u.s,
                        "bulk_density": 2350. * u.kg / u.m ** 3,
                        "thickness": xmax
                        }]
    chain = chain.copy()
    num_states = num_states.copy()
    chain = [*chain, "stable"]
    new_chains = []
    for j, subchain in enumerate(chain_intel):
        new_chain = [*subchain, "stable"]
        new_chains.append(new_chain)
        if num_states[j] == -1:
            if j > 0:
                num_states[j] = len(new_chain) - 1
    chain_intel = new_chains
    parameters_transport_material = {}
    kd = np.logspace(-3, 0, len(chain) - 1)
    for i, nuclide in enumerate(chain):
        parameters_transport_material[nuclide] = {"effective_diffusion_coefficient": 0 * u.m ** 2 / u.s}
        if nuclide == "stable":
            parameters_transport_material[nuclide]["sorption_coefficient"] = 0. * u.m ** 3 / u.kg
        else:
            parameters_transport_material[nuclide]["sorption_coefficient"] = kd[i] * u.m ** 3 / u.kg

    parameters_transport_material = [parameters_transport_material]
    np.zeros(len(chain)) * u.kg / u.m ** 3

    model_exp = modeltype(chain, parameters_rock, parameters_transport_material,
                          options={"include_decay": True, "nuclid_database": "vsg", "external_solver_method": "DOP853"},
                          geometry={"dx": dx},
                          time_interval=1e6 * u.yr)
    dt = model_exp.get_max_timestep()
    if use_bateman:
        dt = 100 * u.yr

    ics = np.zeros((model_exp.N, model_exp.nspecies)) * u.mol / u.m ** 3
    ics[1:-1, :-1] = 1.0 * u.mol / u.m ** 3
    if use_bateman:
        model_exp.setup_bateman(chain_intel, num_states)
    model_exp.run(ics=ics, dt_max=dt, t_eval=model_exp.default_t_eval())
    truesol = compare_chain(chain, model_exp.results["t"])
    return model_exp, truesol


def do_test_decay(modeltype, nuclides, use_bateman):
    """
    Run the decay test for a given modeltype (either ExplicitTransportModel1D or ImplicitTransportModel1D)

    Parameters
    ----------
    modeltype : BaseTransportModel1D
        the type of model concerned

    Returns
    -------
    model_exp : BaseTransportModel1D
        the processed model
    truesol : array-like
        the true solution

    """

    dx = 1.0 * u.m
    xmax = 10. * u.m

    if isinstance(nuclides, str):
        nuclides = [nuclides]
    else:
        nuclides = nuclides.copy()
    nuclides.append("stable")

    parameters_rock = [{"porosity": 0.05,
                        "head_gradient": 0.,
                        "hydraulic_conductivity": 0. * u.m / u.s,
                        "bulk_density": 2350. * u.kg / u.m ** 3,
                        "thickness": xmax
                        }]
    parameters_transport_material = [{}]
    for i, nuc in enumerate(nuclides):
        parameters_transport_material[0][nuc] = {"effective_diffusion_coefficient": 0 * u.m ** 2 / u.s}
        if nuc == "stable":
            parameters_transport_material[0][nuc]["sorption_coefficient"] = 0. * u.m ** 3 / u.kg
        else:
            parameters_transport_material[0][nuc]["sorption_coefficient"] = 1. * u.m ** 3 / u.kg
    ic = [1.] * u.mol / u.m ** 3

    model_exp = modeltype(nuclides, parameters_rock, parameters_transport_material,
                          options={"include_decay": True, "nuclid_database": "vsg", "external_solver_method": "DOP853"},
                          geometry={"dx": dx},
                          time_interval=1e6 * u.yr)
    ics = np.zeros([model_exp.N]) * u.mol / u.m ** 3
    ics[:] = ic
    realics = {}
    for nuc in nuclides:
        if nuc == "stable":
            realics[nuc] = np.zeros([model_exp.N]) * u.mol / u.m ** 3
        else:
            realics[nuc] = ics
    dt = 200 * u.yr
    if use_bateman:
        states = []
        chains = []
        for nuc in nuclides:
            if nuc == "stable":
                continue
            chains.append([nuc, "stable"])
            if len(states) > 0:
                states.append(1)
            else:
                states.append(-1)

        model_exp.setup_bateman(chains, states)
    # dt = np.log(2) / model_exp.lambda_minus[0] * u.s / 100.

    model_exp.run(ics=realics, dt_max=dt, t_eval=model_exp.default_t_eval())
    # truesol = compare_decay(model_exp.lambda_minus[0] / u.s, model_exp.results["t"])
    truesol = compare_decay(nuclides, model_exp.results["t"])
    return model_exp, truesol


def test_single_species(modeltype, species, use_bateman):
    log = logging.getLogger("test_single_species")
    model, truesol = do_test_decay(modeltype, species, use_bateman)
    mysol = model.get_concentrations_t(0, ix=4, per_rock_volume=True, quantity="molar density")[1].value
    # import matplotlib.pyplot as plt
    # t = model.results["t"].to(u.yr)
    # plt.plot(t, mysol, label=repr(modeltype))
    # plt.title(f"{model.R}")
    # plt.plot(t, truesol[0], label="true solution")
    # plt.plot(t, truesol[0]/mysol)
    # plt.yscale("log")
    # plt.xscale("log")
    # plt.legend()
    # plt.show()
    rmse = RMSE(mysol, truesol[0])
    log.info(f"RMSE = {rmse} for {modeltype}, {species} {use_bateman}")
    return rmse


# noinspection PyArgumentList
def test_multi_species(modeltype, species, use_bateman):
    log = logging.getLogger("test_multi_species")
    model, truesol = do_test_decay(modeltype, species, use_bateman)
    n = truesol.shape[0]
    rmse = np.zeros(n)
    for i in range(n):
        mysol = model.get_concentrations_t(i, ix=4, per_rock_volume=True, quantity="molar density")[1].value
        # plt.plot(model.results["t"], mysol)
        # plt.plot(model.results["t"], truesol[i])
        # plt.plot(model.results["t"], truesol[i]/mysol)
        # plt.yscale("log")
        # plt.xscale("log")
        rmse[i] = RMSE(mysol, truesol[i])
    # plt.show()
    log.info(f"RMSE = {rmse} for {modeltype}, {species} {use_bateman}")
    return rmse.max()


def test_multi_species_total_amount(modeltype, species, use_bateman):
    log = logging.getLogger("test_multi_species_total_amount")
    model, truesol = do_test_decay(modeltype, species, use_bateman)
    tot = np.zeros(model.results["t"].size)
    for i, spec in enumerate(model.species):
        mysol = model.get_concentrations_t(spec, ix=4, per_rock_volume=True, quantity="molar density")[1].value
        tot += mysol
    # plt.plot(model.results["t"].to(u.yr), tot)
    # plt.xscale("log")
    # plt.show()
    rmse = RMSE(tot, np.ones_like(tot) * len(species))
    log.info(f"RMSE = {rmse} for {modeltype}, {species} {use_bateman}")
    return tot / (np.ones_like(tot) * len(species))


def test_multi_species_total_mass(modeltype, species, use_bateman):
    log = logging.getLogger("test_multi_species_total_mass")
    model, truesol = do_test_decay(modeltype, species, use_bateman)
    tot = np.zeros(model.results["t"].size) * u.kg / u.m ** 3
    for i, spec in enumerate(model.species):
        mysol = model.get_concentrations_t(spec, ix=4, per_rock_volume=True, quantity="mass density")[1]
        tot += mysol
    initial_mass = tot[0]
    # plt.plot(model.results["t"].to(u.yr), tot/initial_mass)
    # plt.xscale("log")
    # plt.show()
    rmse = RMSE(tot, np.ones_like(tot) * len(species))
    log.info(f"RMSE = {rmse} for {modeltype}, {species} {use_bateman}")
    return tot / initial_mass


def test_chain_total(modeltype, chain, chain_intel, num_states, use_bateman):
    logging.getLogger("test_chain")
    model, truesol = do_test_decay_chain(modeltype, chain, chain_intel, num_states, use_bateman)
    truesol.shape[1]
    tot = np.zeros(model.results["t"].size)
    for i, spec in enumerate(model.species):
        mysol = model.get_concentrations_t(spec, ix=4, per_rock_volume=True, quantity="molar density")[1].value
        tot += mysol

    return tot / (np.ones_like(tot) * (model.nspecies - 1))


def test_chain(modeltype, chain, chain_intel, num_states, use_bateman):
    log = logging.getLogger("test_chain")
    model, truesol = do_test_decay_chain(modeltype, chain, chain_intel, num_states, use_bateman)
    n = truesol.shape[1]
    rmse = np.zeros(n - 1)
    # plt.figure(figsize=(12,8))
    for i, spec in enumerate(model.species):
        if spec == "stable":
            continue
        mysol = model.get_concentrations_t(spec, ix=4, per_rock_volume=True, quantity="molar density")[1].value
        chain[i]
        # p=plt.plot(model.results["t"], mysol,label=label)
        # color = p[0].get_color()
        # plt.plot(model.results["t"], 1.1*truesol[:,i],c=color,ls=":")
        # plt.plot(model.results["t"], mysol/truesol[:,i],ls=":")
        # plt.yscale("log")
        # plt.xscale("log")
        rmse[i] = RMSE(mysol, truesol[:, i])
    # plt.legend()
    # plt.show()
    log.info(f"RMSE = {rmse} for {modeltype}, {chain} {use_bateman}")
    return rmse.max()


class DecayTests(unittest.TestCase):
    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, "Ca-41", False),
         ("cn", CrankNicolsonTransportModel1D, "Ca-41", False),
         ("euler_bateman", EulerBackwardsModel1D, "Ca-41", True),
         ("cn_bateman", CrankNicolsonTransportModel1D, "Ca-41", True),
         ("dop853", DOP853TransportModel1D, "Ca-41", False)
         ])
    def test_decay_ca41(self, name, model, species, use_bateman):
        rmse = test_single_species(model, species, use_bateman)
        self.assertLess(rmse, 1e-5)

    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, ["Ca-41", "Cl-36"], False),
         ("cn", CrankNicolsonTransportModel1D, ["Ca-41", "Cl-36"], False),
         ("euler_bateman", EulerBackwardsModel1D, ["Ca-41", "Cl-36"], True),
         ("cn_bateman", CrankNicolsonTransportModel1D, ["Ca-41", "Cl-36"], True),
         ("dop853", DOP853TransportModel1D, ["Ca-41", "Cl-36"], False)
         ])
    def test_decay_multi(self, name, model, species, use_bateman):
        rmse = test_multi_species(model, species, use_bateman)
        self.assertLess(rmse, 1e-5)

    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, ["Ca-41", "Cl-36"], False),
         ("cn", CrankNicolsonTransportModel1D, ["Ca-41", "Cl-36"], False),
         ("euler_bateman", EulerBackwardsModel1D, ["Ca-41", "Cl-36"], True),
         ("cn_bateman", CrankNicolsonTransportModel1D, ["Ca-41", "Cl-36"], True),
         ("dop853", DOP853TransportModel1D, ["Ca-41", "Cl-36"], False)
         ])
    def test_decay_multi_total(self, name, model, species, use_bateman):
        tot = test_multi_species_total_amount(model, species, use_bateman)
        np.testing.assert_allclose(tot, 1., rtol=1e-10)

    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, ["Ca-41", "Cl-36"], False),
         ("cn", CrankNicolsonTransportModel1D, ["Ca-41", "Cl-36"], False),
         ("euler_bateman", EulerBackwardsModel1D, ["Ca-41", "Cl-36"], True),
         ("cn_bateman", CrankNicolsonTransportModel1D, ["Ca-41", "Cl-36"], True),
         # dop853 has no working implementation of the scheme to track the stable mass
         # ("dop853", DOP853TransportModel1D, ["Ca-41","Cl-36"], False)
         ])
    def test_decay_multi_total_mass(self, name, model, species, use_bateman):
        tot = test_multi_species_total_mass(model, species, use_bateman)
        np.testing.assert_allclose(tot, 1., rtol=1e-10)

    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, *actininum_chain, False),
         ("euler_bateman", EulerBackwardsModel1D, *actininum_chain, True),
         ("cn", CrankNicolsonTransportModel1D, *actininum_chain, False),
         ("cn_bateman", CrankNicolsonTransportModel1D, *actininum_chain, True),
         ("dop853", DOP853TransportModel1D, *actininum_chain, False)
         ])
    def test_decay_chain(self, name, model, chain, chain_intel, num_states, use_bateman):
        rmse = test_chain(model, chain, chain_intel, num_states, use_bateman)
        self.assertLess(rmse, 1e-6)

    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, *actininum_chain, False),
         ("euler_bateman", EulerBackwardsModel1D, *actininum_chain, True),
         ("cn", CrankNicolsonTransportModel1D, *actininum_chain, False),
         ("cn_bateman", CrankNicolsonTransportModel1D, *actininum_chain, True),
         ("dop853", DOP853TransportModel1D, *actininum_chain, False)
         ])
    def test_decay_chain_total(self, name, model, chain, chain_intel, num_states, use_bateman):
        tot = test_chain_total(model, chain, chain_intel, num_states, use_bateman)
        np.testing.assert_allclose(tot, 1., rtol=1e-10)


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)

"""
implements some tests for the interpolator in utils.interpolator
"""
import os
import unittest

import astropy.units as u
import numpy as np
from transpyrend.utils.interpolator import Interpolator1D

ROOT_DIR = os.path.dirname(os.path.realpath(__file__))

def simple_interpolator():
    x = np.arange(1, 20) * u.dimensionless_unscaled
    y = np.array([x.value, x.value]) * u.dimensionless_unscaled
    ip = Interpolator1D(xdata=x, ydata=y)
    return ip


class InterpolatorTests(unittest.TestCase):
    def test_simple_interpolator(self):
        ip = simple_interpolator()
        np.testing.assert_allclose(ip(1.5 * u.dimensionless_unscaled), 1.5 * u.dimensionless_unscaled)

    def test_read_write_interpolator(self):
        ip = simple_interpolator()
        path = os.path.join(ROOT_DIR, "test_interpolator.hdf5")
        ip.to_disk(path)
        ip2 = Interpolator1D(filename=path)
        os.unlink(path)
        np.testing.assert_allclose(ip2(1.5 * u.dimensionless_unscaled), 1.5 * u.dimensionless_unscaled)


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)

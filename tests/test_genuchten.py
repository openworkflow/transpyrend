"""
implements a comparison with the analytic solution by (Genuchten 1982)
"""
import unittest

import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np
from parameterized import parameterized
from scipy import special
from stats import RMSE
from transpyrend.core.cranknicolson import CrankNicolsonTransportModel1D
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D

# TODO give credit to UFZ
###Input parameters###
# diffusion coefficient [m2/s]
De = 8.3e-11
# Porosity [-]
phi = 0.12
# Pore diffusion coefficient [m2/s]
D = De / phi
# Porous medium bulk density [kg/m3]
rho = 2.394e3
# Distribution coefficient [m3/kg]
Kd = 0.5
# Retardation factor [-]
R = 1 + rho * Kd / phi
# Sn-126 Half-life [year]
half_life = 2.35e5
# Decay constant [1/s]
k = np.log(2) / half_life / 3.1536e7  # unit conversion from year to second
# Include advective mechansim
# Darcy velocity [m/s]
q = 5e-11
# Pore water velocity [m/s]
v = q / phi
###Spatial and temporal discretization###
# Distance [m]
x = np.linspace(0, 20, num=2001)
# Time [year]
time = np.array([1e3, 5e3, 1e4, 5e4, 1e5, 5e5, 1e6])


def genuchten_1982():
    """
    calculate the analytic solution from Genuchten 1982

    Returns
    -------
    c : array_like
        the concentration array
    """
    # Initial condition [mol/L]
    c_ini = 0
    # Inflow concentration [mol/L]
    c0 = 1

    mu = k * R
    u1 = v * (1 + 4 * mu * D / v ** 2) ** 0.5

    # Auxilary functions
    def H(x, t):
        """
        defines function H from Genuchten 1982

        Parameters
        ----------
        x : array-like
            location at which to evaluate the function
        t : float
            time at which to evaluate the function
        Returns
        -------
        h : array-like
            the calculated value for H
        """
        return 0.5 * np.exp((v - u1) * x / 2 / D) * special.erfc((R * x - u1 * t) / 2 / (D * R * t) ** 0.5) \
               + 0.5 * np.exp((v + u1) * x / 2 / D) * special.erfc((R * x + u1 * t) / 2 / (D * R * t) ** 0.5)

    def M(x, t):
        """
        defines function M from Genuchten 1982

        Parameters
        ----------
        x : array-like
            location at which to evaluate the function
        t : float
            time at which to evaluate the function
        Returns
        -------
        m : array-like
            the calculated value for H
        """
        return - c_ini * np.exp(-mu * t / R) * (0.5 * special.erfc((R * x - v * t) / 2 / (D * R * t) ** 0.5) \
                                                + 0.5 * np.exp(v * x / D) * special.erfc(
                    (R * x + v * t) / 2 / (D * R * t) ** 0.5)) \
               + c_ini * np.exp(-mu * t / R)

    def eq_genuchten1981(c0, x, t):
        """
        implements the analytic solution of Genuchten 1982

        Parameters
        ----------
        c0 : float
            the initial concentration
        x : array-like
            location at which to evaluate the function
        t : float
            time at which to evaluate the function
        Returns
        -------
        m : array-like
            the calculated value for H
        """
        return c0 * H(x, t) + M(x, t)

    c = np.empty((0, x.size))
    for t in time * 3.1536e7:  # unit conversion from year to second
        c_t = eq_genuchten1981(c0, x, t)
        c = np.vstack([c, c_t])
    return c


def run_genuchten(modeltype, dx=0.01 * u.m, use_bateman=True, dt=100 * u.yr):
    """
    run the Genuchten 1982 setup in transpyrend

    Parameters
    ----------
    modeltype : BaseTransportModel1D
        the type of model to be used
    dx : astropy.units.Quantity
        the grid spacing
    use_bateman : bool
        if true, use the Bateman solution for operator splitting
    dt : astropy.units.Quantity
        the timestep size

    Returns
    -------
    model : BaseTransportModel1D
        the processed model

    """
    nuc = "Sn-126"
    kf = q * u.m / u.s
    parameters_rock = [{"porosity": phi,
                        "bulk_density": rho * u.kg / u.m ** 3,
                        "thickness": 20.0 * u.m,
                        "hydraulic_conductivity": kf,
                        "head_gradient": -1.
                        }]
    parameters_transport_material = [{nuc: {"sorption_coefficient": Kd * u.m ** 3 / u.kg,
                                            "effective_diffusion_coefficient": De * u.m ** 2 / u.s, },
                                      "stable": {"sorption_coefficient": 0 * u.m ** 3 / u.kg,
                                                 "effective_diffusion_coefficient": 0 * u.m ** 2 / u.s, }

                                      }]
    bc = {"bc_left": [(1. * u.mol / u.l / R).to(u.mol / u.m ** 3)], "bc_type_right": "Neumann",
          "bc_right": [(0. * u.mol / u.l).to(u.mol / u.m ** 3) / u.s]}
    # bc = {"bc_left":[(1.*u.mol/u.l).to(u.mol/u.m**3)]}

    total_runtime = 1e6 * u.yr
    model = modeltype([nuc, "stable"],
                      parameters_rock, parameters_transport_material,
                      bcs=bc,
                      geometry={"dx": dx},
                      time_interval=total_runtime, options={"nuclid_database": "vsg",
                                                            "external_solver_options": {"rtol": 1e-6, "atol": 1e-6}})
    if use_bateman:
        model.setup_bateman([[nuc, "stable"]], [1])
    ics = np.zeros([model.N, 2]) * u.mol / u.m ** 3
    # t_eval =np.linspace(0.,1e6,1001)*u.yr
    t_eval = [0, *time] * u.yr
    model.run(ics=ics, dt_max=dt, t_eval=t_eval)
    return model


def plot(truesol, model):
    """
    plots a comparison between the model and the analytic solution

    Parameters
    ----------
    truesol : array-like
        concentration array from the analytic solution
    model : BaseTransportModel1D
        the processed transpyrend model

    Returns
    -------

    """
    tlabels = [r"1 \times 10^3", r"5 \times 10^3",
               r"1 \times 10^4", r"5 \times 10^4",
               r"1 \times 10^5", r"5 \times 10^5",
               r"1 \times 10^6"]
    plt.figure(figsize=(12, 8))

    thecolors = []
    for i, c_t in enumerate(truesol):
        pl = plt.plot(x, c_t, label=r"Genuchten (1981), t=$" + tlabels[i] + "$",
                      markevery=(2, 5), linestyle="-", zorder=10, clip_on=True, markersize=4)
        thecolors.append(pl[0].get_color())
    plt.gca().set_prop_cycle(None)

    for i, c_t in enumerate(truesol):
        markevery = 3
        if i > 3:
            markevery = 3

        myx, myc = model.get_concentrations_x(0, itime=i + 1, per_rock_volume=True, quantity="molar density")
        plt.plot(myx, myc.to(u.mol / u.l), ls="", marker="o", markevery=markevery,
                 label=r"TransPyRend, t=$" + tlabels[i] + "$ yr", markersize=10, mfc='none')
    from matplotlib.lines import Line2D
    patches = []
    line = Line2D([0], [0], label="analytic", color="k")
    patches.append(line)
    line = Line2D([0], [0], label="TransPyREnd", color="k", marker="o", linestyle="", markersize=12, mfc='none')
    patches.append(line)
    # plt.ylim(0,0.01)
    plt.xlim(0, 2)
    plt.xlabel("x [m]")
    plt.ylabel("C [mol/m$^3$]")
    plt.legend(fontsize=18, handles=patches, frameon=False)
    plt.show()


def do_test_genuchten(modeltype, use_bateman):
    """
    do the comparison between transpyrend and the analytic solution

    Parameters
    ----------
    modeltype : BaseTransportModel1D
        the type of model to be used
    use_bateman : bool
        if true, use the Bateman solution for operator splitting

    Returns
    -------
    rmse : float
        the rmse between analytic solution and the transpyrend result

    """
    truesol = genuchten_1982()
    model = run_genuchten(modeltype, dx=0.01 * u.m, use_bateman=use_bateman, dt=100 * u.yr)
    errs = np.zeros(truesol.shape[0])
    # plot(truesol, model)
    for i, c_t in enumerate(truesol):
        myx, cexp = model.get_concentrations_x(0, itime=i + 1, per_rock_volume=True, quantity="molar density")
        cref = c_t
        rmse = RMSE(cexp.to(u.mol / u.l).value, cref)
        errs[i] = rmse
    rmse = errs.max()
    return rmse


class Genuchten1982Tests(unittest.TestCase):
    @parameterized.expand(
        [("euler", EulerBackwardsModel1D, False),
         ("euler_bateman", EulerBackwardsModel1D, True),
         ("cn", CrankNicolsonTransportModel1D, False),
         ("cn_bateman", CrankNicolsonTransportModel1D, True),
         # ("dop853", DOP853TransportModel1D, False)
         ])
    def test_genuchten(self, name, model, use_bateman):
        rsme = do_test_genuchten(model, use_bateman)
        self.assertLess(rsme, 1e-5)


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)

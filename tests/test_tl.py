"""some tests of the algorithm calculating the transport length"""

import unittest
import numpy as np


from transpyrend.core.cranknicolson import CrankNicolsonTransportModel1D
import astropy.units as u


def prepare_model_single():
    """
    prepare a transpyrend model

    Returns
    -------
    model : BaseTransportModel1D
        the model
    """
    nuclides = ["I-129", "Cl-36", "stable"]

    dx = 1 * u.m

    porosity = 0.1
    dhdx = -1.0
    K = 1e-11 * u.m / u.s
    De = 1e-11 * u.m ** 2 / u.s

    parameters_rock = [{"porosity": porosity,
                        "head_gradient": dhdx,
                        "hydraulic_conductivity": K,
                        "thickness": 100 * u.m
                        }]
    params0 = {"effective_diffusion_coefficient": De}
    parameters_transport_material = [{nuc: params0 for nuc in nuclides}]
    parameters_transport_material[0]["stable"] = {"effective_diffusion_coefficient": 0. * u.m ** 2 / u.s}

    model = CrankNicolsonTransportModel1D(nuclides, parameters_rock, parameters_transport_material,
                                          geometry={"dx": dx},
                                          time_interval=1e6 * u.yr,
                                          options={"nuclid_database": "vsg"})
    return model


def test_tl_simple(model, deposit_position=50 * u.m, deposit_width=1 * u.m):
    """
    a simple test of the transport length
    Parameters
    ----------
    model : BaseTransportModel1D
        the model
    deposit_position : astropy.units.Quantity
        position of the deposit
    deposit_width : astropy.units.Quantity
        width of the deposit

    Returns
    -------
    tl : astropy.units.Quantity
        transport length to the left
    tr : astropy.units.Quantity
        transport length to the right

    """
    model.results = {}
    model.results["t"] = model.default_t_eval()
    model.results["c_fluid"] = np.zeros([model.nspecies, model.results["t"].size, model.N])
    model.results["stable_mass"] = np.zeros([model.results["t"].size, model.N])

    w = int(deposit_width / (model.dxs[1] * u.m))
    if w <= 1:
        depo = int(deposit_position / (model.dxs[1] * u.m))
        model.results["c_fluid"][0:2, -1, depo] = 0.5 * (1. - 1.0001e-4) / model.phi[0, 0]
        model.results["c_fluid"][0:2, -1, depo - 20] = 0.25 * (1.0001e-4) / model.phi[0, 0]
        model.results["c_fluid"][0:2, -1, depo + 20] = 0.25 * (1.0001e-4) / model.phi[0, 0]
        wc = 1
    else:
        depo_right = int((deposit_position + deposit_width) / (model.dxs[1] * u.m))
        depo_left = int((deposit_position) / (model.dxs[1] * u.m))
        wc = depo_right - depo_left
        model.results["c_fluid"][0:2, -1, depo_left:depo_right] = 0.5 * (1. - 1.0001e-4) / model.phi[0, 0] / wc
        model.results["c_fluid"][0:2, -1, depo_left - 20] = 0.25 * (1.0001e-4) / model.phi[0, 0]
        model.results["c_fluid"][0:2, -1, depo_right + 20 - 1] = 0.25 * (1.0001e-4) / model.phi[0, 0]
        depo = depo_left

    tl = model.find_transport_length(depo, 1 * u.mol, from_side="left", threshold=0.5e-4, quantity="molar density",
                                     area=1 * u.m ** 2, repo_width=wc)
    tr = model.find_transport_length(depo, 1 * u.mol, from_side="right", threshold=0.5e-4, quantity="molar density",
                                     area=1 * u.m ** 2, repo_width=wc)
    return tl, tr


def test_tl_quantities(model, deposit_position=50 * u.m, quantity="molar density"):
    """
        another test of the transport length
        Parameters
        ----------
        model : BaseTransportModel1D
            the model
        deposit_position : astropy.units.Quantity
            position of the deposit
        quantity : str
            either "molar density" or "mass density"

        Returns
        -------
        tl : astropy.units.Quantity
            transport length to the left
        tr : astropy.units.Quantity
            transport length to the right

        """
    model.results = {}
    model.results["t"] = model.default_t_eval()
    model.mass_conversion_factor = [1., 2., 1.] * u.kg / u.mol
    if quantity == "mass density":
        total = (model.mass_conversion_factor * np.array([0.5, 0.5, 0.]) * u.mol).sum()
    else:
        total = 1 * u.mol
    model.results["c_fluid"] = np.zeros([model.nspecies, model.results["t"].size, model.N])
    model.results["stable_mass"] = np.zeros([model.results["t"].size, model.N])
    depo = int(deposit_position / (model.dxs[1] * u.m))
    # species 0 is completeley contained
    model.results["c_fluid"][0, -1, depo] = 0.5 / model.phi[0, 0]
    # species 1 has some small fraction scattered around
    uncontained = 1.000001e-4
    contained = 0.5 - uncontained
    model.results["c_fluid"][1, -1, depo] = contained / model.phi[0, 0]
    model.results["c_fluid"][1, -1, depo - 30:depo - 20] = 0.5 * uncontained / model.phi[0, 0] / 10.
    model.results["c_fluid"][1, -1, depo + 20 + 1:depo + 30 + 1] = 0.5 * uncontained / model.phi[0, 0] / 10.
    x, c = model.get_summed_concentrations_x(quantity=quantity, per_rock_volume=True)
    tl = model.find_transport_length(depo, total, from_side="left", threshold=0.5e-4, quantity=quantity,
                                     area=1 * u.m ** 2)
    tr = model.find_transport_length(depo, total, from_side="right", threshold=0.5e-4, quantity=quantity,
                                     area=1 * u.m ** 2)
    return tl, tr


class TLTests(unittest.TestCase):
    def test_tl_simple(self):
        model = prepare_model_single()
        tl, tr = test_tl_simple(model)
        self.assertAlmostEqual(tl.value, 19.5, places=3)
        self.assertAlmostEqual(tr.value, 19.5, places=3)
        tl, tr = test_tl_simple(model, deposit_width=5 * u.m, deposit_position=50 * u.m)
        self.assertAlmostEqual(tl.value, 19.5, places=3)
        self.assertAlmostEqual(tr.value, 19.5, places=3)

    def test_tl_quantities(self):
        model = prepare_model_single()
        tl, tr = test_tl_quantities(model, quantity="molar density")
        self.assertAlmostEqual(tl.value, 20.5, places=3)
        self.assertAlmostEqual(tr.value, 20.5, places=3)

        tl, tr = test_tl_quantities(model, quantity="mass density")
        self.assertAlmostEqual(tl.value, 23.0, places=3)
        self.assertAlmostEqual(tr.value, 23.0, places=3)


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)

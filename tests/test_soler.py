"""a comparison with the analytic soltion by Soler 2001"""

import unittest
from stats import RMSE
import numpy as np
from parameterized import parameterized

from transpyrend.core.externalsolver import DOP853TransportModel1D
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D
from transpyrend.core.cranknicolson import CrankNicolsonTransportModel1D
from transpyrend.utils.analyticsolutions import diffusion_advection_eq_soler2001, v_eq_soler2001
import astropy.units as u


def diffusion_advection_eq_soler2001_array(x, ts, v, D, c0):
    """
    evaluate Soler (2001) analytic solution at multiple times ts

    Parameters
    ----------
    x :
        distance from origin
    ts :
        times
    v :
        flow velocity
    D :
        diffusion coefficient

    Returns
    -------
    c : array
        calculated concentration

    """

    c = np.zeros((ts.size, x.size))

    for i, t in enumerate(ts):
        c[i, :] = diffusion_advection_eq_soler2001(x, t, v, D, c0)
    return c


def do_test_transport_soler2001(modeltype):
    """
    Run the transport test for a given modeltype (either ExplicitTransportModel1D or ImplicitTransportModel1D)

    The default parameter values follow values reported by Soler (2001, https://doi.org/10.1016/S0169-7722(01)00140-1)
    with values for K of 1e-12 m/s, De of 1e-12 m2/s and porosity of 0.05.
    The temperature gradient, osmotic pressure gradient were set to 0

    Parameters
    ----------
    modeltype :
        the type of model concerned

    Returns
    -------
    True on success
    """

    # set model parameters
    nuclide = "I-129"
    dt = 3 * u.yr
    time_interval = 20000. * u.yr
    dx = 0.1 * u.m

    porosity = 0.1
    dhdx = -1.0
    K = 1e-11 * u.m / u.s
    De = 1e-11 * u.m ** 2 / u.s
    c0 = 1.0 * u.mol / u.m ** 3
    density = 2350. * u.kg / u.m ** 3

    parameters_rock = [{"porosity": porosity,
                        "head_gradient": dhdx,
                        "hydraulic_conductivity": K,
                        "bulk_density": density,
                        "thickness": 300 * u.m
                        }]
    params0 = {"effective_diffusion_coefficient": De}
    parameters_transport_material = [{nuclide: params0}]

    bc = {"bc_left": 1. * porosity * u.mol / u.m ** 3}

    # run the numerical model
    model_exp = modeltype([nuclide], parameters_rock, parameters_transport_material,
                          geometry={"dx": dx},
                          time_interval=time_interval,
                          options={"include_decay": False, "nuclid_database": "vsg", "include_sorption": False},
                          bcs=bc)
    ics = np.zeros((model_exp.N, 1)) * u.mol / u.m ** 3
    ics[0, :] = 1.0 * porosity * u.mol / u.m ** 3
    model_exp.run(ics=ics, dt_max=dt, t_eval=model_exp.default_t_eval())

    # get the analytical solution
    x, _ = model_exp.get_concentrations_x(nuclide)
    t = model_exp.results["t"].to(u.s)
    v = v_eq_soler2001(De, K, dhdx, porosity)
    truesol = diffusion_advection_eq_soler2001_array(x, t, v, De / porosity, c0)
    return model_exp, truesol


class Soler2001Test(unittest.TestCase):
    @parameterized.expand(
        [("dop853", DOP853TransportModel1D),
         ("euler", EulerBackwardsModel1D),
         ("cn", CrankNicolsonTransportModel1D)
         ])
    def test_soler(self, name, modeltype):
        nnodes = 1000
        exclude_first_x_timesteps = 1
        model, truesol = do_test_transport_soler2001(modeltype)
        # plt.figure(figsize=(12, 8))
        for i, t in enumerate(model.results["t"]):
            if i < exclude_first_x_timesteps:
                continue
            x, mysol = model.get_concentrations_x(0, itime=i, quantity="molar density", per_rock_volume=False)
            x = x[0:nnodes + 1]
            sol = truesol[i, 0:nnodes + 1]
            mysol = mysol.value[0:nnodes + 1]
            rmse = RMSE(mysol, sol)
            self.assertLess(rmse, 1e-5)

            # plt.plot(x, mysol, label="TransPyREnd", alpha=0.9)
            # plt.plot(x, sol, label="analytic", ls=":", c="r")
            # plt.yscale("log")
            # plt.title(name)
            # plt.ylim(1e-20, 1)
        # plt.show()


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)

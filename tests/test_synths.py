"""a test on a synthetic, simple model with all 47 VSG nuclides and parallelization"""

import os
import unittest
import numpy as np
from parameterized import parameterized
from transpyrend.core.cranknicolson import CrankNicolsonTransportModel1D
from transpyrend.core.eulerbackwards import EulerBackwardsModel1D
from transpyrend.core.externalsolver import DOP853TransportModel1D
from transpyrend.wrapper.output import process_results, get_total
from transpyrend.wrapper.tools import prepare_model_from_disk, run_model, ModelArgs
import astropy.units as u


def execute_model(model, include_sorption, include_decay, use_bateman):
    """
    run transpyrend on the synthetic test

    Parameters
    ----------
    model : type
        the type of model to consider
    include_sorption : bool
        if true, consider sorption
    include_decay : bool
        if true, include decay
    use_bateman : bool
        if true, use the bateman solution for operator splitting


    Returns
    -------
    m : ModelArgs
        the wrapper argument for the model
    model : BaseTransportModel1D
        the processed transpyrend model
    mp : ModelParameters
        the model parameters object

    """
    inname = f"{os.path.dirname(os.path.realpath(__file__))}/../input_parameters/synth/model_parameters.yaml"
    m = ModelArgs(inname, ".")
    m.output = ["all_transport_lengths"]
    m.save_output = False
    if model == EulerBackwardsModel1D:
        m.solver = "euler-backwards"
    elif model == CrankNicolsonTransportModel1D:
        m.solver = "crank-nicolson"
    elif model == DOP853TransportModel1D:
        m.solver = "dop853"
    m.verbose = True

    override_args = {}
    if not include_sorption:
        override_args["include_sorption"] = False
    if not include_decay:
        override_args["include_decay"] = False
    if not use_bateman:
        override_args["use_bateman"] = False
    model, mp = prepare_model_from_disk(m, override_args=override_args)

    model, mp = run_model(m, model, mp)

    return m, model, mp


def calculate_transport_lengths(model, include_sorption, include_decay, use_bateman):
    """
    run the model and calculate the transport length

    Parameters
    ----------
    model : type
        the type of model to consider
    include_sorption : bool
        if true, consider sorption
    include_decay : bool
        if true, include decay
    use_bateman : bool
        if true, use the bateman solution for operator splitting

    Returns
    -------
    l : list
        the list of calculated transport lengths

    """
    m, model, mp = execute_model(model, include_sorption, include_decay, use_bateman)
    res = process_results(m, model, mp)
    res = res["all_transport_lengths"]
    res2 = np.zeros(4)
    res2[0] = res[0]
    res2[1] = res[4]
    res2[2] = res[1]
    res2[3] = res[5]

    return res2


def check_number_conservation(model, include_sorption, include_decay, use_bateman):
    """
    check the number conservation in the model

    Parameters
    ----------
    model : type
        the type of model to consider
    include_sorption : bool
        if true, consider sorption
    include_decay : bool
        if true, include decay
    use_bateman : bool
        if true, use the bateman solution for operator splitting

    Returns
    -------
    M : float
        the total amount of mass in the system divided by the expected mass

    """
    m, model, mp = execute_model(model, include_sorption, include_decay, use_bateman)
    # a fast way to get the total amount of all species
    # sum over species
    ctot = (model.results["c_fluid"] * model.phi.T[:, None, :]).sum(axis=0) * u.mol / u.m ** 3
    # sum over cells
    mtot = (ctot * mp.area_repository * model.dxs[None, :] * u.m).sum(axis=1)
    tot = get_total(m, "amount", mp)
    return mtot / tot


class SynthTests(unittest.TestCase):

    @parameterized.expand(
        [
            ("euler", EulerBackwardsModel1D, False),
            ("cn", CrankNicolsonTransportModel1D, False),
            ("euler_bateman", EulerBackwardsModel1D, True),
            ("cn_bateman", CrankNicolsonTransportModel1D, True),
            #   ("dop853", DOP853TransportModel1D, False)
        ])
    def test_TL(self, name, model, use_bateman):
        ls = calculate_transport_lengths(model, True, True, use_bateman)
        expected_result = np.array([37.12554547, 37.12554547, 49.56335073, 49.56335073])
        np.testing.assert_allclose(expected_result, ls, atol=0.1, rtol=0)

    @parameterized.expand(
        [
            ("euler", EulerBackwardsModel1D),
            ("cn", CrankNicolsonTransportModel1D),
            ("dop853", DOP853TransportModel1D)
        ])
    def test_TL_no_decay(self, name, model):
        ls = calculate_transport_lengths(model, True, False, False)
        expected_result = np.array([44.28684794, 44.28684794, 55.97130928, 55.97130928])
        np.testing.assert_allclose(expected_result, ls, atol=0.1, rtol=0)

    @parameterized.expand(
        [
            ("euler", EulerBackwardsModel1D),
            ("cn", CrankNicolsonTransportModel1D),
            # ("dop853", DOP853TransportModel1D)
        ])
    def test_TL_no_sorption(self, name, model):
        ls = calculate_transport_lengths(model, False, True, True)
        expected_result = np.array([295.380959, 295.448819, 345.131393, 345.207018])
        np.testing.assert_allclose(expected_result, ls, atol=0.1, rtol=0)

    @parameterized.expand(
        [
            ("euler", EulerBackwardsModel1D, False),
            ("cn", CrankNicolsonTransportModel1D, False),
            ("euler_bateman", EulerBackwardsModel1D, True),
            ("cn_bateman", CrankNicolsonTransportModel1D, True),
            # ("dop853", DOP853TransportModel1D, False)
        ])
    def test_mass_conservation(self, name, model, use_bateman):
        res = check_number_conservation(model, True, True, use_bateman)
        np.testing.assert_allclose(res, 1.0, rtol=1e-9)


if __name__ == '__main__':
    import unittest

    unittest.main(verbosity=10)
